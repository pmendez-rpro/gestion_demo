const reporter = require('cucumber-html-reporter');
const fs = require('fs');

const { reporterOptions } = require('./config/reporterOptions');

reporterOptions.forEach((option) => {
	fs.exists(
		option.jsonFile,
		(exists) => {
			if (exists) reporter.generate(option);
		}
	);
});
