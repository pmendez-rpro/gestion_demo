const { Given, When, Then, AfterAll, BeforeAll, Before, After, And } = require('cucumber');
const assert = require('assert');
const webdriver = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const chrome = require('selenium-webdriver/chrome');
const { exec } = require('child_process');
const remote = require('selenium-webdriver/remote');
const mobile = ['mobile', 'landscape_mobile', 'tablet'];

let driver;
let domain = (process.env.ENVIRONMENT == undefined || process.env.ENVIRONMENT == '' || process.env.ENVIRONMENT == 'dev') ? 'https://dev-gestion.??????.com.ar' : 'https://qa-gestion.??????.com.ar';
let domainBackend = (process.env.ENVIRONMENT == undefined || process.env.ENVIRONMENT == '' || process.env.ENVIRONMENT == 'dev') ? 'https://dev-gestion-api.??????.com.ar' : 'https://qa-gestion-api.??????.com.ar';

const { capabilities } = require ('../../config/capabilities');
const { resolutions } = require('../../config/resolutions');

const maxTimeOut = { timeout: 1000 * 60 * 60 * 24 };

const arg = process.argv[process.argv.length - 1];
const browser = arg.split('-')[0];
const res = arg.split('-')[1];

/////////////////////////!!!
const Functions = require('./utils/functions');
const funciones = new Functions(driver, webdriver, capabilities, browser, resolutions, res, remote);

BeforeAll( maxTimeOut, async() => {
	await funciones.inicializar();
});

AfterAll(maxTimeOut, () => {
  	try {
    	funciones.quit();
  	}
	catch(e) {
    	console.log(e);
  	}
});

Given('Click en loguearse como', maxTimeOut, async() => {
	let element;
	if (mobile.indexOf(res) >= 0) {
		element = await funciones.getElementAndClick('button.navbar-toggler', 15);
		await funciones.wait(1500);
	}
	element = await funciones.getElementAndClick('[id="menu_sistemas_login_como"]', 15);
	return element !== false;
});

Given('un Login válido como comercial que tenga permisos de alta tomador', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="39"]', 15);
	return element !== false;
});

Given('un Login válido como comercial sin permisos de alta tomador', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="51"]', 15);
	return element !== false;
});

Given('un Login válido como Developer', maxTimeOut, async() => {
	exec(`curl -d "secret=f6imY8gsd1JLWrOvqHR3ptWoZF896jOOs4TTAj2h" -X POST "${domainBackend}/1zcTwubuURaP0DucQKgxfqYDUlJf2CXZnMa32VOE"`, async (err, stdout, stderr) => {
		if (err) return false;
		const token = stdout;
		await funciones.loadPage(`${domain}?token=${token}`);
		return true;
	});
	
	//element = await getElement("#google_signIn_button", 30);
	//if (element === false) return false;
	//await element.Click();
	//result = await getNewWindow(2, 1, 30);
	//if (!result) return false;
	//element = await getElement("#identifierId", 30);
	//if (element === false) return false;
	//await element.Click();
	//await element.sendKeys(process.env.GOOGLE_USER_DEVELOPER_EMAIL);
	//element = await getElement("#identifierNext", 30);
	//if (element === false) return false;
	//await element.Click();
	//element = await getElement('input[type="password"]', 30);
	//if (element === false) return false;
	//await element.Click();
	//await element.sendKeys(process.env.GOOGLE_USER_DEVELOPER_PASS);
	//element = await getElement("#passwordNext", 30);
	//if (element === false) return false;
	//await element.Click();
});

When('Click en datos informativos', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="entidad_tomador_tomador_tab_1"]');
	return element !== false;
});

When('Click en editar contacto', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="contacto_tomador_editar_1"]', 15);
	return element !== false;
});

When('Click en el menú, opción: "alta modificación"', maxTimeOut, async() => {
	let element;
	if (mobile.indexOf(res) >= 0) {
		element = await funciones.getElementAndClick('button.navbar-toggler', 15);
	}
	element = await funciones.getElementAndClick('a[id="menu_comercial_alta_modificacion"]', 15);
	return element !== false;
});

When('Click en el menú, opción: "tomadores"', maxTimeOut, async() => {
	let element;
	if (mobile.indexOf(res) >= 0) {

	}
	element = await funciones.getElementAndClick('a[id="menu_comercial_alta_modificacion_tomador"]', 15);
	return element !== false;
});


When('se carga la Home Page', maxTimeOut, async() => {
	return await funciones.waitForUrl(`${domain}/home`, 15);
});

When('se carga el contacto', maxTimeOut, async() => {
	let element;
	
	element = await funciones.getElementAndClick('[id="contacto_tomador_nuevo"]', 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_sector"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="1"]', 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_nombre"]', 15);
	element.clear();
	element.sendKeys("Juan Perez");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_mail"]', 15);
	element.sendKeys("Juanperez@gmail.com");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_telefono_tipo"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="1"]', 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_telefono_numero"]', 15);
	element.sendKeys("544454488164");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_telefono_interno"]', 15);
	element.sendKeys("142");	

	return element !== false;

 });

When('se edita el contacto', maxTimeOut, async() => {
	let element;


	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_sector"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="2"]', 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_nombre"]', 15);
	element.clear();
	element.sendKeys("Ana");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_mail"]', 15);
	element.clear();
	element.sendKeys("ana@gmail.com");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_contacto_telefono_tipo"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="2"]', 15);

	return element !== false;

 });


Then('observo número de tomador y razón social', maxTimeOut, async() => {
	return true;
})

When('se carga el tomador con datos correctos', maxTimeOut, async() => {

	let element; 
	let element2;

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_tipo_ente"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="1"]', 15);

	element = await funciones.getElement('[id="form_entidad_tomador_razon_social"]', 15);
	element.sendKeys("Sociedad Anonima");

	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_pais"]', 9, 15);
	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_provincia"]', 1, 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_tipo"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="1"]', 15);

	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_localidad"]', 2, 30);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_calle"]', 15);
	element.sendKeys("Av. Santa Fe")

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_numero"]', 15);
	element.sendKeys("16665");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_cp"]', 15);
	element.sendKeys("1485");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_descripcion"]', 15);
	element.sendKeys("casa al fondo con rejas verdes");
	
	element = await funciones.getElementAndClick('[id="form_entidad_tomador_web"]', 15);
	element.sendKeys("http://www.google.com");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_telefono_empresarial"]', 15);
	element.sendKeys("545464654465456456");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_google_places_search"]', 15);
	element.sendKeys("Avenida Santa Fe 875");

	element2 = await funciones.getElements('[class="pac-item"]', 15);
	if (element2.length >= 1) {
		await element2[0].click();
	}

	element = await funciones.getElement('[id="form_entidad_tomador_informe_nosis"]', 15);
	element.sendKeys("/etc/pdfTest2.pdf")

	return element !== false;

})

When('se carga el tomador con datos erróneos', maxTimeOut, async() => {
	let element;

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_tipo_ente"]', 15);
	element = await funciones.getElementAndClick('[ng-reflect-value="1"]', 15);

	element = await funciones.getElement('[id="form_entidad_tomador_razon_social"]', 15);
	element.sendKeys("Sociedad Anonima");

	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_pais"]', 9, 15);
	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_provincia"]', 1, 15);
	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_localidad"]', 2, 30);
	element = await funciones.ajaxSelect('[id="form_entidad_tomador_direccion_tipo"]', 1, 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_calle"]', 15);

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_numero"]', 15);
	element.sendKeys("16665");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_cp"]', 15);
	element.sendKeys("1485");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_descripcion"]', 15);
	element.sendKeys("casa al fondo con rejas verdes");
	
	element = await funciones.getElementAndClick('[id="form_entidad_tomador_web"]', 15);
	element.sendKeys("localhost:2411");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_telefono_empresarial"]', 15);
	element.sendKeys("545464654465456456");

	element = await funciones.getElementAndClick('[id="form_entidad_tomador_direccion_calle"] + span.text-danger', 15);
	element = await funciones.getElementAndClick('[id="form_entidad_tomador_web"] + span.text-danger', 15);

	return element !== false;

})

When('se elimina un contacto', maxTimeOut, async() =>  {
	let element;
	 element = await funciones.getElementAndClick('[id="contacto_tomador_eliminar_1"]', 15);
	  element = await funciones.getElementAndClick('[id="modal_contacto_tomador_confirm_button"]', 15);
	return element !== false;
});

When('ingreso 4 o más caractéres correspondientes a un tomador existente', maxTimeOut, async() => {
	let element;
	element = await funciones.getElementAndClick('[id="buscador_entidades"]', 15);
	element.sendKeys("aran");
	return element !== false;
});

When('ingreso 4 o más caractéres correspondientes a un tomador inexistente', maxTimeOut, async() => {
	let element;
	element = await funciones.getElementAndClick('[id="buscador_entidades"]', 15);
	element.sendKeys("sand");
	return element !== false;
});

When('verifico que el tomador exista', maxTimeOut, async() => {
	const element = await funciones.getElement('[swall2-content]', 15);
	return element !== false;

})

Then('aparece el buscador', maxTimeOut, async() => {
	const element = await funciones.getElement('[id="buscador_entidades"]', 15);
	return element !== false;
});


Then('detecto el logo de ???', maxTimeOut, async() => {
	const element = await funciones.getElement('img[src*="letras.gif"]', 15);
	return element !== false;
});

Then('detecto que se indica que pertenece a la Agencia Buenos Aires', maxTimeOut, async() => {
	const element = await funciones.getElement('div.user-info span', 15);
	let text = await element.getText();
	text = String(text).trim();
	
	return text == 'Agencia: Buenos Aires';
});

Then('detecto la presencia de las dos campanas', maxTimeOut, async() => {
	const elements = await funciones.getElements('img[src*="notificacion.png"]', 15);
	return elements.length == 2;
});

Then('detecto que el avatar del usuario ha sido provisto por Google', maxTimeOut, async() => {
	const element = await funciones.getElement('img[src*="googleusercontent]', 15);
	return element !== false;
});

Then('efectúo el Logout', maxTimeOut, async() => {
	let element;
	if (mobile.indexOf(res) >= 0) {
		element = await funciones.getElementAndClick('button.navbar-toggler', 15);
	}
	element = await funciones.getElementAndClick('a#navbarDropdownMenuLink_accountOptions', 15);
	element = await funciones.getElementAndClick('a#endSessionPhoto', 15);
	element = await funciones.getElementAndClick('button#google_signIn_button', 15);
	return element !== false;
});

Then('el buscador devuelve los tomadores que coincidan con la búsqueda', maxTimeOut, async() => {
	const element = await funciones.getElement('[id="buscador_lista_entidades"]', 15);
	return element !== false;
});
	
Then('el buscador devuelve una lista vacía', maxTimeOut, async() => {
	const element = await funciones.getElement('#buscador_lista_entidades', 15);
	return element == null;
});

Then('no debería aparecer la opción alta modificación', maxTimeOut, async() => {
	const element = await funciones.getElement('a[id="menu_comercial_alta_modificacion"]', 15);
	return element == false;
});


Then('no debería aparecer la opción comercial', maxTimeOut, async() => {
	const element = await funciones.getElement('#opcion_comercial', 15);
	return element == false;
});


When('Click en crear contacto', maxTimeOut, async() => {
	let element;
	element = await funciones.getElementAndClick('[id="modal_contacto_tomador_confirm_button"]', 15);
	element = await funciones.getElementAndClick('["swall2-confirm"]', 15);

	return element !== false;
})

When('Click en crear tomador', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="alta_entidad_tomador"]', 30);
	return element !== false;
})

When('Click en cancelar crear contacto', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="modal_contacto_tomador_cancel_button"]', 15);
	return element !== false;
})

Then('Click en ir arriba', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="form_tomador_up_button"]', 15);
	return element !== false;
})

Then('Click en datos fiscales', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="form_tomador_next_button"]', 15);
	return element !== false;
})

Then('Click en confirmar', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="modal_contacto_tomador_confirm_button"]', 15);
	return element !== false;
})

Then('se despliega la consulta de alta', maxTimeOut, async() => {
	const element = await funciones.getElementAndClick('[id="alta_entidad"]', 15);
	return element !== false;
});

Then('Click alert', maxTimeOut, async() => {
	const element = await funciones.getAlert();
	return element !== false;
})