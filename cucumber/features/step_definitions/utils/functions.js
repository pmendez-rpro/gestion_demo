"use strict"
const assert = require('assert');
class Functions {
	constructor(pDriver, pWebdriver, pCapabilities, pBrowser, pResolutions, pRes, pRemote) {
		this.driver = pDriver;
		this.webdriver = pWebdriver;
		this.capabilities = pCapabilities;
		this.browser = pBrowser;
		this.resolutions = pResolutions;
		this.res = pRes;
		this.remote = pRemote;
	}
	
	async inicializar() {
		const cap = this.capabilities[this.browser];
		cap.name = cap.name.replace("{resolution}", this.resolutions[this.res]);
		this.dimensiones = this.resolutions[this.res].split("x");
		if (process.env.ENVIRONMENT === undefined || process.env.ENVIRONMENT === '' || process.env.ENVIRONMENT === 'local' || process.env.ENVIRONMENT === 'dev') {
			this.driver = await new this.webdriver.Builder().forBrowser('chrome');
			this.driver = await this.driver.build();
		}
		else if (process.env.ENVIRONMENT === 'qa') {
			this.driver = await new this.webdriver.Builder()
												  .usingServer(process.env.LAMBDA_TEST_URL)
												  .withCapabilities(cap);
			this.driver = await this.driver.build();
			await this.driver.setFileDetector(new this.remote.FileDetector());
		}
		await this.driver.manage().window().setRect({width: this.dimensiones[0] * 1, height: this.dimensiones[1] * 1, x: this.browser == "safari" ? 2 : 0, y: this.browser == "safari" ? 2 : 0});
	}
	
	async getElement (selector, maxAttempts) {
		let count, element;

		for (count = 1; count <= maxAttempts; count++) {
			try {
				element = await this.driver.findElement(this.webdriver.By.css(selector));
				if (element != undefined) 
					return element;
				await this.wait(1000);
			}
			catch (ex) {
				await this.wait(1000);
			}
		}
		
		return false;
	}

	async ajaxSelect (selector, optionID, maxAttempts) {
		let count, element, element2;
		element = await this.getElement (selector, maxAttempts);
		if (element === false) return false;

		for (count = 1; count <= maxAttempts; count++) {
			try {
				element = await this.getElementAndClick (selector, maxAttempts);
				element2 = await this.getElementAndClick(`[ng-reflect-value="${optionID}"]`, 1);

					if (element !== false) return element;
						await this.wait(1000);
				}
			catch (ex) {
				await this.wait(1000);
			}
		}
		
		return false;
	}


	async getElementAndClick(selector, maxAttempts) {
		let count, element;
		
		for (count = 1; count <= maxAttempts; count++) {
			try {
				element = await this.driver.findElement(this.webdriver.By.css(selector));
				if (element != undefined) {
					try {
						await element.click();
						return element;
					} catch(ex) {
						await this.wait(1000);
					}
				}				
				await this.wait(1000);
			}
			catch (ex) {
				await this.wait(1000);
			}
		}
		
		return false;
	}

	async getElements (selector, maxAttempts) {
		let count, elements;
		
		for (count = 1; count <= maxAttempts; count++) {
			try {
				elements =  await this.driver.findElements(this.webdriver.By.css(selector));
				if (elements != undefined && elements.length > 0) return elements;
				await this.wait(1000);
			}
			catch (ex) {
				await this.wait(1000);
			}
		}
		
		return false;
	}

	async getNewWindow (expectedWindows, switchTo, maxAttempts) {
		let windows, count;

		for (count = 1; count <= maxAttempts; count++) {
			try {
				windows = await this.driver.getAllWindowHandles();
				if (Array.isArray(windows)) {
					if (windows.length == expectedWindows) {
						await this.driver.switchTo().window(windows[switchTo]);
						return true;
					}
				}
				await this.wait(1000);
			}
			catch (ex) {
				await this.wait(1000);
			}
		}

		return false;
	}

	async wait (ms) {
		return new Promise(resolve => {
			setTimeout(resolve, ms);
		});
	}
	
	quit () {
		this.driver.quit();
	}
	
	async loadPage (page) {
		await this.driver.get(page);
	}
	
	async getCurrentUrl () {
		const url = await this.driver.getCurrentUrl();
		return url;
	}


	async getAlert () {
		await this.wait(10000);
		const alert = await this.driver.switchTo().alert();
		return true;	

		console.log("alert", alert);
		return alert;
	}


	async waitForUrl(url, maxAttempts) {
		let count, element;
		
		for (count = 1; count <= maxAttempts; count++) {
			try {
				const currentUrl = await this.getCurrentUrl();
				if (currentUrl == url) return true;
				await this.wait(1000);
			}
			catch (ex) {
				await this.wait(1000);
			}
		}
		
		return false;
	}
}

module.exports = Functions;
