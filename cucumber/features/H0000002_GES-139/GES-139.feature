Feature: Home
	Como cualquier usuario del Sistema de Gestión
	Quiero poder acceder a la Home Page del Sistema de Gestión
	Para disponer de un punto de partida y comenzar a efectuar operaciones
	
	Scenario: Home del usuario Developer
		Given un Login válido como Developer
		When se carga la Home Page
		Then detecto el logo de ???
		Then detecto que se indica que pertenece a la Agencia Buenos Aires
		Then detecto la presencia de las dos campanas
		Then detecto que el avatar del usuario ha sido provisto por Google
		Then efectúo el Logout