exports.reporterOptions = [
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-large_desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-large_desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Chrome",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Chrome",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-tablet.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-tablet.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Chrome",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-landscape_mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-landscape_mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Chrome",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_chrome-mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Chrome",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-large_desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-large_desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Firefox",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Firefox",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-tablet.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-tablet.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Firefox",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-landscape_mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-landscape_mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Firefox",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_firefox-mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Firefox",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-large_desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-large_desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Safari",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Safari",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-tablet.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-tablet.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Safari",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-landscape_mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-landscape_mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Safari",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_safari-mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Safari",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
    {
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-large_desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-large_desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Edge",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-desktop.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-desktop.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Edge",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-tablet.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-tablet.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Edge",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-landscape_mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-landscape_mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Edge",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    },
	{
        theme: 'bootstrap',
        jsonFile: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-mobile.json`,
        output: `report/${process.env.STORY_BRANCH}/cucumber_report_edge-mobile.html`,
        reportSuiteAsScenarios: true,
        launchReport: true,
        metadata: {
            "App Version":"0.3.2",
            "Test Environment": "QA",
            "Browser": "Edge",
            "Platform": process.platform,
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
    }
];