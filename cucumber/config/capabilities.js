exports.capabilities =
{
	chrome: {
		"build" : `${process.env.STORY_BRANCH}`,
		"name" : "Chrome - {resolution}",
		"platform" : "Windows 10",
		"browserName" : "Chrome",
		"version" : "73.0",
		"selenium_version" : "3.13.0",
		"console" : true,
		"network" : true,
		"visual" : true,
		"timezone" : "UTC-03:00",
		"chrome.driver" : 73.0,
		"resolution": "1280x1024"
	},
	firefox: {
		"build" : `${process.env.STORY_BRANCH}`,
		"name" : "Firefox - {resolution}",
		"platform" : "Windows 10",
		"browserName" : "Firefox",
		"version" : "66.0",
		"selenium_version" : "3.8.0",
		"console" : true,
		"network" : true,
		"visual" : true,
		"timezone" : "UTC-03:00",
		"firefox.driver" : "v0.24.0",
		"resolution": "1280x1024"
	},
	safari: {
		"build" : `${process.env.STORY_BRANCH}`,
		"name" : "Safari - {resolution}",
		"platform" : "macOS Mojave",
		"browserName" : "Safari",
		"version" : "12.0",
		"selenium_version" : "3.13.0",
		"console" : true,
		"network" : true,
		"visual" : true,
		"timezone" : "UTC-03:00",
		"safari.cookies" : true,
		"resolution": "1280x1024"
	},
	edge: {
		"build" : `${process.env.STORY_BRANCH}`,
		"name" : "Edge - {resolution}",
		"platform" : "Windows 10",
		"browserName" : "MicrosoftEdge",
		"version" : "18.0",
		"selenium_version" : "3.13.1",
		"console" : true,
		"network" : true,
		"visual" : true,
		"timezone" : "UTC-03:00",
		"resolution": "1280x1024"
	},
	internetExplorer: {
		"build" : `${process.env.STORY_BRANCH}`,
		"name" : "IE - {resolution}",
		"platform" : "Windows 11",
		"browserName" : "Internet Explorer",
		"version" : "11.0",
		"selenium_version" : "3.13.0",
		"console" : true,
		"network" : true,
		"visual" : true,
		"timezone" : "UTC-03:00",
		"ie.driver" : "2.53.1",
		"ie.compatibility" : 11001,
		"resolution": "1280x1024"
	}
};