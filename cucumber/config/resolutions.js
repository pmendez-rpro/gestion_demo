// Resoluciones basadas en los breakpoints de bootstrap 4 (menos mobile)
exports.resolutions =
{
	mobile: "320x576",
	// Landscape phones (width >= 576px)
	landscape_mobile: "576x500",
	// Tablets (width >= 768px)
	tablet: "768x1024",
	// Desktops (width >= 992px)
	desktop: "1024x768",
	// Large Desktops (width >= 1200px)
	large_desktop: "1200x1024"
};