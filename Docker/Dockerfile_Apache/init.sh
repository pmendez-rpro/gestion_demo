#!/bin/bash

sed -i "s/{DOMAIN_FRONTEND}/$DOMAIN_FRONTEND/g" /usr/local/apache2/conf/extra/httpd-vhosts.conf
sed -i "s/{DOMAIN_BACKEND}/$DOMAIN_BACKEND/g" /usr/local/apache2/conf/extra/httpd-vhosts.conf
sed -i "s/{DOMAIN_STATS}/$DOMAIN_STATS/g" /usr/local/apache2/conf/extra/httpd-vhosts.conf
sed -i "s/{HOST_IP}/$HOST_IP/g" /usr/local/apache2/conf/extra/httpd-vhosts.conf

cp /tmp/certs/certs.$ENVIRONMENT/* /usr/local/apache2/ssl-certs-2

htpasswd -b -c /usr/local/apache2/.htpasswd $SWAGGER_USER $SWAGGER_PASS

httpd-foreground
