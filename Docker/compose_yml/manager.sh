#!/bin/bash

# Environment				local|dev|qa|uat|prod
# Action					up|down
# HTTP Port					NÚMERO
# HTTPS Port				NÚMERO
# DB Port					NÚMERO
# Memcached Port			NÚMERO
# Express Port				NÚMERO
# Full Frontend Rebuild		yes|no
# NPM Install				yes|no
# Composer Update			yes|no
# Rebuild Dist				yes|no
# [Story Branch]			NOMBRE_HISTORIA

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

printf "${GREEN}Inicio del proceso de integración continua:${NC}\n"
set -e

# GET PARAMETERS ----------------------------------------------------------------
COMPOSE_YML_PATH="."
IS_ENV="0"
IS_ACT="0"
ENVIRONMENT="dev"
ACTION="up"
HTTP_PORT="80"
HTTPS_PORT="443"
DB_PORT="3306"
MEMCACHED_PORT="11211"
EXPRESS_PORT="3000"
FULL_FRONTEND_REBUILD="no"
NPM_INSTALL="no"
COMPOSER_UPDATE="no"
REBUILD_DIST="no"
STORY_BRANCH=""

for ((i=1;i<=$#;i++));
	do
		if [ ${!i} = "-env" ]; then
			((i++))
			IS_ENV="1"
			ENVIRONMENT=${!i}
		fi
		
		if [ ${!i} = "-act" ]; then
			((i++))
			IS_ACT="1"
			ACTION=${!i}
		fi
		
		if [ ${!i} = "-http-port" ]; then
			((i++))
			HTTP_PORT=${!i}
		fi
		
		if [ ${!i} = "-https-port" ]; then
			((i++))
			HTTPS_PORT=${!i}
		fi
		
		if [ ${!i} = "-db-port" ]; then
			((i++))
			DB_PORT=${!i}
		fi
		
		if [ ${!i} = "-memcached-port" ]; then
			((i++))
			MEMCACHED_PORT=${!i}
		fi
		
		if [ ${!i} = "-express-port" ]; then
			((i++))
			EXPRESS_PORT=${!i}
		fi
		
		if [ ${!i} = "-full-frontend-rebuild" ]; then
			((i++))
			FULL_FRONTEND_REBUILD=${!i}
		fi
		
		if [ ${!i} = "-npm-install" ]; then
			((i++))
			NPM_INSTALL=${!i}
		fi
		
		if [ ${!i} = "-composer-update" ]; then
			((i++))
			COMPOSER_UPDATE=${!i}
		fi
		
		if [ ${!i} = "-rebuild-dist" ]; then
			((i++))
			REBUILD_DIST=${!i}
		fi
		
		if [ ${!i} = "-story-branch" ]; then
			((i++))
			STORY_BRANCH=${!i}
		fi
done;

printf "Parámetros:\n"
printf "IS_ENV: $IS_ENV\n"
printf "IS_ACT: $IS_ACT\n"
printf "ENVIRONMENT: $ENVIRONMENT\n"
printf "ACTION: $ACTION\n"
printf "HTTP_PORT: $HTTP_PORT\n"
printf "HTTPS_PORT: $HTTPS_PORT\n"
printf "DB_PORT: $DB_PORT\n"
printf "MEMCACHED_PORT: $MEMCACHED_PORT\n"
printf "EXPRESS_PORT: $EXPRESS_PORT\n"
printf "FULL_FRONTEND_REBUILD: $FULL_FRONTEND_REBUILD\n"
printf "NPM_INSTALL: $NPM_INSTALL\n"
printf "COMPOSER_UPDATE: $COMPOSER_UPDATE\n"
printf "REBUILD_DIST: $REBUILD_DIST\n"
if [ "$ENVIRONMENT" == "qa" ]; then
	printf "STORY_BRANCH: $STORY_BRANCH\n"
fi

VALID="1"

if [ "$IS_ENV" == "0" ]; then
	printf "${RED}Se omitió el parámetro -env.${NC}\n"
	VALID="0"
fi

if [ "$IS_ACT" == "0" ]; then
	printf "${RED}Se omitió el parámetro -act.${NC}\n"
	VALID="0"
fi

if [ "$ACTION" != "up" ] && [ "$ACTION" != "down" ]; then
	printf "${RED}El parámetro -act debe valer 'up' o 'down'.${NC}\n"
	VALID="0"
fi

if [ "$ENVIRONMENT" != "local" ] && [ "$ENVIRONMENT" != "dev" ] && [ "$ENVIRONMENT" != "qa" ]  && [ "$ENVIRONMENT" != "uat" ] && [ "$ENVIRONMENT" != "prod" ]; then
	printf "${RED}El parámetro -env debe valer 'local', 'dev', 'qa', 'uat' o 'prod'.${NC}\n"
	VALID="0"
fi

if [ "$FULL_FRONTEND_REBUILD" != "yes" ] && [ "$FULL_FRONTEND_REBUILD" != "no" ]; then
	printf "${RED}El parámetro -full-frontend-rebuild debe valer 'yes' o 'no'.${NC}\n"
	VALID="0"
fi

if [ "$NPM_INSTALL" != "yes" ] && [ "$NPM_INSTALL" != "no" ]; then
	printf "${RED}El parámetro -npm-install debe valer 'yes' o 'no'.${NC}\n"
	VALID="0"
fi

if [ "$COMPOSER_UPDATE" != "yes" ] && [ "$COMPOSER_UPDATE" != "no" ]; then
	printf "${RED}El parámetro -composer-update debe valer 'yes' o 'no'.${NC}\n"
	VALID="0"
fi

if [ "$REBUILD_DIST" != "yes" ] && [ "$REBUILD_DIST" != "no" ]; then
	printf "${RED}El parámetro -rebuild-dist debe valer 'yes' o 'no'.${NC}\n"
	VALID="0"
fi

# DOCKER COMPOSE CONF------------------------------------------------------------
if [ "$VALID" == "1" ]; then
	DOCKER_COMPOSE_YML="stack.yml"
	rm -f $DOCKER_COMPOSE_YML
	cp "stack_${ENVIRONMENT}.template.yml" $DOCKER_COMPOSE_YML
	sed -i "s/{DB_PORT}/$DB_PORT/g" $DOCKER_COMPOSE_YML
	sed -i "s/{HTTP_PORT}/$HTTP_PORT/g" $DOCKER_COMPOSE_YML
	sed -i "s/{HTTPS_PORT}/$HTTPS_PORT/g" $DOCKER_COMPOSE_YML
	sed -i "s/{MEMCACHED_PORT}/$MEMCACHED_PORT/g" $DOCKER_COMPOSE_YML
	sed -i "s/{EXPRESS_PORT}/$EXPRESS_PORT/g" $DOCKER_COMPOSE_YML
	sed -i "s/{FULL_FRONTEND_REBUILD}/$FULL_FRONTEND_REBUILD/g" $DOCKER_COMPOSE_YML
	sed -i "s/{NPM_INSTALL}/$NPM_INSTALL/g" $DOCKER_COMPOSE_YML
	sed -i "s/{COMPOSER_UPDATE}/$COMPOSER_UPDATE/g" $DOCKER_COMPOSE_YML
	sed -i "s/{REBUILD_DIST}/$REBUILD_DIST/g" $DOCKER_COMPOSE_YML
else
	printf "${RED}Proceso finalizado insatisfactoriamente, debido a parámetros inválidos.${NC}\n"
	exit 1
fi

# UP ACTION ----------------------------------------------------------------
if [ "$ACTION" == "up" ] && [ "$VALID" == "1" ]; then
	if [ "$ENVIRONMENT" == "qa" ] && [ "$STORY_BRANCH" != "" ]; then
		if [ "$STORY_BRANCH" != "incremento" ]; then
			DESTINATION="incremento"
		else
			DESTINATION="master"
		fi
		
		#Crea el Pull Request:
		COD_PULL_REQUEST=$(curl -s https://api.bitbucket.org/2.0/repositories/???-???/gestion/pullrequests -u ???-???:jwnUE9xeXa4aJW8hDEH2 --request POST --header 'Content-Type: application/json' --data '{ "title": "Merge historia '"$STORY_BRANCH"' con '"$DESTINATION"'", "source": { "branch": { "name": "'"$STORY_BRANCH"'" } }, "destination": { "branch": { "name": "'"$DESTINATION"'" } } }' | sed -n "s/^.*\"id\": \([0-9]\+\).\+$/\1/p")
		
		#¿Hubo error?
		if [ "$COD_PULL_REQUEST" == "" ]; then
			printf "${RED}Error al crear el Pull Request.${NC}\n"
			exit 1
		fi
		
		#Intenta realizar el Merge:
		curl https://api.bitbucket.org/2.0/repositories/???-???/gestion/pullrequests/$COD_PULL_REQUEST/merge -u ???-???:jwnUE9xeXa4aJW8hDEH2 --request POST
		
		#¿Hubo error?
		RESULTADO_MERGE=$(curl -s https://api.bitbucket.org/2.0/repositories/???-???/gestion/pullrequests?q=state=%22MERGED%22 -u ???-???:jwnUE9xeXa4aJW8hDEH2 | sed -n "s/^.*\"id\": \($COD_PULL_REQUEST\).\+$/\1/p")
		if [ $RESULTADO_MERGE -eq $COD_PULL_REQUEST ]; then
			#Funcionó correctamente: aprobar el Pull Request
			curl https://api.bitbucket.org/2.0/repositories/???-???/gestion/pullrequests/$COD_PULL_REQUEST/approve -u ???-???:jwnUE9xeXa4aJW8hDEH2 --request POST
		else
			printf "${RED}Falló el Merge.${NC}\n"
			exit 1
		fi
		
		git checkout master
		git pull origin master
		git checkout $DESTINATION
		git pull origin $DESTINATION
		git checkout $DESTINATION
		git branch -d $STORY_BRANCH || git status
	fi
	
	if [ "$ENVIRONMENT" == "uat" ] && [ "$STORY_BRANCH" != "" ]; then
		git checkout master
		git pull origin master
		git checkout $STORY_BRANCH
		git pull origin $STORY_BRANCH
		git checkout $STORY_BRANCH
		if [ "$STORY_BRANCH" == "master" ]; then
			git branch -d incremento || git status
		fi
	fi
	
	if [ "$ENVIRONMENT" == "prod" ]; then
		git checkout master
		git pull origin master
	fi
	
	cp ${COMPOSE_YML_PATH}/gestion/gestion_${ENVIRONMENT}.env.template ${COMPOSE_YML_PATH}/gestion/gestion_${ENVIRONMENT}.env
	set -a
	source ${COMPOSE_YML_PATH}/gestion/gestion_${ENVIRONMENT}.env
	printf "${GREEN}Se inicia el Building de contenedores:${NC}\n"
	if [ "$ENVIRONMENT" != "prod" ]; then
		cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p gestion_${ENVIRONMENT} rm --stop --force
	fi
	cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p gestion_${ENVIRONMENT} up --build -d
	printf "${GREEN}Finalizó el Building de contenedores.${NC}\n"
	printf "${GREEN}Iniciando la verificación de contenedores:${NC}\n"
	CANTIDAD_CONTENEDORES="$(docker ps | wc -l)"
	if [ "$CANTIDAD_CONTENEDORES" != "8" ]; then
		printf "${RED}Uno o más contenedores se han detenido.${NC}\n"
		exit 1
	fi
	
	#Verifica Angular:
	printf "${GREEN}Verificando Angular:${NC}\n"
	while [ "$(docker exec gestion_${ENVIRONMENT}_angular_1 bash -c 'ps ax | grep "node index.js" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}Angular iniciado.${NC}\n"
	
	#Verifica Apache:
	printf "${GREEN}Verificando Apache:${NC}\n"
	while [ "$(docker exec gestion_${ENVIRONMENT}_apache_1 bash -c 'ps ax | grep "httpd -DFOREGROUND" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}Apache iniciado.${NC}\n"
	
	#Verifica Cucumber:
	if [ "$ENVIRONMENT" != "uat" ] && [ "$ENVIRONMENT" != "prod" ]; then
		printf "${GREEN}Verificando Cucumber:${NC}\n"
		while [ "$(docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c 'ps ax | grep "supervisord -n" | grep -v "grep"')" == "" ]; do
    		sleep 1
			printf "."
		done
	fi
	printf "${GREEN}Cucumber iniciado.${NC}\n"
	
	#Verifica DB (MySQL):
	printf "${GREEN}Verificando DB (MySQL):${NC}\n"
	while [ "$(docker exec gestion_${ENVIRONMENT}_db_1 bash -c 'ps ax | grep "mysqld" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}DB (MySQL) iniciado.${NC}\n"
	
	#Verifica Memcached (su inicialización ya está ubicada en el CMD):
	printf "${GREEN}Memcached iniciado.${NC}\n"
	
	#Verifica Migrations:
	printf "${GREEN}Verificando Migrations (Liquibase):${NC}\n"
	while [ "$(docker exec gestion_${ENVIRONMENT}_migrations_1 bash -c 'ps ax | grep "supervisord -n" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}Migrations (Liquibase) iniciado.${NC}\n"
	
	#Verifica PHP:
	printf "${GREEN}Verificando PHP:${NC}\n"
	while [ "$(docker exec gestion_${ENVIRONMENT}_php_1 bash -c 'ps ax | grep "php-fpm: master process" | grep -v "grep"')" == "" ]; do
    	sleep 1
		printf "."
	done
	printf "${GREEN}PHP iniciado.${NC}\n"
	
	#Regenera Swagger:
	printf "${GREEN}Regenerando Swagger.${NC}\n"
	docker exec gestion_${ENVIRONMENT}_php_1 bash -c 'cd /var/www/html/gestion && php artisan swagger-lume:publish && chmod -R 777 /var/www/html/gestion/storage/logs'
	docker exec gestion_${ENVIRONMENT}_php_1 bash -c 'cd /var/www/html/gestion && php artisan swagger-lume:generate && chmod -R 777 /var/www/html/gestion/storage/logs'
	
	if [ "$ENVIRONMENT" == "qa" ]; then
		#Correr Tests:
		printf "${GREEN}Se inicia la ejecución de escenarios de Cucumber:${NC}\n"
		
		#Crea la carpeta de los reportes de los Tests de la historia:
		mkdir -p ../../cucumber/report/$STORY_BRANCH
		export STORY_BRANCH="$STORY_BRANCH"
		#make -j "chrome-mobile" "chrome-landscape_mobile" "chrome-tablet" "chrome-desktop" "chrome-large_desktop" "edge-mobile" "edge-landscape_mobile" "edge-tablet" "edge-desktop" "edge-large_desktop" "firefox-mobile" "firefox-landscape_mobile" "firefox-tablet" "firefox-desktop" "firefox-large_desktop" "safari-mobile" "safari-landscape_mobile" "safari-tablet" "safari-desktop" "safari-large_desktop"
		
		#Chrome:
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_chrome-mobile.json -n 'chrome-mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_chrome-landscape_mobile.json -n 'chrome-landscape_mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_chrome-tablet.json -n 'chrome-tablet'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_chrome-desktop.json -n 'chrome-desktop'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_chrome-large_desktop.json -n 'chrome-large_desktop'"
		
		#Edge:
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_edge-mobile.json -n 'edge-mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_edge-landscape_mobile.json -n 'edge-landscape_mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_edge-tablet.json -n 'edge-tablet'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_edge-desktop.json -n 'edge-desktop'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_edge-large_desktop.json -n 'edge-large_desktop'"
		
		#Firefox:
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_firefox-mobile.json -n 'firefox-mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_firefox-landscape_mobile.json -n 'firefox-landscape_mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_firefox-tablet.json -n 'firefox-tablet'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_firefox-desktop.json -n 'firefox-desktop'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_firefox-large_desktop.json -n 'firefox-large_desktop'"
		
		#Safari:
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_safari-mobile.json -n 'safari-mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_safari-landscape_mobile.json -n 'safari-landscape_mobile'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_safari-tablet.json -n 'safari-tablet'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_safari-desktop.json -n 'safari-desktop'"
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && npx cucumber-js /cucumber/features/ -f json:/cucumber/report/$STORY_BRANCH/cucumber_report_safari-large_desktop.json -n 'safari-large_desktop'"
		
		docker exec gestion_${ENVIRONMENT}_cucumber_1 bash -c "export STORY_BRANCH="$STORY_BRANCH" && cd /cucumber && node --no-warnings index.js"
		printf "${GREEN}Finalizó la ejecución de escenarios de Cucumber.${NC}\n"
	fi
	
	printf "${GREEN}Finalizó la verificación de contenedores.${NC}\n"
	printf "${GREEN}¡Finalizó la integración continua! Verifique los escenarios ejecutados, para fiscalizar la corrección del proceso.${NC}\n"
fi

# DOWN ACTION ----------------------------------------------------------------

if [ "$ACTION" == "down" ] && [ "$VALID" == "1" ]; then
	set -a
	source ${COMPOSE_YML_PATH}/gestion/gestion_${ENVIRONMENT}.env
	printf "${GREEN}Se inicia la detención y eliminación de contenedores:${NC}\n"
	cat ${DOCKER_COMPOSE_YML} | envsubst | /usr/local/bin/docker-compose -f - -p gestion_${ENVIRONMENT} rm --stop --force
	printf "${GREEN}¡Finalizó la detención y eliminación de contenedores!${NC}\n"
fi
# -----------------------------------------------------------------------
