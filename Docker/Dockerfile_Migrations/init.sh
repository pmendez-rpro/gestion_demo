#!/bin/bash

#Se espera a que se inicie MySQL:
echo "Waiting for MySQL..."
while [ "$(mysqladmin ping --host=$DB_HOST --user=$DB_USERNAME --password=$DB_PASSWORD --port=$DB_PORT)" != "mysqld is alive" ]; do
    sleep 3
done
echo "MySQL started"

#MySQL:
#Workaround necesario por problemas con PDO:
sed -i "s/{pass}/$DB_PASSWORD/g" /tmp/init.sql

#Ejecuta el Script inicial de MySQL:
mysql --host=$DB_HOST --user=$DB_USERNAME --password=$DB_PASSWORD --port=$DB_PORT < /tmp/init.sql

if [ "$ENVIRONMENT" != "prod" ]; then
	#Efectúa una comparación entre los XML de la rama actual y los efectivamente ejecutados, para poder determinar cuál fue el último changeSet en común:
	#Se garantiza de que exista el archivo /root/current_migrations/changelog_0000001.xml:
	touch /root/current_migrations/changelog_0000001.xml
	ULTIMO_TAG_COMUN=$(grep -Fxf /root/migrations/changelog_0000001.xml /root/current_migrations/changelog_0000001.xml | grep "<changeSet" | tail -n 1 | sed -n "s/^.*<changeSet.*id=\"\([^\"]*\)\".*>$/\1/p")
	if [ "$ULTIMO_TAG_COMUN" != "" ]; then
		mv /root/migrations/changelog_0000001.xml /root/migrations/changelog_0000001.xml.new
		cp -pr /root/current_migrations/changelog_0000001.xml /root/migrations/changelog_0000001.xml
		
		#Calcula la cantidad de Migrations que debe retroceder:
		MIGRATIONS_POR_RETROCEDER=$(echo "select count(*) from DATABASECHANGELOG where ORDEREXECUTED > (select ORDEREXECUTED from DATABASECHANGELOG where ID='$ULTIMO_TAG_COMUN')" | mysql --host=$DB_HOST --user=$DB_USERNAME --port=$DB_PORT $DB_DATABASE | tail -n 1)
		
		cd /root/liquibase && \
		java -jar liquibase.jar \
		--classpath=/usr/share/java/mysql-connector-java-8.0.11.jar \
		--driver=com.mysql.cj.jdbc.Driver \
		--changeLogFile=/root/migrations/changelog_master.xml \
		--url="jdbc:mysql://$DB_HOST:$DB_PORT/$DB_DATABASE" \
		--username=$DB_USERNAME \
		--password=$DB_PASSWORD \
		rollbackCount $MIGRATIONS_POR_RETROCEDER
		
		rm -f /root/migrations/changelog_0000001.xml
		mv /root/migrations/changelog_0000001.xml.new /root/migrations/changelog_0000001.xml
		rm -f /root/current_migrations/changelog_0000001.xml
		cp -pr /root/migrations/changelog_0000001.xml /root/current_migrations/changelog_0000001.xml
	fi
fi

cd /root/liquibase && \
java -jar liquibase.jar \
--classpath=/usr/share/java/mysql-connector-java-8.0.11.jar \
--driver=com.mysql.cj.jdbc.Driver \
--changeLogFile=/root/migrations/changelog_master.xml \
--url="jdbc:mysql://$DB_HOST:$DB_PORT/$DB_DATABASE" \
--username=$DB_USERNAME \
--password=$DB_PASSWORD \
migrate

#Efectúa una copia de todo lo ejecutado:
rm -fr /root/current_migrations/*.xml
cp -pr /root/migrations/*.xml /root/current_migrations

supervisord -n
