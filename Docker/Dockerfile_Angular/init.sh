#!/bin/bash

if [ "$FULL_FRONTEND_REBUILD" == "yes" ]; then
	cd /app
	npm link @angular/cli
	rm -fr /app/node_modules
	rm -f /app/package-lock.json
	npm install
	ng build --configuration=$ENVIRONMENT
	rm -fr /app/building/dist
	mv /app/dist /app/building/dist
	mkdir /app/building/dist/temp
	chmod -R 777 /app/building/dist/temp
	cd /app/server
	rm -fr /app/server/node_modules
	rm -f /app/server/package-lock.json
	npm install
	npm link @angular/cli
else
	cd /app
	if [ "$NPM_INSTALL" == "yes" ]; then
		npm install
	fi
	if [ "$REBUILD_DIST" == "yes" ]; then
		npm link @angular/cli
		npm install
		ng build --configuration=$ENVIRONMENT
		rm -fr /app/building/dist
		mv /app/dist /app/building/dist
		mkdir /app/building/dist/temp
		chmod -R 777 /app/building/dist/temp
	fi
	cd /app/server
	npm link @angular/cli
	if [ "$NPM_INSTALL" == "yes" ]; then
		npm install
	fi
fi

node index.js
