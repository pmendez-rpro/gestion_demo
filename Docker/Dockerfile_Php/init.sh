#!/bin/bash

#Configuraciones de PHP, dependientes del ambiente:
sed -i "s/{display_errors}/$PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{display_startup_errors}/$PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{enable_dl}/$PHP_ENABLE_DL/g" /usr/local/etc/php/php.ini
sed -i "s/{error_log}/$PHP_ERROR_LOG/g" /usr/local/etc/php/php.ini
sed -i "s/{log_errors}/$PHP_LOG_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/{max_execution_time}/$PHP_MAX_EXECUTION_TIME/g" /usr/local/etc/php/php.ini

#Archivo de configuración:
rm -f /var/www/html/gestion/.env
mv /root/.env /var/www/html/gestion/.env
sed -i "s/{APP_ENV}/$LARAVEL_APP_ENV/g" /var/www/html/gestion/.env
sed -i "s/{APP_DEBUG}/$LARAVEL_APP_DEBUG/g" /var/www/html/gestion/.env
sed -i "s/{APP_FRONTEND_DOMAIN}/$LARAVEL_APP_FRONTEND_DOMAIN/g" /var/www/html/gestion/.env
sed -i "s/{APP_BACKEND_DOMAIN}/$LARAVEL_APP_BACKEND_DOMAIN/g" /var/www/html/gestion/.env
sed -i "s/{APP_LOG_LEVEL}/$LARAVEL_APP_LOG_LEVEL/g" /var/www/html/gestion/.env
sed -i "s/{DB_HOST}/$LARAVEL_DB_HOST/g" /var/www/html/gestion/.env
sed -i "s/{DB_PORT}/$LARAVEL_DB_PORT/g" /var/www/html/gestion/.env
sed -i "s/{DB_DATABASE}/$LARAVEL_DB_DATABASE/g" /var/www/html/gestion/.env
sed -i "s/{DB_USERNAME}/$LARAVEL_DB_USERNAME/g" /var/www/html/gestion/.env
sed -i "s/{DB_PASSWORD}/$LARAVEL_DB_PASSWORD/g" /var/www/html/gestion/.env
sed -i "s/{SESSION_DOMAIN}/$LARAVEL_SESSION_DOMAIN/g" /var/www/html/gestion/.env
sed -i "s/{X_FORWARDED_FOR}/$LARAVEL_X_FORWARDED_FOR/g" /var/www/html/gestion/.env
sed -i "s/{GOOGLE_DRIVE_PERSONAS}/$LARAVEL_GOOGLE_DRIVE_PERSONAS/g" /var/www/html/gestion/.env

#Permisos:
chown -R dev:dev /var/www/html/gestion
chmod -R 777 /var/www/html/gestion/public/temp
touch /var/www/html/gestion/storage/logs/lumen.log
chmod 664 /var/www/html/gestion/storage/logs/lumen.log
chmod -R 777 /var/www/html/gestion/storage
chmod -R 777 /var/www/html/gestion/storage/app
mkdir -p /var/www/html/gestion/storage/app/public
chmod -R 777 /var/www/html/gestion/storage/app/public
chmod -R 777 /var/www/html/gestion/storage/gestion
chmod -R 777 /var/www/html/gestion/storage/gestion/cache
mkdir -p /var/www/html/gestion/storage/gestion/sessions
chmod -R 777 /var/www/html/gestion/storage/gestion/sessions
chmod -R 777 /var/www/html/gestion/storage/gestion/views
chmod -R 777 /var/www/html/gestion/storage/logs
mkdir -p /var/www/html/gestion/bootstrap/cache
chmod 777 /var/www/html/gestion/bootstrap/cache
touch /var/www/html/gestion/storage/logs/laravel.log
chgrp www-data /var/www/html/gestion/storage/logs/laravel.log
chgrp www-data /var/www/html/gestion/storage
chgrp www-data /var/www/html/gestion/storage/app
chgrp www-data /var/www/html/gestion/storage/app/public
chgrp www-data /var/www/html/gestion/storage/gestion
chgrp www-data /var/www/html/gestion/storage/gestion/cache
chgrp www-data /var/www/html/gestion/storage/gestion/sessions
chgrp www-data /var/www/html/gestion/storage/gestion/views
chgrp www-data /var/www/html/gestion/storage/logs
chgrp www-data /var/www/html/gestion/bootstrap/cache

#Credenciales de Google:
rm -f /var/www/html/gestion/app/Console/Commands/google_drive/credentials.json
cp /tmp/google_credentials/credentials.json.$ENVIRONMENT /var/www/html/gestion/app/Console/Commands/google_drive/credentials.json
cp -u /tmp/google_credentials/token.json.$ENVIRONMENT /var/www/html/gestion/app/Console/Commands/google_drive/token.json
chmod -R 777 /var/www/html/gestion/app/Console/Commands/google_drive

#Se espera a que se inicie MySQL:
echo "Waiting for MySQL..."
while [ "$(mysqladmin ping --host=$LARAVEL_DB_HOST --user=$LARAVEL_DB_USERNAME --password=$LARAVEL_DB_PASSWORD --port=$LARAVEL_DB_PORT)" != "mysqld is alive" ]; do
    sleep 3
done
echo "MySQL started"

#Composer Update:
if [ "$COMPOSER_UPDATE" == "yes" ]; then
	cd /var/www/html/gestion
	composer update
	#sed -i "s/ENT_QUOTES,/ENT_QUOTES|ENT_SUBSTITUTE,/g" /var/www/html/gestion/vendor/laravel/lumen-gestion/src/helpers.php
fi

service cron start
crontab /tmp/crontab.txt

php-fpm
