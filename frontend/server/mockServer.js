const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();

server.use(middlewares);
server.use(jsonServer.bodyParser);

const simulateError = (res, error, path) => {
	switch(error) {
    case 400:
        res.status(400).jsonp({ error: ["ERR_INVALID_REQUEST"] });
        break;
    case 401:
            res.status(401).jsonp({ error: ["ERR_NO_PERMISSION"] });
        break;
    case 403:
        if(path === "/usuario/login")
            res.status(403).jsonp({ error: ["ERR_INVALID_LOGIN"] });
        if(path === "/usuarios/lista" || path === "/usuario/login-como")
            res.status(403).jsonp({ error: ["ERR_TOKEN_NOT_FOUND"] });
    case 404:
        res.status(404).jsonp({ error: ["ERR_NOT_FOUND"] });
        break;
    case 500:
        res.status(500).jsonp({ error: ["ERR_INTERNAL_SERVER_ERROR"] });
        break;
    default:
        res.status(error).jsonp({ error: ["ERR_UNKNOWN_ERROR"]});
        break;
  }
};

const mockedDb = router.db.__wrapped__;
let data;

router.render = (req, res) => {
  if(req.method === 'POST') {
    switch(req.path) {
      case "/usuario/login":
          console.log(req.body);
          //simulateError(res,403, req.path);
	        res.status(200).jsonp(mockedDb.loginComercial);
          break;
	    case "/usuario/logout":
          console.log(req.body);
          //simulateError(res,400, req.path);
          //res.status(200).jsonp({ error: ["ERR_NO_ERROR"] });
          res.status(200).jsonp(mockedDb.loginSistemas);
          break;
      case "/usuario/login-como":
          console.log(req.body);
          //simulateError(res,400, req.path);
          res.status(200).jsonp(mockedDb.loginComercial);
          break;
      case "/usuarios/lista":
          //simulateError(res,400, req.path);
          res.status(200).jsonp(mockedDb.users);
          break;
      case "/entidades/lista":
          //simulateError(res,400, req.path);
          const searchText = req.body.search;
          const filteredEntities = mockedDb.entities.entities.filter((e) => {
            return e.text.includes(searchText);
          });
          data = { error: "ERR_NO_ERROR", entities: filteredEntities };
          res.status(200).jsonp(data);
          break;
      case "/crud/obtener-token":
          console.log(req.body);
          //simulateError(res,400, req.path);
          res.status(200).jsonp(mockedDb.crud_token);
          break;
      case "/geo/paises":
          console.log(req.body);
          //simulateError(res,400, req.path);
          res.status(200).jsonp(mockedDb.paises);
          break;
      case "/geo/provincias":
          console.log(req.body);
          //simulateError(res,400, req.path);
          const paisId = req.body.country;
          const filteredProvincias = mockedDb.provincias.provinces.filter((p) => {
            return p.country_id == paisId;
          });
          const provinces = [];
          filteredProvincias.forEach((filterProv) => {
            provinces.push({ id: filterProv.id, name: filterProv.name });
          });
          data = { error: "ERR_NO_ERROR", provinces };
          res.status(200).jsonp(data);
          break;
      case "/geo/localidades":
          console.log(req.body);
          //simulateError(res,400, req.path);
          const provinciaId = req.body.province;
          const filteredLocalidades = mockedDb.localidades.cities.filter((c) => {
            return c.province_id == provinciaId;
          });
          const cities = [];
          filteredLocalidades.forEach((filterLoc) => {
            cities.push({ id: filterLoc.id, name: filterLoc.name });
          });
          data = { error: "ERR_NO_ERROR", cities };
          res.status(200).jsonp(data);
          break;
      case "/archivo/enviar":
          console.log(req.body);
          //simulateError(res,400, req.path);
          res.status(200).jsonp(mockedDb.archivo_informe_nosis);
          break;
  	  default:
          console.log("Ruta no contemplada");
          break;
    }
  }
  if(req.method === "GET") {
    switch(req.path) {
        default:
            console.log("Ruta no contemplada");
            return;
    }
  }
  if(req.method === "PUT") {
    switch(req.path) {
      case "/tomador/insertar":
        console.log(req.body);
        //simulateError(res,400, req.path);
        res.status(200).jsonp(mockedDb.nuevoTomador);
        break;
      default:
          console.log("Ruta no contemplada");
          break;
    }
  }
}

server.use(router);
server.listen(3001, () => {
  console.log('JSON Server is running on port 3001')
});
