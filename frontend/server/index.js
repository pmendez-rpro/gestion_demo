const express = require('express');
const app = express();
const http = require('http');
const https = require('https');
const fs = require('fs');
const cors = require("cors");

const key = fs.readFileSync('./???');
const cert = fs.readFileSync('./???' );
const ca = fs.readFileSync('./???');

const options = { key, cert, ca };

let server;

if(process.env.DOMAIN_FRONTEND == 'localhost'){
	server = http.createServer(app);
	app.use(cors({ origin: `http://${ process.env.DOMAIN_FRONTEND }:4200` }));
} else {
	server = https.createServer(options, app);
	app.use(cors({ origin: `https://${ process.env.DOMAIN_FRONTEND }-???` , credentials : true }));
}

if(!process.env.DOMAIN_FRONTEND) {
	server = http.createServer(app);
	app.use(cors({ origin: `http://localhost:4200` }));
}

const io = require('socket.io')(server);

server.listen(3000, () => {
	console.log('Listening on port: 3000');
});

io.on('connection', (socket) => {
	console.log('Usuario conectado');
	socket.on('disconnect', function(){
		console.log('Usuario desconectado');
	});
});