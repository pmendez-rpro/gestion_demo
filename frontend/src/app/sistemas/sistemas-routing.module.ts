import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../socialSignin/auth-guard.service';
import { SistemasComponent } from './sistemas.component';
import { SistemasLoginComoComponent } from './sistemas-loginComo/sistemas-loginComo.component';

const sistemasRoutes: Routes = [
  { path: '', component: SistemasComponent, children: [
    { path: 'login-como', component: SistemasLoginComoComponent, canActivate: [AuthGuard]}
  ] },
];

@NgModule({
  imports: [
    RouterModule.forChild(sistemasRoutes)
  ],
  exports: [RouterModule],
  providers: [
    AuthGuard
  ]
})
export class SistemasRoutingModule {}