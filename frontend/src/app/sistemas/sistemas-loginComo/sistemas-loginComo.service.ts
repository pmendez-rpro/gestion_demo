import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
export class SistemasLoginComoService {

	constructor(private httpClient: HttpClient) { }

	// Traer lista de usuarios para loguearse como otro
	getListaUsuarios(): Observable<any>{
		const token = { token: localStorage.getItem("token") };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${environment.baseUrl}/usuarios/lista`, token);
	}
}