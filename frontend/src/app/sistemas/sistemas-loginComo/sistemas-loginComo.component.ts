import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromApp from '../../store/app.reducers';
import * as fromSistemas from '../store/sistemas.reducers';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import * as SistemasActions from '../store/sistemas.actions';
import * as SocialSigninActions from '../../socialSignin/store/socialSignin.actions';

@Component({
  selector: 'app-recipes',
  templateUrl: './sistemas-loginComo.component.html'
})
export class SistemasLoginComoComponent implements OnInit {

  private users: Array<{id: number, name: string}> = [];
  private sistemasState: Observable<fromSistemas.State>;
  private socialSiginState: Observable<fromSocialSignin.State>;



  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.sistemasState = this.store.select('sistemas');
    this.socialSiginState = this.store.select('socialSignin');
  	this.sistemasState.subscribe((res) => {
      if(res.users)
        this.users = res.users;
    });
  	this.store.dispatch(new SistemasActions.TryGetUsers());
  }

  loginComo(person_id: number){
    this.store.dispatch(new SocialSigninActions.TrySigninComo(person_id));
  }

}