import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SistemasRoutingModule } from './sistemas-routing.module';
import { SistemasComponent } from './sistemas.component';
import { SistemasLoginComoComponent } from './sistemas-loginComo/sistemas-loginComo.component';
import { SharedModule } from '../shared/shared.module';
import { SistemasLoginComoService } from './sistemas-loginComo/sistemas-loginComo.service';
//import { reducers } from '../store/app.reducers';
import { SistemasEffects } from './store/sistemas.effects';

@NgModule({
	declarations: [
		SistemasComponent,
		SistemasLoginComoComponent
	],
	imports: [
		CommonModule,
		SharedModule,
		SistemasRoutingModule,
		//StoreModule.forRoot(reducers),
		EffectsModule.forRoot([SistemasEffects])
	],
	providers: [
		SistemasLoginComoService
	]
})
export class SistemasModule {}