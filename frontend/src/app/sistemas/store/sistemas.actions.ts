import { Action } from '@ngrx/store';

export const SET_USERS = 'SET_USERS';
export const GET_USERS_ERROR = 'GET_USERS_ERROR';
export const TRY_GET_USERS = 'TRY_GET_USERS';

export class TryGetUsers implements Action {
	readonly type = TRY_GET_USERS;
}

export class SetUsers implements Action {
	readonly type = SET_USERS;
	constructor(public payload: {
		users: Array<{ id:number, name: string }>,
		error: Array<string>
	}) {}
}

export class GetUsersError implements Action {
	readonly type = GET_USERS_ERROR;
	constructor(public payload: Array<string>) {};
}

export type SistemasActions = TryGetUsers | SetUsers | GetUsersError ;