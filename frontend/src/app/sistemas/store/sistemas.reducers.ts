import * as SistemasActions from './sistemas.actions';

export interface State {
  users: Array<{ id: number, name: string}>;
  error: Array<string>;
}

const getInitialState = () => {
  return {
    users: [],
    error: ["ERR_NO_ERROR"]
  }
}

const initialState: State = getInitialState();

export function sistemasReducer(state = initialState, action: SistemasActions.SistemasActions) {
  switch (action.type) {

    case(SistemasActions.SET_USERS):
      return {
        ...state,
        users: [...action.payload.users],
        error: [...action.payload.error]
      };

    case(SistemasActions.GET_USERS_ERROR):
      return {
        ...state,
        error: [...action.payload]
      };

    default:
      return state;
  }
}