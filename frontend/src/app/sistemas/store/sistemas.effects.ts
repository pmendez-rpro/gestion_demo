import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';
import { map, tap, switchMap, mergeMap, take, catchError } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducers';
import * as SistemasActions from './sistemas.actions';
import { SistemasLoginComoService } from '../sistemas-loginComo/sistemas-loginComo.service';

@Injectable()
export class SistemasEffects {

  @Effect()
  authSignin = this.actions$
    .ofType(SistemasActions.TRY_GET_USERS)
    .pipe(
      mergeMap((action: SistemasActions.TryGetUsers) => {
        return from(this.sistemasLoginComoService.getListaUsuarios());
      }),
      mergeMap((getUsersResponse:any) => {
          if(getUsersResponse.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: SistemasActions.GET_USERS_ERROR,
                payload: getUsersResponse.error
              }
            ];
          } else {
            return [
              {
                type: SistemasActions.SET_USERS,
                payload: getUsersResponse
              }
            ];
          }
      }),
      catchError(err => {
        let errors = [];
        errors.push(err);
        throw err;
        return [
          {
            type: SistemasActions.GET_USERS_ERROR,
            payload: errors
          }
        ]
      })
     );

  constructor(
    private actions$: Actions,
    private router: Router,
    private sistemasLoginComoService: SistemasLoginComoService,
    private store: Store<fromApp.AppState>) {
  }
}