import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import Login from './login.model';

@Injectable()
export class SocialSigninService {

	constructor(private httpClient: HttpClient) { }

	// Login validando token con backend
	validateIdToken(id_token: string): Observable<any> {
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${ environment.baseUrl }/usuario/login`,
			{ id_token },
			{
     			headers: httpHeaders,
     			observe: 'response'
			}
		);
	};

	// Logout
	logOut(): Observable<any> {
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		const token = { token: localStorage.getItem("token") };
		return this.httpClient.post(`${environment.baseUrl}/usuario/logout`,
			token,
			{
     			headers: httpHeaders,
     			observe: 'response'
			}
		);
	};

	// Login como otro usuario
	signInComo(person_id: number): Observable<any> {
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		const token = localStorage.getItem("token");
		const data = { token, person_id };
		return this.httpClient.post(`${ environment.baseUrl }/usuario/login-como`,
			data,
			{
     			headers: httpHeaders,
     			observe: 'response'
			}
		);
	};

	// Chequeo de token para ingresar como desarrollador
	checkTestingToken(testingToken: string): Observable<any> {
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		const data = { token: testingToken };
		return this.httpClient.post(`${ environment.baseUrl }/usuario/datos`,
			data,
			{
				headers: httpHeaders,
				observe: 'response'
			})
	};

}
