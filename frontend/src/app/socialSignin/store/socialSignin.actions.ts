import { Action } from '@ngrx/store';

export const TRY_SIGNIN = 'TRY_SIGNIN';
export const VALIDATE_ID_TOKEN = 'VALIDATE_ID_TOKEN';
export const SET_PROFILE = 'SET_PROFILE';
export const SIGNIN_ERROR = 'SIGNIN_ERROR';
export const TRY_LOG_OUT = 'TRY_LOG_OUT';
export const LOG_OUT = 'LOG_OUT';
export const LOG_OUT_ERROR = 'LOG_OUT_ERROR';
export const TRY_SIGNIN_COMO = 'TRY_SIGNIN_COMO';
export const SIGNIN_COMO_ERROR = 'SIGNIN_COMO_ERROR';

export class TrySignin implements Action {
	readonly type = TRY_SIGNIN;
	constructor(public payload: { googleLoginProvider: string }) {}
}

export class ValidateIdToken implements Action {
	readonly type = VALIDATE_ID_TOKEN;
	constructor(public payload: string ) {}
}

export class SetProfile implements Action {
	readonly type = SET_PROFILE;
	constructor(public payload: {
		token: string,
		email: string,
		error: Array<string>,
		name: string,
		photo: string,
		area: string,
		areas: Array<string>,
		agency: string,
		agencies: Array<{ id: number, name: string }>,
		notifications: {
			social: Array<{ text: string, href: string }>,
			enterprise: Array<{ text: string, href: string }>
		},
		permissions: Array<string>
	}) {}
}

export class SigninError implements Action {
	readonly type = SIGNIN_ERROR;
	constructor(public payload: Array<string>) {};
}

export class TryLogOut implements Action {
	readonly type = TRY_LOG_OUT;
}

export class LogOut implements Action {
	readonly type = LOG_OUT;
}

export class LogOutError implements Action {
	readonly type = LOG_OUT_ERROR;
	constructor(public payload: Array<string>) {}
}

export class TrySigninComo implements Action {
	readonly type = TRY_SIGNIN_COMO;
	constructor(public payload: number){};
}

export class SigninComoError implements Action {
	readonly type = SIGNIN_COMO_ERROR;
	constructor(public payload: Array<string>){};
}

export type SocialSigninActions = TrySignin | ValidateIdToken | SetProfile | SigninError | TryLogOut | LogOut | LogOutError | TrySigninComo | SigninComoError;