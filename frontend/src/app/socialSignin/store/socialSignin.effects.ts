import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';
import { map, tap, switchMap, mergeMap, take, catchError } from 'rxjs/operators';
import { from, of } from 'rxjs';
import { AuthService, SocialUser } from 'angularx-social-login';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducers';
import * as SocialSigninActions from './socialSignin.actions';
import { SocialSigninService } from '../socialSignin.service';
import Login from '../login.model';

@Injectable()
export class SocialSigninEffects {

  @Effect()
  authSignin = this.actions$
    .ofType(SocialSigninActions.TRY_SIGNIN)
    .pipe(
      map((action: SocialSigninActions.TrySignin) => {
        /*let state;
        this.store.select('socialSignin').take(1).subscribe(s => state = s);
        if(state.error && !state.error.find(e => e === "ERR_NO_ERROR")){
          window.location.reload();
          return;
        }*/
        return action.payload;
      }),
      switchMap((socialSigninData: { googleLoginProvider: string }) => {
        return from(this.authService.signIn(socialSigninData.googleLoginProvider));
      }),
      switchMap((socialUser: SocialUser) => {
        localStorage.setItem('email', socialUser.email);
        return [
          {
            type: SocialSigninActions.VALIDATE_ID_TOKEN,
            payload: socialUser.idToken
          }
        ];
      }),
      catchError(err => {
        return [
          {
            type: SocialSigninActions.SIGNIN_ERROR,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  authLogout = this.actions$
    .ofType(SocialSigninActions.TRY_LOG_OUT)
    .pipe(
      switchMap(() => {
        return from(this.socialSiginService.logOut());
      }),
      switchMap((logoutResponse) => {
        if(logoutResponse.status == 200 && !logoutResponse.body.token){
          localStorage.removeItem('token');
          localStorage.removeItem('email');
          localStorage.removeItem('photo');
          localStorage.removeItem('name');
          localStorage.removeItem('area');
          this.router.navigate(["home"]);
          return [
              {
                type: SocialSigninActions.LOG_OUT
              }
          ]
          //return this.authService.signOut();
        } else if(logoutResponse.status == 200 && logoutResponse.body.token){
          localStorage.setItem('token', logoutResponse.body.token);
          localStorage.setItem('email', logoutResponse.body.email);
          localStorage.setItem('photo', logoutResponse.body.photo);
          localStorage.setItem('name', logoutResponse.body.name);
          localStorage.setItem('area', logoutResponse.body.area);
          return [
            {
              type: SocialSigninActions.SET_PROFILE,
              payload: logoutResponse.body
            }
          ]
        } else if(logoutResponse.status == 200 && logoutResponse.body.error){
          let errores;
          if(logoutResponse.body)
            errores = logoutResponse.body.error
          else
            errores = logoutResponse.error.error
          return [
            {
              type: SocialSigninActions.LOG_OUT_ERROR,
              payload: errores
            }
          ]
        }
      }),
      catchError(err => {
        return [
          {
            type: SocialSigninActions.LOG_OUT_ERROR,
            payload: err.error.error
          }
        ]
      })
    );

  @Effect()
  validateIdToken = this.actions$
    .ofType(SocialSigninActions.VALIDATE_ID_TOKEN)
    .pipe(
      map((action: SocialSigninActions.ValidateIdToken) => {
        return action.payload;
      }),
      mergeMap((validateTokenData: string ) => {
        return from(this.socialSiginService.validateIdToken(validateTokenData));
      }),
      mergeMap((loginResponse) => {
        if(loginResponse.status == 200){
          localStorage.setItem('token', loginResponse.body.token);
          localStorage.setItem('email', loginResponse.body.email);
          localStorage.setItem('photo', loginResponse.body.photo);
          localStorage.setItem('name', loginResponse.body.name);
          localStorage.setItem('area', loginResponse.body.area);
          return [
            {
              type: SocialSigninActions.SET_PROFILE,
              payload: loginResponse.body
            }
          ]
        } else {
          let errores;
          if(loginResponse.body)
            errores = loginResponse.body.error
          else
            errores = loginResponse.error.error
          return [
            {
              type: SocialSigninActions.SIGNIN_ERROR,
              payload: errores
            }
          ]
        }
      }),
      catchError(err => {
        return [
          {
            type: SocialSigninActions.SIGNIN_ERROR,
            payload: err.error.error
          }
        ]
      })

    );

  @Effect()
  signInComo = this.actions$
    .ofType(SocialSigninActions.TRY_SIGNIN_COMO)
    .pipe(
      map((action: SocialSigninActions.TrySigninComo) => {
        return action.payload;
      }),
      switchMap((trySigninComoData: number ) => {
        return from(this.socialSiginService.signInComo(trySigninComoData));
      }),
      mergeMap((loginComoResponse) => {
        if(loginComoResponse.status == 200){
          localStorage.setItem('token', loginComoResponse.body.token);
          localStorage.setItem('email', loginComoResponse.body.email);
          localStorage.setItem('photo', loginComoResponse.body.photo);
          localStorage.setItem('name', loginComoResponse.body.name);
          localStorage.setItem('area', loginComoResponse.body.area);
          //this.router.navigate(["home"]);
          return [
            {
              type: SocialSigninActions.SET_PROFILE,
              payload: loginComoResponse.body
            }
          ]
        } else {
          let errores;
          if(loginComoResponse.body)
            errores = loginComoResponse.body.error
          else
            errores = loginComoResponse.error.error
          return [
            {
              type: SocialSigninActions.SIGNIN_COMO_ERROR,
              payload: errores
            }
          ]
        }
      }),
      catchError(err => {
        return [
          {
            type: SocialSigninActions.SIGNIN_COMO_ERROR,
            payload: err.error.error
          }
        ]
      })

    );

  constructor(
    private actions$: Actions,
    private router: Router,
    private authService: AuthService,
    private socialSiginService: SocialSigninService,
    private store: Store<fromApp.AppState>) {
  }
}