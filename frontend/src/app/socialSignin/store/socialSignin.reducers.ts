import * as SocialSigninActions from './socialSignin.actions';

export interface State {
  token: string;
  authenticated: boolean;
  error: Array<string>;
  personalInfo: {
    email: string;
    photo: string;
    name: string;
  };
  area: string;
  areas: Array<string>;
  agency: string;
  agencies: Array<{ id: number, name: string }>;
  notifications: {
    social: Array<{ text: string, href: string }>,
    enterprise: Array<{ text: string, href: string }>
  };
  permissions: Array<string>;
}

const getInitialState = () => {
  /*return token ?
    { token, authenticated: true, error: ["ERR_NO_ERROR"], personalInfo }
    :
    { token: null, authenticated: false, personalInfo: null, error: ["ERR_NO_ERROR"]};*/
  return {
    token: null,
    authenticated: false,
    personalInfo: null,
    error: ["ERR_NO_ERROR"],
    area: null,
    areas: null,
    agency: null,
    agencies: null,
    notifications: null,
    permissions: null
  }
}

const initialState: State = getInitialState();

export function socialSigninReducer(state = initialState, action: SocialSigninActions.SocialSigninActions) {
  switch (action.type) {

    case(SocialSigninActions.LOG_OUT):
      return {
        ...state,
        token: null,
        authenticated: false,
        personalInfo: null,
        error: null,
        area: null,
        areas: null,
        agency: null,
        agencies: null,
        notifications: null,
        permissions: null
      };

    case(SocialSigninActions.SET_PROFILE):
      const { token, email, photo, name, error, area, areas, agency, agencies, notifications, permissions } = action.payload;
      return {
        ...state,
        token,
        authenticated: true,
        personalInfo: { email, photo, name },
        error: [...error],
        area,
        areas,
        agency,
        agencies,
        notifications,
        permissions
      };

    case(SocialSigninActions.SIGNIN_ERROR):
      return {
        ...state,
        error: [...action.payload]
      };

    case(SocialSigninActions.SIGNIN_COMO_ERROR):
      return {
        ...state,
        error: [...action.payload]
      };

    case(SocialSigninActions.LOG_OUT_ERROR):
      return {
        ...state,
        error: [...action.payload]
      };

    default:
      return state;
  }
}