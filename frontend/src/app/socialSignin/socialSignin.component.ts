import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { GoogleLoginProvider, SocialUser } from 'angularx-social-login';

 import * as fromApp from '../store/app.reducers';
 import * as fromSocialSignin from './store/socialSignin.reducers';
 import * as SocialSigninActions from './store/socialSignin.actions';

@Component({
  selector: 'app-socialsignin',
  templateUrl: './socialSignin.component.html',
  styleUrls: [
    './socialSignin.component.scss'
  ]
})
export class SocialSigninComponent implements OnInit, OnDestroy {

  socialSigninState: Observable<fromSocialSignin.State>;
  subscription: Subscription;

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router) { }

  ngOnInit() {
    this.socialSigninState = this.store.select('socialSignin');
    //this.subscription =  this.socialSigninState.subscribe((res) => {
      //if(res.authenticated == true)
        //this.router.navigate(["home"]);
    //});
  }

  ngOnDestroy() {
    //this.subscription.unsubscribe();
  }

  signInWithGoogle(): void {
    this.store.dispatch(new SocialSigninActions.TrySignin({ googleLoginProvider: GoogleLoginProvider.PROVIDER_ID }));
  }

}
