export default class Login {
	public token: string;
	public email: string;
	public name: string;
	public photo: string;

	constructor(token: string, email: string, name: string, photo: string) {
		this.token = token;
		this.email = email;
		this.name = name;
		this.photo = photo;
	}
}