import { Component, OnInit } from '@angular/core';
import { Store, State } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import * as fromApp from '../../store/app.reducers';
import * as SocialSigninActions from '../../socialSignin/store/socialSignin.actions';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import { SocialSigninService } from '../socialSignin.service';

@Component({
  selector: 'app-check-token',
  templateUrl: './checkToken.component.html'
})
export class CheckTokenComponent implements OnInit {

  private testingToken: string;

  constructor(
    private socialSigninService: SocialSigninService,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<fromApp.AppState>
  ) {

  }

  ngOnInit() {
    this.testingToken = this.route.snapshot.queryParams.token;
    if(this.testingToken && String(this.testingToken) != "undefined" ){
      try{
        this.socialSigninService.checkTestingToken(this.testingToken).take(1).subscribe(
          (data) => {
            if(data.status === 200 && data.body.token) {
              const token: string = data.body.token;
              const email: string = data.body.email;
              const error: Array<string> = data.body.error;
              const photo: string = data.body.photo;
              const name: string = data.body.name;
              const area: string = data.body.area;
              const areas: Array<string> = data.body.areas;
              const agency: string = data.body.agency;
              const agencies: Array<{ id: number, name: string }> = data.body.agencies;
              const notifications: {
                  social: Array<{ text: string, href: string }>,
                  enterprise: Array<{ text: string, href: string }>
              } = data.body.notifications;
              const permissions: Array<string> = data.body.permissions;
              localStorage.setItem('token', token);
              localStorage.setItem('email', email);
              localStorage.setItem('photo', photo);
              localStorage.setItem('name', name);
              localStorage.setItem('area', area);
              this.router.navigate(['home']);
              this.store.dispatch(new SocialSigninActions.SetProfile({ token, email, error, photo, name, area, areas, agency, agencies, notifications, permissions }));
            } else {
                this.router.navigate(['/']);
                /*let errores: Array<string>;
                if(data.body)
                  errores = data.body.error
                else
                  errores = data.error.error
                this.store.dispatch(new SocialSigninActions.SigninError(errores));*/
            }
          },
          (err) => { console.log("Error conectando con backend.", err); this.router.navigate(['/']); }
        );
      } catch(e){
        this.router.navigate(['/']);
        throw e;
      }
    } else {
      this.router.navigate(['/']);
    }
  }

}