import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromApp from '../../store/app.reducers';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import * as fromComercial from '../../comercial/store/comercial.reducers';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html'
})
export class ErrorComponent implements OnInit, OnDestroy {

  //Mensajes de errores
  private errors: Array<string> = [];

  private subscriptionSocialSignin: Subscription;
  private subscriptionComercial: Subscription;

  private currentErrors: Array<string> = [];

  private socialSigninState: Observable<fromSocialSignin.State>;
  private comercialState: Observable<fromComercial.State>;

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit() {
    this.socialSigninState = this.store.select('socialSignin');
    this.comercialState = this.store.select('comercial');
    //Suscripción al estado de Social Signin
    this.subscriptionSocialSignin = this.store.select(state => state.socialSignin.error).subscribe( data => {
      this.generateErrors(data);
    });
    //Suscripción al estado del módulo comercial
    this.subscriptionComercial = this.store.select(state => state.comercial.error).subscribe( data => {
      this.generateErrors(data);
    });
  }

  //Si en la lista de errores del estado existe "ERR_NO_ERROR" es porque no hay errores
  checkErrors(): boolean {
  	if(this.errors.length === 0)
  		return false;
  	const noError = this.errors.find(err => err.toLowerCase().includes("err_no_error"));
  	return noError ? false : true;
  }

  getCurrentSocialSigninState() {
    let state;
    this.socialSigninState.take(1).subscribe(s => state = s);
    return state;
  }

  getCurrentComercialState() {
    let state;
    this.comercialState.take(1).subscribe(s => state = s);
    return state;
  }

  generateErrors(data) {
    this.currentErrors = [...this.currentErrors, data];
    if(this.currentErrors && this.currentErrors.length != 0){
      this.currentErrors.forEach( e => {
        switch(e) {
          case "ERR_INVALID_LOGIN":
            let cuenta = localStorage.getItem('email');
            cuenta ?
                this.errors.push(`La cuenta ${ cuenta } no posee permisos para ingresar al sistema. Intente cerrar sesión e ingresar con una cuenta válida.`)
                :
                this.errors.push(`La cuenta con la que intentó acceder no posee permisos para ingresar al sistema. Intente cerrar sesión de esta cuenta e ingresar con una válida.`)
            break;
          case "ERR_INVALID_REQUEST":
            this.errors.push('Petición inválida, no se cuenta con todos los datos requeridos.');
            break;
          case "ERR_INTERNAL_SERVER_ERROR":
            this.errors.push('Error conectando con el servidor. Intente nuevamente en algunos instantes.');
            break;
          case "ERR_NO_PERMISSION":
            this.errors.push('No posee los permisos suficientes para realizar esta acción.');
            break;
          default:
            break;
        }
      });
    }
  };

  ngOnDestroy() {
    this.subscriptionSocialSignin.unsubscribe();
    this.subscriptionComercial.unsubscribe();
  }

}