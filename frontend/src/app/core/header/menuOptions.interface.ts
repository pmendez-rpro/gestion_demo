export default interface MenuOption {
	modulo: string,
	icon: string
}