import { Component, OnInit, ElementRef } from '@angular/core';
// import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Store, State } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as fromApp from '../../store/app.reducers';
import  MenuOption from './menuOptions.interface';
import * as fromHeader from '../../core/header/store/header.reducers';
import * as SocialSigninActions from '../../socialSignin/store/socialSignin.actions';
import * as HeaderActions from '../../core/header/store/header.actions';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import { getArea } from '../../shared/utils';

declare const $: any;

const misc: any = {
    navbar_menu_visible: 0,
    active_collapse: true,
    disabled_collapse_init: 0,
};

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private mobile_menu_visible: any = 0;
  private sidebarVisible: boolean;
  private toggleButton: any;
  //authState: Observable<fromAuth.State>;
  private menuOptions;
  private headerState: Observable<fromHeader.State>;
  private opcionesAdicionales = [];
  private socialSigninState: Observable<fromSocialSignin.State>;
  private maxHeaderOptions = 5;
  private socialNotifications: Array<{ text: string, href: string }> = [];
  private enterpriseNotifications: Array<{ text: string, href: string }> = [];
  private photoUrl: string;
  private userName: string;

  constructor(
    private store: Store<fromApp.AppState>,
    private element: ElementRef) {
    this.sidebarVisible = false;
  }

  ngOnInit() {
    const navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
    this.headerState = this.store.select('header');
    this.socialSigninState = this.store.select('socialSignin');
    this.socialSigninState.subscribe((data) => {
      this.generateHeaderElements(this.getCurrentSocialSigninState());
    });
    //this.generateHeaderElements(this.getCurrentSocialSigninState());
  }

  onLogOut() {
    this.store.dispatch(new SocialSigninActions.TryLogOut());
  }

  onChangeModulo(modulo: string, icon: string) {
    if(modulo && icon)
      this.store.dispatch(new HeaderActions.ChangeModulo({ moduloActual: modulo, icon}));
  }

  minimizeSidebar(){
    const body = document.getElementsByTagName('body')[0];

    if (misc.sidebar_mini_active === true) {
        body.classList.remove('sidebar-mini');
        misc.sidebar_mini_active = false;
    } else {
        setTimeout(function() {
            body.classList.add('sidebar-mini');
            misc.sidebar_mini_active = true;
        }, 300);
    }

    // we simulate the window Resize so the charts will get updated in realtime.
    //const simulateWindowResize = setInterval(function() {
    //    window.dispatchEvent(new Event('resize'));
    //}, 180);

    // we stop the simulation of Window Resize after the animations are completed
    //setTimeout(function() {
    //    clearInterval(simulateWindowResize);
    //}, 1000);
  }

  sidebarOpen() {
    var $toggle = document.getElementsByClassName('navbar-toggler')[0];
    const toggleButton = this.toggleButton;
    const body = document.getElementsByTagName('body')[0];
    setTimeout(function(){
        toggleButton.classList.add('toggled');
    }, 500);
    body.classList.add('nav-open');
    setTimeout(function() {
        $toggle.classList.add('toggled');
    }, 430);

    var $layer = document.createElement('div');
    $layer.setAttribute('class', 'close-layer');


    if (body.querySelectorAll('.main-panel')) {
        document.getElementsByClassName('main-panel')[0].appendChild($layer);
    }else if (body.classList.contains('off-canvas-sidebar')) {
        document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
    }

    setTimeout(function() {
        $layer.classList.add('visible');
    }, 100);

    $layer.onclick = function() { //asign a function
      body.classList.remove('nav-open');
      this.mobile_menu_visible = 0;
      this.sidebarVisible = false;

      $layer.classList.remove('visible');
      setTimeout(function() {
          $layer.remove();
          $toggle.classList.remove('toggled');
      }, 400);
    }.bind(this);

    body.classList.add('nav-open');
    this.mobile_menu_visible = 1;
    this.sidebarVisible = true;
  };

  sidebarClose() {
    var $toggle = document.getElementsByClassName('navbar-toggler')[0];
    const body = document.getElementsByTagName('body')[0];
    this.toggleButton.classList.remove('toggled');
    var $layer = document.createElement('div');
    $layer.setAttribute('class', 'close-layer');

    this.sidebarVisible = false;
    body.classList.remove('nav-open');
    // $('html').removeClass('nav-open');
    body.classList.remove('nav-open');
    if ($layer) {
        $layer.remove();
    }

    setTimeout(function() {
        $toggle.classList.remove('toggled');
    }, 400);

    this.mobile_menu_visible = 0;
  };

  sidebarToggle() {
    if (this.sidebarVisible === false) {
        this.sidebarOpen();
    } else {
        this.sidebarClose();
    }
  };

  generateHeaderElements(currentSocialSigninState) {
    let auxOptions: Array<MenuOption> = [];
    if(currentSocialSigninState.areas){
      currentSocialSigninState.areas.forEach((a) => {
        auxOptions.push(getArea(a));
      });
    }
    this.menuOptions = auxOptions;
    if(this.menuOptions && this.menuOptions.length > this.maxHeaderOptions)
      this.opcionesAdicionales = auxOptions.slice(this.maxHeaderOptions, auxOptions.length);
    if(currentSocialSigninState.notifications && currentSocialSigninState.notifications.social && currentSocialSigninState.notifications.social.length != 0)
      this.socialNotifications = currentSocialSigninState.notifications.social;
    if(currentSocialSigninState.notifications && currentSocialSigninState.notifications.enterprise && currentSocialSigninState.notifications.enterprise.length != 0)
      this.enterpriseNotifications = currentSocialSigninState.notifications.enterprise;
    if(currentSocialSigninState.personalInfo)
      this.photoUrl = currentSocialSigninState.personalInfo.photo;
    this.userName = localStorage.getItem('name');
  };

  getCurrentSocialSigninState() {
    let state;
    this.socialSigninState.take(1).subscribe(s => state = s);
    return state;
  }

}