import { Action } from '@ngrx/store';

export const CHANGE_MODULO = 'CHANGE_MODULO';

export class ChangeModulo implements Action {
  readonly type = CHANGE_MODULO;

  constructor(public payload: { moduloActual: string, icon: string }){}
}

export type HeaderActions = ChangeModulo;