import * as HeaderActions from './header.actions';
import { getArea } from '../../../shared/utils';

export interface State {
  moduloActual: string;
}

const getInitialState = () => {
  const area: string = localStorage.getItem("area");
  let aux: { modulo: string, icon: string } = { modulo: "", icon: ""};
  if(area){
    aux =  getArea(area);
    return { moduloActual: aux.modulo, icon: aux.icon };
  } else {
    aux.modulo = null;
    aux.icon = null;
  }
  return { moduloActual: aux.modulo, icon: aux.icon };
}

const initialState: State = getInitialState();

export function headerReducer(state = initialState, action: HeaderActions.HeaderActions) {
  switch (action.type) {
    case (HeaderActions.CHANGE_MODULO):
      return {
        ...state,
        moduloActual: action.payload.moduloActual,
        icon: action.payload.icon
      };
    default:
      return state;
  }
}