import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import PerfectScrollbar from 'perfect-scrollbar';

import * as fromApp from '../../store/app.reducers';
import * as fromHeader from '../header/store/header.reducers';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import * as SocialSigninActions from '../../socialSignin/store/socialSignin.actions';
import * as HeaderActions from '../header/store/header.actions';
import MenuOption from '../header/menuOptions.interface';
import { permissions } from '../../shared/permissions';
import { getArea } from '../../shared/utils';

declare const $: any;

@Component({
  selector: 'app-sidenavbar',
  templateUrl: './sidenavbar.component.html',
  styleUrls: ['./sidenavbar.component.css']
})
export class SidenavbarComponent implements OnInit {
  //authState: Observable<fromAuth.State>;

  private selectedAgency: string;
  private menuOptions;
  private headerState: Observable<fromHeader.State>;
  private socialSigninState: Observable<fromSocialSignin.State>;
  private socialNotifications: Array<{ text:string, href: string }> = [];
  private enterpriseNotifications: Array<{ text:string, href: string }> = [];
  private photoUrl: string;
  private agency: string;
  private agencies: Array<{ id: number, name: string }> = [];
  private userPermissions: Array<string> = [];
  private userName: string;

  changeAgency(index: number){
    this.selectedAgency = this.agencies[index].name;
  }

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit(){
    //this.authState = this.store.select('auth');
    this.headerState = this.store.select('header');
    this.socialSigninState = this.store.select('socialSignin');
    this.socialSigninState.subscribe((data) => {
      this.generateSideNavbarElements(this.getCurrentSocialSigninState());
    });
  }

  onLogout(){
    this.store.dispatch(new SocialSigninActions.TryLogOut());
  }

  onChangeModulo(modulo: string, icon: string){
    this.store.dispatch(new HeaderActions.ChangeModulo({ moduloActual: modulo, icon }));
  }

  isMobileMenu(): boolean {
    if($(window).width() > 991) {
      return false;
    }
    return true;
  };

  updatePS(): void {
    if(window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: .25, suppressScrollX: true });
    }
  }

  isMac(): boolean {
    let bool = false;
    if(navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
      bool = true;
    }
    return bool;
  }

  getCurrentSocialSigninState() {
    let state;
    this.socialSigninState.take(1).subscribe(s => state = s);
    return state;
  }

  generateSideNavbarElements(currentSocialSigninState) {
    let auxOptions: Array<MenuOption> = [];
    if(currentSocialSigninState.areas){
      currentSocialSigninState.areas.forEach((a) => {
        auxOptions.push(getArea(a));
      });
    }
    this.menuOptions = auxOptions;
    if(currentSocialSigninState.notifications && currentSocialSigninState.notifications.social && currentSocialSigninState.notifications.social.length != 0)
      this.socialNotifications = currentSocialSigninState.notifications.social;
    if(currentSocialSigninState.notifications && currentSocialSigninState.notifications.enterprise && currentSocialSigninState.notifications.enterprise.length != 0)
      this.enterpriseNotifications = currentSocialSigninState.notifications.enterprise;
    if(currentSocialSigninState.personalInfo)
      this.photoUrl = currentSocialSigninState.personalInfo.photo;
    this.agency = currentSocialSigninState.agency;
    this.selectedAgency = currentSocialSigninState.agency;
    this.agencies = currentSocialSigninState.agencies;
    if(currentSocialSigninState.permissions && currentSocialSigninState.permissions.length != 0)
      this.userPermissions = currentSocialSigninState.permissions;
    this.userName = localStorage.getItem('name');
  };


  hasSistemasPermissions() {
    if(this.userPermissions && this.userPermissions.length != 0){
      let permisoLoginComo = this.userPermissions.findIndex(p => p.includes("PERMISO_LOGIN_COMO"));
      return permisoLoginComo != -1 ? true : false;
    } else
      return false;
  }

  hasAltaTomadorPermission() {
    if(this.userPermissions && this.userPermissions.length != 0){
      let permisoAltaTomador = this.userPermissions.findIndex(p => p.includes("PERMISO_TOMADOR_ALTA"));
      return permisoAltaTomador != -1 ? true : false;
    } else
      return false;
  }

  comercialUser() {
    return this.userPermissions.find(p => p.includes("COMERCIAL") ? true : false);
  }


  cerrarMenu() {
    const navbarOpenCloseButton = <HTMLElement>document.querySelector('.navbar-toggler');
    const closeLayer = <HTMLElement>document.querySelector('.close-layer');
    if(this.isMobileMenu()){
      closeLayer.remove();
      navbarOpenCloseButton.click();
    }
  }
}