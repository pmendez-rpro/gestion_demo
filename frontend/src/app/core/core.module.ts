import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { SidenavbarComponent } from './sidenavbar/sidenavbar.component';
import { AppRoutingModule } from '../app-routing.module';
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    HomeComponent,
    SidenavbarComponent,
    ErrorComponent
  ],
  imports: [
    AppRoutingModule,
    CommonModule
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent,
    FooterComponent,
    SidenavbarComponent,
    ErrorComponent
  ]
})
export class CoreModule {}