import { ActionReducerMap } from '@ngrx/store';

import * as fromSocialSignin from '../socialSignin/store/socialSignin.reducers';
import * as fromHeader from '../core/header/store/header.reducers';
import * as fromSistemas from '../sistemas/store/sistemas.reducers';
import * as fromComercial from '../comercial/store/comercial.reducers';

export interface AppState {
  socialSignin: fromSocialSignin.State,
  header: fromHeader.State,
  sistemas: fromSistemas.State,
  comercial: fromComercial.State
};

export const reducers: ActionReducerMap<AppState> = {
  socialSignin: fromSocialSignin.socialSigninReducer,
  header: fromHeader.headerReducer,
  sistemas: fromSistemas.sistemasReducer,
  comercial: fromComercial.comercialReducer
};