import { AuthServiceConfig, GoogleLoginProvider, LoginOpt } from 'angularx-social-login';
import { environment } from '../environments/environment';

const google_client_id = environment.google_client_id;

const googleLoginOptions: LoginOpt = {
	ux_mode: "redirect",
	redirect_uri: "http://localhost:4200"
};

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(google_client_id)
  }
]);

export function provideConfig() {
  return config;
}