import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardComercial } from './auth-guard-comercial.service';
import { AuthGuardAltaTomador } from './form-tomador/auth-guard-alta-tomador.service';
import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { ComercialComponent } from './comercial.component';
import { BuscadorEntidadComponent } from '../shared/buscador-entidad/buscador-entidad.component';
import { FormTomadorComponent } from './form-tomador/form-tomador.component';

const comercialRoutes: Routes = [
  { path: '', component: ComercialComponent, children: [
    { path: 'buscador', component: BuscadorEntidadComponent, canActivate: [AuthGuardComercial] },
    { path: 'tomador/nuevo', component: FormTomadorComponent, canActivate: [AuthGuardAltaTomador], canDeactivate: [CanDeactivateGuard] }
  ] },
];

@NgModule({
  imports: [
    RouterModule.forChild(comercialRoutes)
  ],
  exports: [RouterModule]
})
export class ComercialRoutingModule {}
