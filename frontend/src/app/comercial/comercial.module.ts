import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { ComercialRoutingModule } from './comercial-routing.module';
import { ComercialComponent } from './comercial.component';
import { SharedModule } from '../shared/shared.module';
import { FormTomadorComponent } from './form-tomador/form-tomador.component';
import { SectorContactoPipe } from './form-tomador/sectorContacto.pipe';
import { TipoTelefonoContactoPipe } from './form-tomador/tipoTelefonoContacto.pipe';
import { ComercialService } from './comercial.service';
//import { reducers } from '../store/app.reducers';
import { ComercialEffects } from './store/comercial.effects';
import { AuthGuardComercial } from './auth-guard-comercial.service';
import { AuthGuardAltaTomador } from './form-tomador/auth-guard-alta-tomador.service';
import { CanDeactivateGuard } from './can-deactivate-guard.service';

@NgModule({
	declarations: [
		ComercialComponent,
		FormTomadorComponent,
		SectorContactoPipe,
		TipoTelefonoContactoPipe
	],
	imports: [
		CommonModule,
		SharedModule,
		ComercialRoutingModule,
		//StoreModule.forRoot(reducers),
		EffectsModule.forRoot([ComercialEffects])
	],
	providers: [
		ComercialService,
		AuthGuardAltaTomador,
		AuthGuardComercial,
		CanDeactivateGuard
	]
})
export class ComercialModule {}
