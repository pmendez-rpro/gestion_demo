import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { take, map } from 'rxjs/operators';


import * as fromApp from '../store/app.reducers';
import * as fromSocialSignin from '../socialSignin/store/socialSignin.reducers';


@Injectable()
export class AuthGuardComercial implements CanActivate {

  constructor(private store: Store<fromApp.AppState>) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    /*return this.store.select(state => state.socialSignin.permissions)
      .pipe(take(1),
        map((permissions: Array<string>) => {
          if(permissions && permissions.length > 0)
          	return permissions.findIndex((p) => { return p.includes("PERMISO_TOMADOR_ALTA")}) === -1 ? false : true;
          return false;
        }));*/
    return true;
  }

}
