import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'tipoTelefonoContactoPipe' })
export class TipoTelefonoContactoPipe implements PipeTransform {

	transform(value: any){
		switch(value){
			case 1:
				return 'Celular';
				break;
			case 2:
				return 'Fijo';
				break;
			default:
				break;
		}
	}

}