import { Component, OnInit, ViewChild, ElementRef, NgZone, ChangeDetectorRef, OnDestroy, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2';
import { SwalPartialTargets } from '@sweetalert2/ngx-sweetalert2';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { Store, State } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { PUNTUACION_NUMERO_LETRA } from '../../shared/regularExpressions';
import { ContactoTomador } from '../../models/comercial/contactoTomador';
import { Tomador } from '../../models/comercial/tomador';
import { Country } from '../../models/shared/country';
import { City } from '../../models/shared/city';
import { Province } from '../../models/shared/province';
import * as fromApp from '../../store/app.reducers';
import * as fromComercial from '../../comercial/store/comercial.reducers';
import * as ComercialActions from '../../comercial/store/comercial.actions';
import { DeactivationGuarded } from '../can-deactivate-guard.service';

@Component({
  selector: 'comercial-form-tomador',
  templateUrl: './form-tomador.component.html',
  styleUrls: ['./form-tomador.component.css']
})
export class FormTomadorComponent implements OnInit, OnDestroy, DeactivationGuarded {

  private activePill: string = 'datosInformativos';
  private formTomador: FormGroup;
  private formContactoTomador: FormGroup;
  private formInformeNosis: FormGroup;

  private razonSocialRegEx: RegExp
  		= new RegExp(`^[${PUNTUACION_NUMERO_LETRA}]([\\s]?[${PUNTUACION_NUMERO_LETRA}]+)*$`, 'i');

  private telefonoEmpresarialRegEx: RegExp = new RegExp(/^\+[1-9][0-9]{1,24}$/);
  private paginaWebRegEx: RegExp = new RegExp(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/, "gm");
  private internoRegEx: RegExp = new RegExp(/^[0-9#\*\+]+$/i);

  private calleObligatoria: boolean = true;
  private numeroCalleObligatorio: boolean = true;
  private tipoCalleText: string = "Calle";
  private paisSeleccionado: number;
  private tipoCalleSeleccionado: number;

  private paisCodigoSeleccionado: number = null;

  private contactosTomador: Array<ContactoTomador> = [];

  private dataModalContactoTomador: ContactoTomador = { sector: null, name: null, mail: null, phone_type: null, phone: null, int: null };
  private idContactoAEditar: number;
  private idContactoAEliminar: number;

  // Propiedades para manejo de Google Maps y Google Places
  private mapsLatitude: number;
  private mapsLongitude: number;
  private mapsZoom: number;
  @ViewChild('searchAddress')
  public searchAddressElementRef: ElementRef;

  // Propiedades para el manejo de Street View
  private streetViewPitch: number = 0;
  private streetViewHeading: number = 265;
  private streetViewScrollwheel: boolean = false;
  @ViewChild('streetViewMap')
  public streetViewMapElemRef: ElementRef;
  @ViewChild('streetViewPano')
  public streetViewPanoElemRef: ElementRef;
  private streetViewLocation: any = undefined;
  private streetViewPano: any = undefined;
  private streetViewMap: any = undefined;
  private streetViewMapPanorama: any = undefined;

  //Propiedades para el despliegue de opciones de paises, provincias y localidades
  private paises: Array<Country> =  [];
  private provincias: Array<Province> =  [];
  private localidades: Array<City> = [];

  //Propiedad usada para mostrar la información del nuevo tomador una vez fue cargado exitosamente.
  private tomadorCargado: { id: number, name: string } = null;

  private nosisCargado: boolean = false;

  //Propiedad usada para navegar hacia arriba en el form
  @ViewChild('tabsStructure')
  private tabsStructure: ElementRef;

  private subscription: Subscription;

  //Lista de opciones para sector de un contacto
  private opcionesSectorContacto: Array<{ sector: string, sectorId: number }> = [
    { sector: "Comercial", sectorId: 1 },
    { sector: "Cobranza", sectorId: 2 },
    { sector: "Refacturación", sectorId: 3 },
    { sector: "Ejecutivo", sectorId: 4 },
    { sector: "Otros", sectorId: 5 }
  ];

  //Lista de opciones de tipo de teléfono
  private opcionesTipoTelefono: Array<{ tipoTelefono: string, tipoTelefonoId: number }> = [
    { tipoTelefono: "Celular", tipoTelefonoId: 1 },
    { tipoTelefono: "Fijo", tipoTelefonoId: 2 }
  ];

  //Lista de opciones de tipo de ente
  private opcionesTipoEnte: Array<{ tipoEnte: string, tipoEnteId: number }> = [
    { tipoEnte: "Jurídico", tipoEnteId: 1 },
    { tipoEnte: "Físico", tipoEnteId: 2 }
  ];

  //Lista de opciones de tipo de camino
  private opcionesTipoCamino: Array<{ tipoCamino: string, tipoCaminoId: number }> = [
    { tipoCamino: "Calle", tipoCaminoId: 1 },
    { tipoCamino: "Ruta", tipoCaminoId: 2 },
    { tipoCamino: "Camino", tipoCaminoId: 3 },
    { tipoCamino: "Otro", tipoCaminoId: 4 }
  ];

  private comercialState: Observable<fromComercial.State>;

  private loading: boolean = false;

  constructor(
    public readonly swalTargets: SwalPartialTargets,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private store: Store<fromApp.AppState>,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {

    this.comercialState = this.store.select('comercial');

    //Obtención de países
    this.store.dispatch(new ComercialActions.TryGetPaises());

    //Obtención de crud token
    this.store.dispatch(new ComercialActions.TryGetTokenCrud());

    // Seteo de países
    this.subscription = this.comercialState.subscribe(s => {
      this.paises = s.paises;
      this.provincias = s.provincias;
      this.localidades = s.ciudades;
      this.tomadorCargado = s.tomador_insertado;
      if(this.tomadorCargado){
        this.displayNewTomadorInsertado();
      }
    });

    // Configuraciones places
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation().then(() => {
        this.initStreetViewMap();
        this.generateStreetViewMap();
      });
      let autoComplete = new google.maps.places.Autocomplete(this.searchAddressElementRef.nativeElement, {
        types: ['address']
      });
      autoComplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autoComplete.getPlace();

          if(place.geometry === undefined || place.geometry === null)
            return;

          this.mapsLatitude = place.geometry.location.lat();
          this.mapsLongitude = place.geometry.location.lng();
          this.mapsZoom = 15;

          this.generateStreetViewMap();

        });
      });
    });


    // Controles Formulario Tomador
  	this.formTomador = new FormGroup({
  		'tipoEnte': new FormControl(null, [
  			Validators.required
  		]),
  		'razonSocial': new FormControl(null, [
  			Validators.required,
  			Validators.pattern(this.razonSocialRegEx),
  			Validators.maxLength(200)
  		]),
      'pais': new FormControl(null, [
        Validators.required
      ]),
      'provincia': new FormControl(null, [
        Validators.required
      ]),
      'localidad': new FormControl(null, [
        Validators.required
      ]),
      'tipo_direccion': new FormControl(null, [
        Validators.required
      ]),
      'calle': new FormControl(null, [
        this.calleValidator.bind(this),
        Validators.maxLength(100)
      ]),
      'numero_calle': new FormControl(null, [
        this.numeroCalleValidator.bind(this),
        Validators.maxLength(10)
      ]),
      'codigo_postal': new FormControl(null, [
        this.codigoPostalValidator.bind(this),
        Validators.required,
        Validators.maxLength(10)
      ]),
      'descripcion_ubicacion': new FormControl(null, [
        this.descripcionUbicacionValidator.bind(this),
        Validators.maxLength(200)
      ]),
      'telefono_empresarial': new FormControl(null, [
        Validators.pattern(this.telefonoEmpresarialRegEx),
        Validators.required,
        Validators.maxLength(25)
      ]),
      'pagina_web': new FormControl(null, [
        Validators.maxLength(255),
        Validators.pattern(this.paginaWebRegEx)
      ])
  	});

    //Control Formulario Informe Nosis
    this.formInformeNosis = this.formBuilder.group({
      informeNosis: ['']
    });


    // Controles Formulario Contacto Tomador
    this.formContactoTomador = new FormGroup({
      'sector': new FormControl(this.dataModalContactoTomador.sector, [
        Validators.required
      ]),
      'nombre': new FormControl(this.dataModalContactoTomador.name, [
        Validators.required,
        Validators.maxLength(200),
        Validators.pattern(this.razonSocialRegEx)
      ]),
      'mail': new FormControl(this.dataModalContactoTomador.mail, [
        Validators.required,
        Validators.maxLength(100),
        Validators.email
      ]),
      'tipo_telefono': new FormControl(this.dataModalContactoTomador.phone_type, [
        Validators.required
      ]),
      'numero_telefono': new FormControl(`+${this.paisCodigoSeleccionado}${this.dataModalContactoTomador.phone}`, [
        Validators.required,
        Validators.pattern(this.telefonoEmpresarialRegEx),
        Validators.maxLength(25)
      ]),
      'interno': new FormControl(this.dataModalContactoTomador.int, [
        Validators.maxLength(10),
        Validators.pattern(this.internoRegEx)
      ])
    });

  }

  //Cambiar la pill (o tab) activa
  changeActivePill(pill: string) {
  	this.activePill = pill;
  }

  //Cambio en el select de país
  changePaisSeleccionado(pais: number) {
    this.paisSeleccionado = pais;
    this.formTomador.controls.codigo_postal.updateValueAndValidity();
    this.store.dispatch(new ComercialActions.TryGetProvincias(pais));
    // Seteo de codigo pais seleccionado
    this.paises.map((p) => {
      if(p.id == pais && p.prefix)
        this.paisCodigoSeleccionado = p.prefix
    });
    this.formTomador.controls.telefono_empresarial.setValue(`+${this.paisCodigoSeleccionado}`);
  }

  //Cambio en el select de tipo de camino
  changeTipoCalleSeleccionado(tipoCalle: number) {
    this.tipoCalleSeleccionado = tipoCalle;
    this.formTomador.controls.calle.setValue("");
    this.formTomador.controls.numero_calle.setValue("");
    this.formTomador.controls.calle.updateValueAndValidity();
    this.formTomador.controls.numero_calle.updateValueAndValidity();
    this.formTomador.controls.descripcion_ubicacion.updateValueAndValidity();
  }

  //Chequeo de calle y numero obligatorios dependiendo el tipo de camino seleccionado
  checkRoadType(event) {
    let calleLabel = <HTMLElement>document.querySelector('#form_entidad_tomador_direccion_calle_label');
    if(event == 4){
      this.calleObligatoria = false;
      this.numeroCalleObligatorio = false;
    } else {
      this.calleObligatoria = true;
      this.numeroCalleObligatorio = true;
      if(event == 1){
        calleLabel.innerText = `Calle (*)`;
        this.tipoCalleText = `Calle`;
      }
      if(event == 2){
        calleLabel.innerText = `Ruta (*)`;
        this.tipoCalleText = `Ruta`;
      }
      if(event == 3){
        calleLabel.innerText = `Camino Rural (*)`;
        this.tipoCalleText = `Camino Rural`;
      }
    }
  }

  //Validador custom para el campo calle
  calleValidator(control: AbstractControl): { [key: string]: any } | null {
    let valid = false;
    let tipo = this.tipoCalleSeleccionado;
    if(tipo == 1 || tipo == 2 || tipo == 3 || !tipo){
      if(!control.value || control.value.toString().length == 0)
        return { calleObligatoria: { valid: false, value: control.value }};
    }
    if(tipo == 4){
      if(control.value || (control.value && control.value.toString().length != 0))
        return { calleVacia: { valid: false, value: control.value }};
    }
    return null;
  }

  //Validador custom para el campo número de calle
  numeroCalleValidator(control: AbstractControl): { [key: string]: any } | null {
    let valid = false;
    let tipo = this.tipoCalleSeleccionado;
    if(tipo == 1 || tipo == 2 || tipo == 3 || !tipo){
      if(!control.value || control.value.length == 0)
        return { numeroCalleObligatorio: { valid: false, value: control.value }};
    }
    if(tipo == 4){
      if(control.value || (control.value && control.value.length != 0))
        return { numeroCalleVacio: { valid: false, value: control.value }};
      return null;
    }
    return null;
  }

  //Validador custom para el campo código postal
  codigoPostalValidator(control: AbstractControl): { [key: string]: any } | null {
    let valid = false;
    if(this.paisSeleccionado && this.paisSeleccionado == 9){
      if(control.value && control.value.toString().length > 4)
        return { invalidCodigoPostalArgentina: { valid: false, value: control.value }};
      if(control.value && control.value.toString().length > 0){
        if(isNaN(control.value.toString()))
          return { invalidCodigoPostalArgentina: { valid: false, value: control.value }};
      };
    } else if(this.paisSeleccionado && this.paisSeleccionado != 9){
      if(control.value && control.value.toString().length > 10)
        return { invalidCodigoPostalExtranjero: { valid: false, value: control.value }};
    }
    if(control.value && control.value.toString().length > 10)
      return { invalidLength: { valid: false, value: control.value } };
  }

  //Validador custom para el campo descripción ubicación
  descripcionUbicacionValidator(control: AbstractControl): { [key: string]: any } | null {
    let valid = false;
    let tipo = this.tipoCalleSeleccionado;
    if(tipo == 4){
      if(!control.value || control.value.length == 0)
        return { descripcionObligatoria: { valid: false, value: control.value }};
    }
    return null;
  }

  //Evento disparado al tocar botón de eliminar contacto de tomador
  deleteContacto(){
    this.contactosTomador.splice(this.idContactoAEliminar, 1);
    Swal.fire({
      title: "¡Eliminado!",
      text: "El contacto ha sido eliminado.",
      timer: 5000,
      type: "success"
    });
  }

  //Seteo del index del contacto del tomador a eliminar
  setIdDeleteContacto(index: number) {
    this.idContactoAEliminar = index;
  }

  //Seteo de ids en botones de sweet alert
  onOpenSwal() {
    const cancelBtn = <HTMLElement>document.querySelector('.swal2-cancel');
    if(cancelBtn)
      cancelBtn.setAttribute("id", "modal_contacto_tomador_cancel_button");
    const confirmBtn = <HTMLElement>document.querySelector('.swal2-confirm');
    if(confirmBtn)
      confirmBtn.setAttribute("id", "modal_contacto_tomador_confirm_button");
  }

  //Evento disparado al tocar el botón crear en el swal de nuevo contacto de tomador
  newContacto() {
    const sector = this.formContactoTomador.get('sector').value;
    const nombre = this.formContactoTomador.get('nombre').value.trim();
    const mail = this.formContactoTomador.get('mail').value.trim();
    const tipo_telefono = this.formContactoTomador.get('tipo_telefono').value;
    const numero_telefono = this.formContactoTomador.get('numero_telefono').value.trim();
    const interno = this.formContactoTomador.get('interno').value ? this.formContactoTomador.get('interno').value.trim() : null;
    const newContacto: ContactoTomador = new ContactoTomador(
      sector,
      nombre,
      mail,
      tipo_telefono,
      numero_telefono,
      interno
    );
    this.contactosTomador.push(newContacto);
    this.formContactoTomador.reset();
    Swal.fire({
      title: "¡Creado!",
      text: "El contacto ha sido creado.",
      timer: 5000,
      type: "success"
    });
  }

  //Seteo del index del contacto del tomador a editar
  setIdEditContacto(index: number) {
    this.idContactoAEditar = index;
  }

  //Evento disparado al tocar en botón de editar contacto de tomador
  handleEditContacto() {
    this.formContactoTomador.get('sector').setValue(this.contactosTomador[this.idContactoAEditar].sector);
    this.formContactoTomador.get('nombre').setValue(this.contactosTomador[this.idContactoAEditar].name);
    this.formContactoTomador.get('mail').setValue(this.contactosTomador[this.idContactoAEditar].mail);;
    this.formContactoTomador.get('tipo_telefono').setValue(this.contactosTomador[this.idContactoAEditar].phone_type);
    this.formContactoTomador.get('numero_telefono').setValue(this.contactosTomador[this.idContactoAEditar].phone);
    this.formContactoTomador.get('interno').setValue(this.contactosTomador[this.idContactoAEditar].int);
  }

  //Evento disparado al tocar en botón de nuevo contacto de tomador
  handleNewContacto() {
    this.formContactoTomador.reset();
    if(this.paisCodigoSeleccionado)
      this.formContactoTomador.get('numero_telefono').setValue(`+${this.paisCodigoSeleccionado}`);
    else
      this.formContactoTomador.get('numero_telefono').setValue(null);
  }

  //Evento disparado al tocar en botón de editar contacto de tomador
  editContacto() {
    this.contactosTomador[this.idContactoAEditar].sector = +this.formContactoTomador.get('sector').value;
    this.contactosTomador[this.idContactoAEditar].name = this.formContactoTomador.get('nombre').value;
    this.contactosTomador[this.idContactoAEditar].mail = this.formContactoTomador.get('mail').value;
    this.contactosTomador[this.idContactoAEditar].phone_type = +this.formContactoTomador.get('tipo_telefono').value;
    this.contactosTomador[this.idContactoAEditar].phone = this.formContactoTomador.get('numero_telefono').value;
    this.contactosTomador[this.idContactoAEditar].int = this.formContactoTomador.get('interno').value;
    Swal.fire({
      title: "¡Modificado!",
      text: "El contacto ha sido modificado.",
      timer: 5000,
      type: "success"
    });
    this.formContactoTomador.reset();
  }

  //Forzar cierre de modal actual abierto
  closeModal(){
    Swal.close();
  }

  //Seteo de la ubicación actual en base a las coordenadas del navegador
  setCurrentLocation() {
    if('geolocation' in navigator) {
      return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition((position) => {
          this.mapsLatitude = position.coords.latitude;
          this.mapsLongitude = position.coords.longitude;
          this.mapsZoom = 15;
          resolve();
        }, reject);
      });
    }
  }

  //Cuando se coloca la chinche en el mapa
  markerDragEnd($event: MouseEvent) {
    this.mapsLatitude = $event.coords.lat;
    this.mapsLongitude = $event.coords.lng;
    this.generateStreetViewMap();
  }

  //Generar Mapa de Street View
  generateStreetViewMap() {
    let center = { lat: +this.mapsLatitude, lng: +this.mapsLongitude };
    let sv = new window['google'].maps.StreetViewService();
    sv.getPanorama({location: center, radius: 50}, processSVData.bind(this));
    function processSVData (data, status) {
      if (status === 'OK') {
        this.streetViewMapPanorama.setPano(data.location.pano);
        this.streetViewLocation = data.location.pano;
        this.streetViewPano = data.location.pano;
        this.streetViewMap.setStreetView(this.streetViewMapPanorama);
        this.streetViewMapPanorama.setVisible(true);
      } else {
        this.streetViewMapPanorama.setVisible(false);
        this.streetViewLocation = undefined;
        this.streetViewPano = undefined;
      }
    }
  }

  //Inicialización de map y de panorama para Street View
  initStreetViewMap() {
    let center = { lat: +this.mapsLatitude, lng: +this.mapsLongitude };
    let panorama = new window['google'].maps.StreetViewPanorama(this.streetViewPanoElemRef.nativeElement, {
      position: center,
      pov: {
        heading: 270,
        pitch: 0
      },
      fullscreenControl: false,
      motionTrackingControl: false,
      linksControl: false
    });
    this.streetViewMapPanorama = panorama;
    //Seteo del mapa
    let map = new window['google'].maps.Map(this.streetViewMapElemRef.nativeElement, {
      center,
      zoom: this.mapsZoom,
      streetViewControl: false
    });
    this.streetViewMap = map;
  }


  //Cuando hay un cambio en el zoom en el mapa
  zoomChanged(event){
    this.mapsZoom = event;
  }

  //Cuando se detecta un cambio en la localidad seleccionada
  /*changeLocalidadSeleccionada(index: number) {
    this.localidadSeleccionada = index;
  }*/

  //Cuando se detecta un cambio en la provincia seleccionada
  changeProvinciaSeleccionada(localidad: number) {
    this.store.dispatch(new ComercialActions.TryGetLocalidades(localidad));
  }

  //Cuando se realiza el submit del formulario de alta tomador
  onSubmit(){
    let newTomador;
    const tipoEnte = this.formTomador.get('tipoEnte').value;
    const razonSocial = this.formTomador.get('razonSocial').value.trim();
    const localidad = this.formTomador.get('localidad').value;
    const tipoDireccion = this.formTomador.get('tipo_direccion').value;
    const calle = this.formTomador.get('calle').value ? this.formTomador.get('calle').value.trim() : null;
    const numeroCalle = this.formTomador.get('numero_calle').value ? this.formTomador.get('numero_calle').value.trim() : null;
    const descripcionUbicacion = this.formTomador.get('descripcion_ubicacion').value ? this.formTomador.get('descripcion_ubicacion').value.trim() : null;
    const codigoPostal = this.formTomador.get('codigo_postal').value.trim();
    const telefonoEmpresarial = this.formTomador.get('telefono_empresarial').value.trim();
    const paginaWeb = this.formTomador.get('pagina_web').value ? this.formTomador.get('pagina_web').value.trim() : null;
    newTomador = new Tomador(
      tipoEnte,
      razonSocial,
      localidad,
      tipoDireccion,
      calle,
      numeroCalle,
      descripcionUbicacion,
      codigoPostal,
      telefonoEmpresarial,
      this.mapsLatitude,
      this.mapsLongitude,
      this.mapsZoom,
      //Street View Latitude es la misma que maps
      this.streetViewLocation ? this.mapsLatitude : null,
      //Street View Longitude es la misma que maps
      this.streetViewLocation ? this.mapsLongitude : null,
      //Street View Heading
      this.streetViewLocation ? this.streetViewHeading : null,
      //Street View Pitch
      this.streetViewLocation ? this.streetViewPitch : null,
      //Street View Zoom es el mismo que maps
      this.streetViewLocation ? this.mapsZoom : null,
      paginaWeb,
      this.contactosTomador
    );
    this.store.dispatch(new ComercialActions.TryInsertarTomador(newTomador));
  }

  //Cuando se sube un nuevo archivo de informe Nosis
  onInformeNosisChange(event) {
    this.nosisCargado = true;
    if(event.target.files.length > 0) {
      const file = event.target.files[0];
      this.formInformeNosis.get('informeNosis').setValue(file);
      const formData = new FormData();
      formData.append('file', this.formInformeNosis.get('informeNosis').value);
      this.store.dispatch(new ComercialActions.TryEnvioInformeNosis(formData));
    }
  }

  //Al destruir el componente, cierro la suscripción
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  //Detectar evento de cierre de pestaña del navegador
  @HostListener('window:beforeunload', ['$event'])
  onWindowClose(event: any) {
    if(this.formTomador.dirty){
      event.returnValue = true;
    }
  }

  //Chequear si el form tiene datos cargados
  hasUnsavedData(): boolean {
    if(this.formTomador.dirty || this.formContactoTomador.dirty || this.formInformeNosis.dirty)
      return true;
    return false;
  }

  //Implementación de canDeactivate para poder evitar la pérdida de información en la carga del formulario
  canDeactivate(): boolean | Observable<boolean> | Promise<boolean> {
    if(this.hasUnsavedData()){
      const waitForResponse = new Promise(
        (resolve, reject) => {
          Swal.fire({
            title: "¡Atención!",
            text: "Si continúa perderá la información del tomador cargada hasta el momento.",
            showCancelButton: true,
            cancelButtonText: "Volver",
            confirmButtonText: "Continuar",
            type: "warning",
            onOpen: (modal) => {
              const cancelBtn = <HTMLElement>document.querySelector('.swal2-cancel');
              if(cancelBtn)
                cancelBtn.setAttribute("id", "modal_contacto_tomador_cancel_button");
              const confirmBtn = <HTMLElement>document.querySelector('.swal2-confirm');
              if(confirmBtn)
                confirmBtn.setAttribute("id", "modal_contacto_tomador_confirm_button");
            }
          }).then((result) => {
            if(result.value){
              resolve();
            } else if (result.dismiss === Swal.DismissReason.cancel || result.dismiss === Swal.DismissReason.backdrop || result.dismiss === Swal.DismissReason.esc || result.dismiss === Swal.DismissReason.close){
              reject();
            }
          });
        }
      );
      return waitForResponse.then(() => true).catch(() => false);
    } else {
      return true;
    }
  }

  //Mensaje disparado al haber confirmación de un nuevo tomador insertado
  displayNewTomadorInsertado(){
    if(this.tomadorCargado){
      Swal.fire({
        title: "¡Tomador cargado exitosamente!",
        text: `Nuevo tomador con id: ${this.tomadorCargado.id} y razón social: ${this.tomadorCargado.name} cargado exitosamente.`,
        timer: 10000,
        type: "success",
        onOpen: (modal) => {
          const cancelBtn = <HTMLElement>document.querySelector('.swal2-cancel');
          if(cancelBtn)
            cancelBtn.setAttribute("id", "modal_contacto_tomador_cancel_button");
          const confirmBtn = <HTMLElement>document.querySelector('.swal2-confirm');
          if(confirmBtn)
            confirmBtn.setAttribute("id", "modal_contacto_tomador_confirm_button");
        }
      });
    }
  }

  //Ir hacia arriba arriba en el formulario
  goToTabsStructure(){
    this.tabsStructure.nativeElement.scrollIntoView({ behaviour: 'smooth', block: 'end', inline: 'start'});
  }

}