import { PipeTransform, Pipe } from '@angular/core';

@Pipe({ name: 'sectorContactoPipe' })
export class SectorContactoPipe implements PipeTransform {

	transform(value: any){
		switch(value){
			case 1:
				return 'Comercial';
				break;
			case 2:
				return 'Cobranza';
				break;
			case 3:
				return 'Refacturación';
				break;
			case 4:
				return 'Ejecutivo';
				break;
			case 5:
				return 'Otros';
				break;
			default:
				break;
		}
	}

}