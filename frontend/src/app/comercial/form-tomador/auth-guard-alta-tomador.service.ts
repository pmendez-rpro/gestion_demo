import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { take, map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { SwalPartialTargets } from '@sweetalert2/ngx-sweetalert2';
import { Router } from '@angular/router';

import * as fromApp from '../../store/app.reducers';
import * as fromSocialSignin from '../../socialSignin/store/socialSignin.reducers';
import { FormTomadorComponent } from './form-tomador.component';

@Injectable()
export class AuthGuardAltaTomador implements CanActivate{

  constructor(private store: Store<fromApp.AppState>,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.store.select(state => state.socialSignin.permissions)
      .pipe(
        take(1),
        map((permissions: Array<string>) => {
          if(permissions && permissions.length > 0)
            if (permissions.findIndex((p) => { return p.includes("PERMISO_TOMADOR_ALTA")}) == -1) {
              return false;
            } else {
              return true;
            }
          return false;
        })
      );
  }

}
