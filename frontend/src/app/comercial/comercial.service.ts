import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { Tomador } from '../models/comercial/tomador';

@Injectable()
export class ComercialService {

	constructor(private httpClient: HttpClient) { }

	// Traer token crud antes de cualquier operación en un ABM
	getTokenCrud(): Observable<any>{
		const token = { token: localStorage.getItem("token") };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${environment.baseUrl}/crud/obtener-token`, token, { headers: httpHeaders, observe: 'response'});
	}


	// Traer lista de países
	getPaises(): Observable<any>{
		const token = { token: localStorage.getItem("token") };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${environment.baseUrl}/geo/paises`, token, { headers: httpHeaders, observe: 'response'});
	}

	// Traer lista de provincias
	getProvincias(countryId: number): Observable<any>{
		const data = { token: localStorage.getItem("token"), country: countryId };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${environment.baseUrl}/geo/provincias`, data, { headers: httpHeaders, observe: 'response'});
	}

	// Traer lista de localidades
	getLocalidades(provinceId: number): Observable<any>{
		const data = { token: localStorage.getItem("token"), province: provinceId };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.post(`${environment.baseUrl}/geo/localidades`, data, { headers: httpHeaders, observe: 'response'});
	}

	// Insertar un nuevo tomador
	insertarTomador(tomador: Tomador, crud_token: string): Observable<any>{
		const { entity_type,
				name,
				city,
				street_type,
				street,
				street_number,
				description,
				zip_code,
				phone,
				maps_latitude,
				maps_longitude,
				maps_zoom,
				street_view_latitude,
				street_view_longitude,
				street_view_heading,
				street_view_pitch,
				street_view_zoom,
				web,
				contacts
		} = tomador;
		const data = {
			token: localStorage.getItem("token"),
			crud_token,
			entity_type,
			name,
			city,
			street_type,
			street,
			street_number,
			description,
			zip_code,
			phone,
			maps_latitude,
			maps_longitude,
			maps_zoom,
			street_view_latitude,
			street_view_longitude,
			street_view_heading,
			street_view_pitch,
			street_view_zoom,
			web,
			contacts
		};
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json'
		});
		return this.httpClient.put(`${environment.baseUrl}/tomador/insertar`, data, { headers: httpHeaders, observe: 'response'});

	}

	// Enviar informe Nosis
	enviarInformeNosis(formData: FormData): Observable<any>{
		formData.append('token', localStorage.getItem("token"));
		formData.append('reason', "REASON_NOSIS");
		return this.httpClient.post(`${environment.baseUrl}/archivo/enviar`, formData, { observe: 'response'});
	}

}