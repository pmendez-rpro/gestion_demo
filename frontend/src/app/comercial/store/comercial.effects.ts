import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';
import { map, tap, switchMap, mergeMap, take, catchError } from 'rxjs/operators';
import { from, of, Observable } from 'rxjs';
import { Store, State } from '@ngrx/store';

import * as fromApp from '../../store/app.reducers';
import * as fromComercial from './comercial.reducers';
import * as ComercialActions from './comercial.actions';
import { ComercialService } from '../comercial.service';

@Injectable()
export class ComercialEffects {

  private comercialState: Observable<fromComercial.State>;

  @Effect()
  getTokenCrud = this.actions$
    .ofType(ComercialActions.TRY_GET_TOKEN_CRUD)
    .pipe(
      mergeMap((action: ComercialActions.TryGetTokenCrud) => {
        return from(this.comercialService.getTokenCrud());
      }),
      mergeMap((getTokenCrudResponse:any) => {
          if(getTokenCrudResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_GET_TOKEN_CRUD,
                payload: getTokenCrudResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_TOKEN_CRUD,
                payload: getTokenCrudResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_GET_TOKEN_CRUD,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  getPaises = this.actions$
    .ofType(ComercialActions.TRY_GET_PAISES)
    .pipe(
      mergeMap((action: ComercialActions.TryGetPaises) => {
        return from(this.comercialService.getPaises());
      }),
      mergeMap((getPaisesResponse:any) => {
          if(getPaisesResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_GET_PAISES,
                payload: getPaisesResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_PAISES,
                payload: getPaisesResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_GET_PAISES,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  getProvincias = this.actions$
    .ofType(ComercialActions.TRY_GET_PROVINCIAS)
    .pipe(
      mergeMap((action: ComercialActions.TryGetProvincias) => {
        return from(this.comercialService.getProvincias(action.payload));
      }),
      mergeMap((getProvinciasResponse:any) => {
          if(getProvinciasResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_GET_PROVINCIAS,
                payload: getProvinciasResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_PROVINCIAS,
                payload: getProvinciasResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_GET_PROVINCIAS,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  getLocalidades = this.actions$
    .ofType(ComercialActions.TRY_GET_LOCALIDADES)
    .pipe(
      mergeMap((action: ComercialActions.TryGetLocalidades) => {
        return from(this.comercialService.getLocalidades(action.payload));
      }),
      mergeMap((getLocalidadesResponse:any) => {
          if(getLocalidadesResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_GET_LOCALIDADES,
                payload: getLocalidadesResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_LOCALIDADES,
                payload: getLocalidadesResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_GET_LOCALIDADES,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  insertarTomador = this.actions$
    .ofType(ComercialActions.TRY_INSERTAR_TOMADOR)
    .pipe(
      mergeMap((action: ComercialActions.TryInsertarTomador) => {
        let crud_token: string;
        this.comercialState.take(1).subscribe(s => crud_token = s.crud_token);
        if(!crud_token){
          return [
              {
                type: ComercialActions.ERROR_INSERTAR_TOMADOR,
                payload: ["ERR_NO_CRUD_TOKEN"]
              }
          ];
        }
        return from(this.comercialService.insertarTomador(action.payload, crud_token));
      }),
      mergeMap((insertarTomadorResponse:any) => {
          if(insertarTomadorResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_INSERTAR_TOMADOR,
                payload: insertarTomadorResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_TOMADOR_INSERTADO,
                payload: insertarTomadorResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_INSERTAR_TOMADOR,
            payload: err.error.error
          }
        ]
      })
     );

  @Effect()
  enviarInformeNosis = this.actions$
    .ofType(ComercialActions.TRY_ENVIO_INFORME_NOSIS)
    .pipe(
      mergeMap((action: ComercialActions.TryEnvioInformeNosis) => {
        let crud_token: string;
        this.comercialState.take(1).subscribe(s => crud_token = s.crud_token);
        if(!crud_token){
          return [
              {
                type: ComercialActions.ERROR_ENVIO_INFORME_NOSIS,
                payload: ["ERR_NO_CRUD_TOKEN"]
              }
          ];
        }
        action.payload.append('crud_token', crud_token);
        return from(this.comercialService.enviarInformeNosis(action.payload));
      }),
      mergeMap((envioInformeNosisResponse:any) => {
          if(envioInformeNosisResponse.body.error[0] != "ERR_NO_ERROR"){
            return [
              {
                type: ComercialActions.ERROR_ENVIO_INFORME_NOSIS,
                payload: envioInformeNosisResponse.body.error
              }
            ];
          } else {
            return [
              {
                type: ComercialActions.SET_URL_INFORME_NOSIS,
                payload: envioInformeNosisResponse.body
              }
            ];
          }
      }),
      catchError(err => {
        return [
          {
            type: ComercialActions.ERROR_ENVIO_INFORME_NOSIS,
            payload: err.error.error
          }
        ]
      })
     );

  constructor(
    private actions$: Actions,
    private router: Router,
    private comercialService: ComercialService,
    private store: Store<fromApp.AppState>) {
    this.comercialState = store.select('comercial');
  }


}