import * as ComercialActions from './comercial.actions';
import { Country } from '../../models/shared/country';
import { Province } from '../../models/shared/province';
import { City } from '../../models/shared/city';
import { Tomador } from '../../models/comercial/tomador';

export interface State {
  crud_token: string;
  error: Array<string>;
  paises: Array<Country>;
  provincias: Array<Province>;
  ciudades: Array<City>;
  tomador_insertado: { id: number, name: string };
  url_informe_nosis: string;
}

const getInitialState = () => {
  return {
    crud_token: null,
    error: ["ERR_NO_ERROR"],
    paises: [],
    provincias: [],
    ciudades: [],
    tomador_insertado: null,
    url_informe_nosis: null
  }
}

const initialState: State = getInitialState();

export function comercialReducer(state = initialState, action: ComercialActions.ComercialActions) {
  switch (action.type) {

    //Una vez obtenido el token del crud se setea el mismo en el estado
    case(ComercialActions.SET_TOKEN_CRUD):
      return {
        ...state,
        crud_token: action.payload.crud_token,
        error: [...action.payload.error]
      };

    //Error en la obtención del crud token
    case(ComercialActions.ERROR_GET_TOKEN_CRUD):
      return {
        ...state,
        error: [...action.payload]
      };


    case(ComercialActions.SET_PAISES):
      return {
        ...state,
        paises: [...action.payload.countries],
        error: [...action.payload.error]
      };

    //Error en la obtención de los países
    case(ComercialActions.ERROR_GET_PAISES):
      return {
        ...state,
        error: [...action.payload]
      };

    case(ComercialActions.SET_PROVINCIAS):
      return {
        ...state,
        provincias: [...action.payload.provinces],
        error: [...action.payload.error]
      };

    //Error en la obtención de las provincias
    case(ComercialActions.ERROR_GET_PROVINCIAS):
      return {
        ...state,
        error: [...action.payload]
      };

    case(ComercialActions.SET_LOCALIDADES):
      return {
        ...state,
        ciudades: [...action.payload.cities],
        error: [...action.payload.error]
      };

    //Error en la obtención de las localidades
    case(ComercialActions.ERROR_GET_LOCALIDADES):
      return {
        ...state,
        error: [...action.payload]
      };

    case(ComercialActions.SET_TOMADOR_INSERTADO):
      return {
        ...state,
        tomador_insertado: action.payload.data,
        error: [...action.payload.error]
      };

    //Error en la inserción del tomador
    case(ComercialActions.ERROR_INSERTAR_TOMADOR):
      return {
        ...state,
        error: [...action.payload]
      };

    case(ComercialActions.SET_URL_INFORME_NOSIS):
      return {
        ...state,
        url_informe_nosis: action.payload.url,
        error: [...action.payload.error]
      };

    case(ComercialActions.ERROR_ENVIO_INFORME_NOSIS):
      return {
        ...state,
        error: [...action.payload]
      };

    default:
      return state;
  }
}