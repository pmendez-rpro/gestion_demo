import { Action } from '@ngrx/store';

import { Country } from '../../models/shared/country';
import { Province } from '../../models/shared/province';
import { City } from '../../models/shared/city';
import { Tomador } from '../../models/comercial/tomador';

export const TRY_GET_TOKEN_CRUD = 'TRY_GET_TOKEN_CRUD';
export const SET_TOKEN_CRUD = 'SET_TOKEN_CRUD';
export const ERROR_GET_TOKEN_CRUD = 'ERROR_GET_TOKEN_CRUD';

export const TRY_GET_PAISES = 'TRY_GET_PAISES';
export const SET_PAISES = 'SET_PAISES';
export const ERROR_GET_PAISES = 'ERROR_GET_PAISES';

export const TRY_GET_PROVINCIAS = 'TRY_GET_PROVINCIAS';
export const SET_PROVINCIAS = 'SET_PROVINCIAS';
export const ERROR_GET_PROVINCIAS = 'ERROR_GET_PROVINCIAS';

export const TRY_GET_LOCALIDADES = 'TRY_GET_LOCALIDADES';
export const SET_LOCALIDADES = 'SET_LOCALIDADES';
export const ERROR_GET_LOCALIDADES = 'ERROR_GET_LOCALIDADES';

export const TRY_INSERTAR_TOMADOR = 'TRY_INSERTAR_TOMADOR';
export const SET_TOMADOR_INSERTADO = 'SET_TOMADOR_INSERTADO';
export const ERROR_INSERTAR_TOMADOR = 'ERROR_INSERTAR_TOMADOR';

export const TRY_ENVIO_INFORME_NOSIS = 'TRY_ENVIO_INFORME_NOSIS';
export const SET_URL_INFORME_NOSIS = 'SET_URL_INFORME_NOSIS';
export const ERROR_ENVIO_INFORME_NOSIS = 'ERROR_ENVIO_INFORME_NOSIS';

export class TryGetTokenCrud implements Action {
	readonly type = TRY_GET_TOKEN_CRUD;
}

export class SetTokenCrud implements Action {
	readonly type = SET_TOKEN_CRUD;
	constructor(public payload: {
		crud_token: string,
		error: Array<string>
	}) {}
}

export class ErrorGetTokenCrud implements Action {
	readonly type = ERROR_GET_TOKEN_CRUD;
	constructor(public payload: Array<string>) {};
}

export class TryGetPaises implements Action {
	readonly type = TRY_GET_PAISES;
}

export class SetPaises implements Action {
	readonly type = SET_PAISES;
	constructor(public payload: {
		countries: Array<Country>,
		error: Array<string>
	}) {}
}

export class ErrorGetPaises implements Action {
	readonly type = ERROR_GET_PAISES;
	constructor(public payload: Array<string>) {};
}

export class TryGetProvincias implements Action {
	readonly type = TRY_GET_PROVINCIAS;
	constructor(public payload: number) {};
}

export class SetProvincias implements Action {
	readonly type = SET_PROVINCIAS;
	constructor(public payload: {
		provinces: Array<Province>,
		error: Array<string>
	}) {}
}

export class ErrorGetProvincias implements Action {
	readonly type = ERROR_GET_PROVINCIAS;
	constructor(public payload: Array<string>) {};
}

export class TryGetLocalidades implements Action {
	readonly type = TRY_GET_LOCALIDADES;
	constructor(public payload: number) {};
}

export class SetLocalidades implements Action {
	readonly type = SET_LOCALIDADES;
	constructor(public payload: {
		cities: Array<City>,
		error: Array<string>
	}) {}
}

export class ErrorGetLocalidades implements Action {
	readonly type = ERROR_GET_LOCALIDADES;
	constructor(public payload: Array<string>) {};
}

export class TryInsertarTomador implements Action {
	readonly type = TRY_INSERTAR_TOMADOR;
	constructor(public payload: Tomador) {};
}

export class SetTomadorInsertado implements Action {
	readonly type = SET_TOMADOR_INSERTADO;
	constructor(public payload: {
		data: { id: number, name: string },
		error: Array<string>
	}) {}
}

export class ErrorInsertarTomador implements Action {
	readonly type = ERROR_INSERTAR_TOMADOR;
	constructor(public payload: Array<string>) {};
}

export class TryEnvioInformeNosis implements Action {
	readonly type = TRY_ENVIO_INFORME_NOSIS;
	constructor(public payload: FormData) {};
}

export class SetUrlInformeNosis implements Action {
	readonly type = SET_URL_INFORME_NOSIS;
	constructor(public payload: {
		url: string,
		error: Array<string>
	}) {}
}

export class ErrorEnvioInformeNosis implements Action {
	readonly type = ERROR_ENVIO_INFORME_NOSIS;
	constructor(public payload: Array<string>) {};
}

export type ComercialActions =
	TryGetTokenCrud |
	SetTokenCrud |
	ErrorGetTokenCrud |
	TryGetPaises |
	SetPaises |
	ErrorGetPaises |
	TryGetProvincias |
	SetProvincias |
	ErrorGetProvincias |
	TryGetLocalidades |
	SetLocalidades |
	ErrorGetLocalidades |
	TryInsertarTomador |
	SetTomadorInsertado |
	ErrorInsertarTomador |
	TryEnvioInformeNosis |
	SetUrlInformeNosis |
	ErrorEnvioInformeNosis;