export class Country {

	id: number;
	name: string;
	prefix: number;

	constructor(id: number, name: string, prefix: number) {
		this.id = id;
		this.name = name;
		this.prefix = prefix;
	}

}