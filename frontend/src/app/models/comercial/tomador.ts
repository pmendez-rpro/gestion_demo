import { ContactoTomador } from './contactoTomador';

export class Tomador {

	entity_type: number;
	name: string;
	city: number;
	street_type: number;
	street: string;
	street_number: string;
	description: string;
	zip_code: string;
	phone: string;
	maps_latitude: number;
	maps_longitude: number;
	maps_zoom: number;
	street_view_latitude: number;
	street_view_longitude: number;
	street_view_heading: number;
	street_view_pitch: number;
	street_view_zoom: number;
	web: string;
	contacts: Array<ContactoTomador>;

	constructor(entity_type: number,
				name: string,
				city: number,
				street_type: number,
				street: string,
				street_number: string,
				description: string,
				zip_code: string,
				phone: string,
				maps_latitude: number,
				maps_longitude: number,
				maps_zoom: number,
				street_view_latitude: number,
				street_view_longitude: number,
				street_view_heading: number,
				street_view_pitch: number,
				street_view_zoom: number,
				web: string,
				contacts: Array<ContactoTomador>)
	{
		this.entity_type = entity_type;
		this.name = name;
		this.city = city;
		this.street_type = street_type;
		this.street = street;
		this.street_number = street_number;
		this.description = description;
		this.zip_code = zip_code;
		this.phone = phone;
		this.maps_latitude = maps_latitude;
		this.maps_longitude = maps_longitude;
		this.maps_zoom = maps_zoom;
		this.street_view_latitude = street_view_latitude;
		this.street_view_longitude = street_view_longitude;
		this.street_view_heading = street_view_heading;
		this.street_view_pitch = street_view_pitch;
		this.street_view_zoom = street_view_zoom;
		this.web = web;
		this.contacts = contacts;
	}

}