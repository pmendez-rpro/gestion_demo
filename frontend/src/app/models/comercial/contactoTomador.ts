export class ContactoTomador {

	sector: number;
	name: string;
	mail: string;
	phone_type: number;
	phone: string;
	int: string;

	constructor(sector: number, name: string, mail: string, phone_type: number, phone: string, int: string) {
		this.sector = +sector;
		this.name = name;
		this.mail = mail;
		this.phone_type = +phone_type;
		this.phone = phone;
		this.int = int;
	}

}