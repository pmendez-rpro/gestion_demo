import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { CoreModule } from '../core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AgmCoreModule } from '@agm/core';

import { MatSelectModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { BuscadorEntidadComponent } from './buscador-entidad/buscador-entidad.component';
import { BuscadorEntidadService } from './buscador-entidad/buscador-entidad.service';

@NgModule({
  declarations: [
  	BuscadorEntidadComponent
  ],
  imports: [
  	CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    SweetAlert2Module,
    AgmCoreModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    SweetAlert2Module,
    AgmCoreModule
  ],
  providers: [
    BuscadorEntidadService
  ]
})
export class SharedModule {}
