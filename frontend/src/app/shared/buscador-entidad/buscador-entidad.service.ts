import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpResponse } from '@angular/common/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable()
export class BuscadorEntidadService {

	constructor(private httpClient: HttpClient) { }

	// Traer lista de entidades existentes con CUIT o Razón Social
	getEntities(searchText: string): Observable<any>{
    const token = localStorage.getItem("token");
		const data = { token, search: searchText };
		let httpHeaders = new HttpHeaders({
     		'Content-Type' : 'application/json',
		});
    return this.httpClient.post(`${environment.baseUrl}/entidades/lista`,
      data,
      {
        headers: httpHeaders,
        observe: 'response'
      });
	}
}
