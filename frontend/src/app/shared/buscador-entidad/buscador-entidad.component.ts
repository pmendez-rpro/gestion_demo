import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';

import { BuscadorEntidadService } from './buscador-entidad.service';

@Component({
  selector: 'app-buscador-entidad',
  templateUrl: './buscador-entidad.component.html'
})
export class BuscadorEntidadComponent implements OnInit {

  private searchEntityForm: FormGroup;
  private existingEntities: Array<{ id: number, text: string }> = [];

  constructor(
    private buscadorEntidadService: BuscadorEntidadService,
    private router: Router
  ) { }

  ngOnInit() {
    this.searchEntityForm = new FormGroup({
      'searchText': new FormControl(null, [
        Validators.maxLength(199)
      ])
    });
  }

  sendSearchText(text: string) {
    if(text.length > 3 && text.length <= 200){
      this.buscadorEntidadService.getEntities(text).subscribe((entities) => {
        this.existingEntities = entities.body.entities;
      });
    } else {
      this.existingEntities = [];
    }
  }

  goToEntityForm() {
    this.router.navigate(['comercial', 'tomador', 'nuevo']);
  }

}
