export const signosPuntuacion = "!\"#$%&'()*+,-./:;<=>?@\\[^_\`{|}~¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿×÷";
export const numeros = "0-9";
export const letras = "a-z";
export const letrasAcentuadas = "áéíñóúü";
export const letrasLatinas = "āăąàâãäåæćĉċčçďđēĕėęěèêëĝğġģĥħĩīĭįıìîïĳĵķĸĺļľŀłðńņňŉŋōŏőœòôõöøŕŗřśŝşšſßţťŧũūŭůűųùûŵŷÿýźżžþƀƁƃƄƃƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯưƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿȀȁȂȃȄȅȆȇȈȉȊȋȌȍȎȏȐȑȒȓȔȕȖȗȘșȚțȜȝȞȟȠȡȢȣȤȥȦȧȨȩȪȫȬȭȮȯȰȱȲȳȴȵȶȷȸȹȺȻȼȽȾȿɀɁɂɃɄɅɆɇɈɉɊɋɌɍɎɏ";
export const todasLasLetrasLatinas = `${letras}${letrasAcentuadas}${letrasLatinas}`;
export const PUNTUACION_NUMERO_LETRA = `${signosPuntuacion}${numeros}${todasLasLetrasLatinas}`;