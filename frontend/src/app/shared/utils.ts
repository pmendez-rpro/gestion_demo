export function getArea(paramArea: string): {modulo: string, icon: string } {
	switch(paramArea.toUpperCase()) {
        case "COMITE_EJECUTIVO":
          return { modulo: "Comité Ejecutivo", icon: "./assets/img/comite.svg" };
          break;
        case "RRCLC":
          return { modulo: "Rrclc", icon: "./assets/img/rrclc.svg" };
          break;
        case "ASEGURADOS":
          return { modulo: "Asegurados", icon: "./assets/img/asegurados.svg" };
          break;
        case "REASEGUROS":
          return { modulo: "Reaseguros", icon: "./assets/img/reaseguros.svg" };
          break;
        case "SUMINISTROS":
          return { modulo: "Suministros", icon: "./assets/img/suministros.svg" };
          break;
        case "COMERCIAL":
          return { modulo: "Comercial", icon: "./assets/img/comercial.svg" };
          break;
        case "CONTROL_Y_SUSCRIPCIONES":
          return { modulo: "Control y Suscripciones", icon: "./assets/img/control.svg" };
          break;
        case "LEGALES":
          return { modulo: "Legales", icon: "./assets/img/legales.svg" };
          break;
        case "TESORERIA":
          return { modulo: "Tesorería", icon: "./assets/img/tesoreria.svg" };
          break;
        case "CONTADURIA":
          return { modulo: "Contaduría", icon: "./assets/img/contaduria.svg" };
          break;
        case "ATENCION_AL_CLIENTE":
          return { modulo: "Atención al Cliente", icon: "./assets/img/atencionAlCliente.svg" };
          break;
        case "SISTEMAS":
          return { modulo: "Sistemas", icon: "./assets/img/sistemas.svg" };
          break;
        default:
          return { modulo: "Default", icon: ""}
          break;
        }
}