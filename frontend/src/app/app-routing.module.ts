import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './socialSignin/auth-guard.service';
import { SocialSigninComponent} from './socialSignin/socialSignin.component';
import { HomeComponent } from './core/home/home.component';
//import { BuscadorEntidadComponent } from './shared/buscador-entidad/buscador-entidad.component'

const routes: Routes = [
  { path: '', component: SocialSigninComponent},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'sistemas', loadChildren: './sistemas/sistemas.module#SistemasModule' },
  { path: 'comercial', loadChildren: './comercial/comercial.module#ComercialModule'},
  //{ path: 'comercial/buscador', component: BuscadorEntidadComponent},
  //{ path: 'comite', loadChildren: './comite/comite.module#ComiteModule'},
  //{ path: 'rrclc', loadChildren: './rrclc/rrclc.module#RrclcModule'},
  //{ path: 'asegurados', loadChildren: './asegurados/asegurados.module#AseguradosModule'},
  //{ path: 'legales', loadChildren: './legales/legales.module#LegalesModule'},
  //{ path: 'suministros', loadChildren: './suministros/suministros.module#SuministrosModule'},
  //{ path: 'controlYSusc', loadChildren: './controlYSusc/controlYSusc.module#ControlYSuscModule'},
  //{ path: 'tesoreria', loadChildren: './tesoreria/tesoreria.module#TesoreriaModule'},
  //{ path: 'reaseguro', loadChildren: './reaseguro/reaseguro.module#ReaseguroModule'},
  //{ path: 'contaduria', loadChildren: './contaduria/contaduria.module#ContaduriaModule'},
  //{ path: 'atencionAlCliente', loadChildren: './atencionAlCliente/atencionAlCliente.module#AtencionAlClienteModule'},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
