import { Component, AfterViewInit, OnInit } from '@angular/core';
import { SocketService } from './socket.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import PerfectScrollbar from 'perfect-scrollbar';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../environments/environment';
import * as fromSocialSignin from './socialSignin/store/socialSignin.reducers';
import * as fromApp from './store/app.reducers';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  loadedFeature = '';
  socialSigninState: Observable<fromSocialSignin.State>;
  private authenticated: boolean;
  private queryParams;

  constructor(
    private store: Store<fromApp.AppState>,
    private socketService: SocketService,
    private route: ActivatedRoute){

  }


  ngOnInit() {
    this.socialSigninState = this.store.select('socialSignin');
    const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
    const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
    const html = document.getElementsByTagName('html')[0];
    this.authenticated = localStorage.getItem("token") ? true : false;
    this.queryParams = this.route.snapshot.queryParams;
    this.route.queryParams.subscribe((params) => {
      this.queryParams = params;
    });
  }

  ngAfterViewInit() {
    this.runOnRouteChange();
  }

  onNavigate(feature: string){
    this.loadedFeature = feature;
  }

  runOnRouteChange(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
      const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
      const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
      let ps
      if(elemMainPanel)
         ps = new PerfectScrollbar(elemMainPanel);
      if(elemSidebar)
         ps = new PerfectScrollbar(elemSidebar);
      if(ps)
         ps.update();
    }
  }

  isMac(): boolean {
      let bool = false;
      if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
          bool = true;
      }
      return bool;
  }

}
