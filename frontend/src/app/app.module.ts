import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
//import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { AgmCoreModule } from '@agm/core';

import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { reducers } from './store/app.reducers';
import { environment } from '../environments/environment';
import { SocialSigninEffects } from './socialSignin/store/socialSignin.effects';
import { SocialSigninComponent } from './socialSignin/socialSignin.component';
import { SocialSigninService } from './socialSignin/socialSignin.service';
import { provideConfig } from './socialLoginConfig';
import { AuthGuard } from './socialSignin/auth-guard.service';
import { SocketService } from './socket.service';
import { CheckTokenComponent } from './socialSignin/checkToken/checkToken.component';

@NgModule({
  declarations: [
    AppComponent,
    SocialSigninComponent,
    CheckTokenComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([SocialSigninEffects]),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    SocialLoginModule,
    SharedModule,
    SweetAlert2Module.forRoot({
        buttonsStyling: false,
        customClass: 'modal-content',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger'
    }),
    AgmCoreModule.forRoot({
      //apiKey: '123',
      apiKey: environment.google_api_key,
      libraries: ['places']
    })
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },
    AuthGuard,
    SocialSigninService,
	  SocketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
