<?php
	namespace App\Rules;
	
	use Illuminate\Contracts\Validation\Rule;
	use App\Models\GeoModel;
	use App\Classes\General\Functions;
	
	class Zipcode implements Rule {
		/**
		* Determine if the validation rule passes.
		*
		* @param  string  $attribute
		* @param  mixed  $value
		* @return bool
		*/
		
		private $city;
		private $attribute;
		
		public function __construct(int $city) {
			$this->city = $city;
		}
		
		public function passes($attribute, $value) {
			$this->attribute = $attribute;
			$geo = new GeoModel();
			$result = $geo->country_by_city($this->city);
			if (!isset($result["pais"])) {
				return false;
			}
			else if ($result["pais"] == "Argentina") {
				return preg_match("/^[1-9][0-9]{3}$/", $value);
			}
			else {
				return preg_match("/^" . Functions::regex("foreign_zipcode") . "$/", $value);
			}
		}
		
		/**
		* Get the validation error message.
		*
		* @return string
		*/
		public function message() {
			return $this->attribute;
		}
	}
?>