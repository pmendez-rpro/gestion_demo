<?php
    namespace App\Models;
	
	use Illuminate\Support\Facades\DB;
	use App\Classes\General\Functions;
    
    class EntitiesModel {
        private $lang;
	
        public function __construct(string $lang) {
            $this->lang = $lang;
        }
		
		public function list(string $search): array {
			try {
				$res = DB::table("entidades_busqueda")
								->select(
									"id",
									"text"
								)
								->where("razon_social", "LIKE", "%" . Functions::escape_query($search) . "%")
								->orWhere("documento", "LIKE", "%" . Functions::escape_query($search) . "%")
								->orderBy("razon_social", "ASC")
								->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = [
						"id" => $r->id,
						"text" => $r->text
					];
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
    }
?>