<?php
    namespace App\Models;
	
	use Illuminate\Support\Facades\DB;
	use App\Classes\General\Functions;
	use App\Rules\Zipcode;
	use Validator;
	use Illuminate\Validation\Rule;
	use Illuminate\Support\Facades\Cache;
	use App\Classes\GoogleDrive\GoogleDrive;
	use App\Models\EmpleadosModel;
	
    class TomadoresModel {
        private $lang;
		
        public function __construct(string $lang) {
            $this->lang = $lang;
        }
		
		public function insert(array $input): array {
			try {
				$regex_name = Functions::regex("business_name");
				$regex_phone = Functions::regex("phone");
				$regex_phone_int = Functions::regex("phone_int");
				$errors = [];
				
				$validator = Validator::make(
					$input,
					[
						"token" => [
							"required",
							"string"
						],
						"crud_token" => [
							"required",
							"string"
						],
						"entity_type" => [
							"required",
							"integer",
							"exists:tipos_de_ente,id"
						],
						"name" => [
							"required",
							"string",
							"max:200",
							"regex:/^$regex_name$/i"
						],
						"city" => [
							"required",
							"integer",
							"gt:0",
							"exists:localidades,id"
						],
						"street_type" => [
							"required",
							"integer",
							"exists:tipos_de_calle,id"
						],
						"street" => [
							"required_unless:street_type,4",
							"nullable",
							"string",
							"max:100",
							"regex:/^$regex_name$/i"
						],
						"street_number" => [
							"required_unless:street_type,4",
							"nullable",
							"string",
							"max:10",
							"regex:/^$regex_name$/i"
						],
						"description" => [
							"required_if:street_type,4",
							"nullable",
							"string",
							"max:200",
							"regex:/^$regex_name$/i"
						],
						"zip_code" => [
							"required",
							new Zipcode((int)$input["city"])
						],
						"phone" => [
							"required",
							"string",
							"max:25",
							"regex:/^$regex_phone$/i"
						],
						"maps_latitude" => [
							"required",
							"numeric",
							"min:-90",
							"max:90"
						],
						"maps_longitude" => [
							"required",
							"numeric",
							"min:-180",
							"max:180"
						],
						"maps_zoom" => [
							"required",
							"integer",
							"min:0"
						],
						"street_view_latitude" => [
							"numeric",
							"nullable",
							"min:-90",
							"max:90"
						],
						"street_view_longitude" => [
							"numeric",
							"nullable",
							"min:-180",
							"max:180"
						],
						"street_view_heading" => [
							"required_with:street_view_zoom",
							"numeric",
							"nullable",
							"min:-999.99",
							"max:999.99"
						],
						"street_view_pitch" => [
							"required_with:street_view_heading",
							"numeric",
							"nullable",
							"min:-999.99",
							"max:999.99"
						],
						"street_view_zoom" => [
							"required_with:street_view_pitch",
							"integer",
							"nullable",
							"min:0"
						],
						"web" => [
							"url",
							"nullable",
							"max:255"
						]
					]
				);
				
				if ($validator->fails()) $errors = $validator->errors()->all();
				
				if (isset($input["contacts"]) && is_array($input["contacts"]) && count($input["contacts"]) > 0) {
					foreach ($input["contacts"] as $contact) {
						$validator = Validator::make(
							$contact,
							[
								"sector" => [
									"required",
									"integer",
									"exists:contactos_personales_sectores,id"
								],
								"name" => [
									"required",
									"string",
									"max:200",
									"regex:/^$regex_name$/i"
								],
								"mail" => [
									"required",
									"email",
									"max:100"
								],
								"phone_type" => [
									"required",
									"integer",
									"exists:tipos_de_telefono,id"
								],
								"phone" => [
									"required",
									"string",
									"max:25",
									"regex:/^$regex_phone$/i"
								],
								"int" => [
									"string",
									"nullable",
									"max:10",
									"regex:/^$regex_phone_int$/i"
								]
							]
						);
						
						if ($validator->fails()) {
							$errs = $validator->errors()->all();
							foreach ($errs as $err) {
								$errors[] = "contacts_$err";
							}
						}
					}
				}
				
				//Verifica la presencia del archivo de Nosis:
				$nosis = glob("/usr/share/nginx/html/dist/temp/{$input["crud_token"]}/nosis_*");
				if (count($nosis) == 0) {
					$errors[] = "nosis";
				}
				else {
					sort($nosis, SORT_STRING);
					$nosis = array_pop($nosis);
				}
					
				$errors = array_unique($errors);
				if (count($errors) > 0) {
					$temp = [];
					foreach ($errors as $error) {
						$temp[] = "ERR_" . mb_strtoupper($error);
					}
					return [
						"errors" => $temp
					];
				}
				
				try {
					$retorno = [];
					DB::transaction(function() use ($input, $nosis, &$retorno) {
						$person_id = DB::table("personas")
									->insertGetId([
										"tipo_de_ente" => $input["entity_type"],
										"razon_social" => $input["name"],
										"tipo_de_calle" => $input["street_type"],
										"calle" => (isset($input["street"]) && $input["street"] != "") ? $input["street"] : null,
										"altura" => (isset($input["street_number"]) && $input["street_number"] != "") ? $input["street_number"] : null,
										"direccion_descripcion" => (isset($input["description"]) && $input["description"] != "") ? $input["description"] : null,
										"codigo_postal" => $input["zip_code"],
										"localidad" => $input["city"],
										"telefono" => $input["phone"],
										"maps_latitud" => $input["maps_latitude"],
										"maps_longitud" => $input["maps_longitude"],
										"maps_zoom" => $input["maps_zoom"],
										"street_view_heading" => $input["street_view_heading"] ?? null,
										"street_view_pitch" => $input["street_view_pitch"] ?? null,
										"street_view_zoom" => $input["street_view_zoom"] ?? null,
										"web" => (isset($input["web"]) && $input["web"] != "") ? $input["web"] : null,
										"nosis" => "x"
									]);
						
						$tomador_id = DB::table("tomadores")
									->insertGetId([
										"persona" => $person_id
									]);
						
						if (isset($input["contacts"]) && is_array($input["contacts"]) && count($input["contacts"]) > 0) {
							foreach ($input["contacts"] as $contact) {
								DB::table("tomadores_contactos_personales")
									->insert([
										"tomador" => $tomador_id,
										"sector" => $contact["sector"],
										"razon_social" => $contact["name"],
										"email" => $contact["mail"],
										"tipo_de_telefono" => $contact["phone_type"],
										"telefono" => $contact["phone"],
										"interno" => (isset($input["int"]) && $input["int"] != "") ? $input["int"] : null
									]);
							}
						}
						
						//Sube el archivo de Nosis:
						$folder_code = GoogleDrive::create_folder($person_id, env("GOOGLE_DRIVE_PERSONAS"));
						$file_code = GoogleDrive::upload($nosis, pathinfo($nosis, PATHINFO_BASENAME), $folder_code);
						
						DB::table("personas")
								->where("id", $person_id)
								->update(["nosis" => "{$folder_code}@{$file_code}"]);
						
						//Borra el directorio del CRUD:
						@shell_exec("rm -fr " . pathinfo($nosis, PATHINFO_DIRNAME));
						
						//Envío de mails:
						$empleados = new EmpleadosModel($this->lang);
						
						//Verifica si debe enviar los mails al desarrollador (parent):
						$emulated_email = "";
						$data = Cache::get("user_token_{$input["token"]}");
						if (isset($data["ancestor"])) {
							$data = Cache::get("user_token_{$data["ancestor"]}");
							if (isset($data["email"])) $emulated_email = $data["email"];
						}
						
						//Envía mails al sector comercial:
						//... A futuro, pues es necesario saber cuáles son las agencias intervinientes (APODERADO Y VICE APODERADO Y GERENTE) ...
						
						//Envía mails al Sector de Control:
						$mails = array_merge(
							$empleados->notification_receivers([900000], ["Control"], ["Control"], ["Gerente"]),
							$empleados->notification_receivers([900000], ["Control"], ["Control de Suscripción"], ["Responsable de área"])
						);
						if (count($mails) > 0) {
							if ($emulated_email != "") {
								$temp = [];
								foreach ($mails as $mail) {
									$temp[] = preg_replace("/([^<]+)<[^>]+>/i", "$1 <$emulated_email>", $mail);
								}
								$mails = $temp;
							}
						}
						
						Functions::mail(
							"Notificaciones del Sistema Integral - DEV <no-reply@info.??????.com.ar>", //from
							$mails, //to
							[], //cc
							[], //bcc
							"Sistema Integral (DEV): notificación de alta de tomador", //subject
							"Estimado: se ha dado de alta al tomador Nº $tomador_id (" . htmlspecialchars($input["name"]) . ") y es necesario establecer su aspecto técnico.", //text o html
							"html",
							[] //attachment
						);
						
						//Envía mails al sector Tesorería:
						$mails = array_merge(
							$empleados->notification_receivers([900000], ["Tesorería"], ["Tesorería"], ["Responsable de área"]),
							$empleados->notification_receivers([900000], ["Finanzas"], ["Finanzas"], ["Gerente"])
						);
						if (count($mails) > 0) {
							if ($emulated_email != "") {
								$temp = [];
								foreach ($mails as $mail) {
									$temp[] = preg_replace("/([^<]+)<[^>]+>/i", "$1 <$emulated_email>", $mail);
								}
								$mails = $temp;
							}
						}
						
						Functions::mail(
							"Notificaciones del Sistema Integral - DEV <no-reply@info.??????.com.ar>", //from
							$mails, //to
							[], //cc
							[], //bcc
							"Sistema Integral (DEV): notificación de alta de tomador", //subject
							"Estimado: se ha dado de alta al tomador Nº $tomador_id (" . htmlspecialchars($input["name"]) . ") y es necesario establecer su aspecto de cobranza.", //text o html
							"html",
							[] //attachment
						);
						
						$retorno = [
							"id" => $tomador_id,
							"name" => $input["name"]
						];
					});
					
					return $retorno;
				}
				catch(\Illuminate\Database\QueryException $ex) {
					return [
						"errors" => ["ERR_INTERNAL_ERROR"]
					];
				}
			}
			catch (Exception $e) {
				return [
					"errors" => ["ERR_INTERNAL_ERROR"]
				];
			}
		}
    }
?>