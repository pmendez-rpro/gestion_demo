<?php
    namespace App\Models;
	
	use Illuminate\Support\Facades\DB;
	//use App\Classes\General\Functions;
    
    class EmpleadosModel {
        private $lang;
	
        public function __construct(string $lang) {
            $this->lang = $lang;
        }
		
		public function notification_receivers(array $agency, array $sector, array $area, array $puesto): array {
			try {
				$res = DB::table("destinatarios_notificaciones")
								->select(
									DB::raw("CONCAT(razon_social, ' <', email, '>') AS destino")
								);
				if (count($agency) > 0) $res = $res->whereIn("agencia", $agency);
				if (count($sector) > 0) $res = $res->whereIn("sector", $sector);
				if (count($area) > 0) $res = $res->whereIn("area", $area);
				if (count($puesto) > 0) $res = $res->whereIn("puesto", $puesto);
				$res = $res->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = $r->destino;
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
    }
?>