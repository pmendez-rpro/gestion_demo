<?php
    namespace App\Models;
	
	use Illuminate\Support\Facades\DB;
	//use App\Classes\General\Functions;
    
    class GeoModel {
        private $lang;
	
        public function __construct(string $lang = "es") {
            $this->lang = $lang;
        }
		
		public function countries(): array {
			try {
				$res = DB::table("paises_lista")
								->select(
									"id",
									"name",
									"prefix"
								)
								->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = [
						"id" => $r->id,
						"name" => $r->name,
						"prefix" => $r->prefix
					];
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
		
		public function country_by_city(int $city): array {
			try {
				$res = DB::table("localidad_por_codigo_pais")
								->select(
									"id",
									"pais"
								)
								->where("localidad_id", $city)
								->get();
				if (count($res) == 0) return [];
				return (array)$res[0];
			}
			catch (Exception $e) {
				return [];
			}
		}
		
		public function provinces(int $country): array {
			try {
				$res = DB::table("provincias_lista")
								->select(
									"id",
									"name"
								)
								->where("pais_prefijo_telefonico", $country)
								->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = [
						"id" => $r->id,
						"name" => $r->name
					];
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
		
		public function cities(int $province): array {
			try {
				$res = DB::table("localidades_lista")
								->select(
									"id",
									"name"
								)
								->where("provincia", $province)
								->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = [
						"id" => $r->id,
						"name" => $r->name
					];
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
    }
?>