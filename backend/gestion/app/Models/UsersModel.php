<?php
    namespace App\Models;
	
	use Illuminate\Support\Facades\Cache;
	use Illuminate\Support\Facades\DB;
	use App\Classes\General\Functions;
    
    class UsersModel {
        private $lang;
	
        public function __construct(string $lang) {
            $this->lang = $lang;
        }
        
        public function login(string $email, string $photo) {
			try {
				$res = DB::table("usuarios_logins_validos")
								->select("*")
								->where("email", $email)
								->get();
				if (count($res) != 1) return false;
				
				//Obtiene un Token y lo guarda en Memcached, junto al "id", al email y al nombre de la persona:
				$token = Functions::get_token();
				$user = [
					"id" => $res[0]->id,
					"token" => $token,
					"email" => $res[0]->email,
					"name" => $res[0]->razon_social,
					"photo" => $photo,
					"area_nombre" => $res[0]->area_nombre,
					"area_constante" => $res[0]->area_constante,
					"agencia_nombre" => $res[0]->agencia_nombre,
					"permisos_nombres" => mb_substr($res[0]->permisos_nombres, 1, mb_strlen($res[0]->permisos_nombres) - 2)
				];
				Cache::put("user_token_$token", $user, 60 * 60 * 24);
				
				return $user;
			}
			catch (Exception $e) {
				return false;
			}
		}
		
		public function login_as(int $person_id, string $ancestor_token, string $ancestor_email) {
			try {
				$res = DB::table("usuarios_logins_validos")
								->select("*")
								->where("id", $person_id)
								->get();
				if (count($res) != 1) return false;
				
				//Obtiene un Token y lo guarda en Memcached, junto al "id", al email y al nombre de la persona:
				$token = Functions::get_token();
				$user = [
					"id" => $res[0]->id,
					"token" => $token,
					"email" => $ancestor_email,
					"name" => $res[0]->razon_social,
					"area_nombre" => $res[0]->area_nombre,
					"area_constante" => $res[0]->area_constante,
					"agencia_nombre" => $res[0]->agencia_nombre,
					"permisos_nombres" => mb_substr($res[0]->permisos_nombres, 1, mb_strlen($res[0]->permisos_nombres) - 2),
					"ancestor" => $ancestor_token
				];
				Cache::put("user_token_$token", $user, 60 * 60 * 24);
				
				return $user;
			}
			catch (Exception $e) {
				return false;
			}
		}
		
		public function list(): array {
			try {
				$res = DB::table("usuarios_logins_validos")
								->select(
									"id",
									"razon_social AS name"
								)
								->orderBy("razon_social", "ASC")
								->get();
				if (count($res) == 0) return [];
				$temp = [];
				foreach ($res as $r) {
					$temp[] = [
						"id" => $r->id,
						"name" => $r->name
					];
				}
				$res = $temp;
				
				return $res;
			}
			catch (Exception $e) {
				return [];
			}
		}
    }
?>