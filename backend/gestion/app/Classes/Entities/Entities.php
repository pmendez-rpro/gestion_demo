<?php
	namespace App\Classes\Entities;
	
    use App\Models\EntitiesModel;
	
	class Entities {
		private $lang;
		
		public function __construct(string $lang) {
			$this->lang = $lang;
		}
		
		public function list(string $search): array {
			$entities = new EntitiesModel($this->lang);
			return $entities->list($search);
    	}
	}
?>