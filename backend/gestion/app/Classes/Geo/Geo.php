<?php
	namespace App\Classes\Geo;
	
    use App\Models\GeoModel;
	
	class Geo {
		private $lang;
		
		public function __construct(string $lang) {
			$this->lang = $lang;
		}
		
		public function countries(): array {
			$geo = new GeoModel($this->lang);
			return $geo->countries();
    	}
		
		public function provinces(int $country): array {
			$geo = new GeoModel($this->lang);
			return $geo->provinces($country);
    	}
		
		public function cities(int $province): array {
			$geo = new GeoModel($this->lang);
			return $geo->cities($province);
    	}
	}
?>