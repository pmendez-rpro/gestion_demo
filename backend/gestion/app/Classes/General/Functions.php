<?php
	namespace App\Classes\General;
	
	use GuzzleHttp\Exception\GuzzleException;
    use GuzzleHttp\Client;
	
	class Functions {
		public const REGEX_TYPE_PUNCT = "!\"#\$%&'\(\)\*\+,\-\.\/:;<=>\?@\\\[\^_`{}~¡¢£¤¥¦§¨©ª«¬­®¯°±²³´µ¶·¸¹º»¼½¾¿×÷\|";
		public const REGEX_TYPE_NUMBERS = "0-9";
		public const REGEX_TYPE_BASIC_LETTERS = "a-z";
		public const REGEX_TYPE_SPANISH_LETTERS = "áéíñóúü";
		public const REGEX_TYPE_OTHER_LATIN_LETTERS = "āăąàâãäåæćĉċčçďđēĕėęěèêëĝğġģĥħĩīĭįıìîïĳĵķĸĺļľŀłðńņňŉŋōŏőœòôõöøŕŗřśŝşšſßţťŧũūŭůűųùûŵŷÿýźżžþƀƁƃƄƃƅƆƇƈƉƊƋƌƍƎƏƐƑƒƓƔƕƖƗƘƙƚƛƜƝƞƟƠơƢƣƤƥƦƧƨƩƪƫƬƭƮƯưƱƲƳƴƵƶƷƸƹƺƻƼƽƾƿǀǁǂǃǄǅǆǇǈǉǊǋǌǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǝǞǟǠǡǢǣǤǥǦǧǨǩǪǫǬǭǮǯǰǱǲǳǴǵǶǷǸǹǺǻǼǽǾǿȀȁȂȃȄȅȆȇȈȉȊȋȌȍȎȏȐȑȒȓȔȕȖȗȘșȚțȜȝȞȟȠȡȢȣȤȥȦȧȨȩȪȫȬȭȮȯȰȱȲȳȴȵȶȷȸȹȺȻȼȽȾȿɀɁɂɃɄɅɆɇɈɉɊɋɌɍɎɏ";
		public const REGEX_TYPE_ALL_LATIN_SYMBOLS = self::REGEX_TYPE_PUNCT . self::REGEX_TYPE_NUMBERS . self::REGEX_TYPE_BASIC_LETTERS . self::REGEX_TYPE_SPANISH_LETTERS . self::REGEX_TYPE_OTHER_LATIN_LETTERS;
		public const REGEX_TYPE_ALL_LATIN_LETTERS_WITH_NUMBERS = self::REGEX_TYPE_NUMBERS . self::REGEX_TYPE_BASIC_LETTERS . self::REGEX_TYPE_SPANISH_LETTERS . self::REGEX_TYPE_OTHER_LATIN_LETTERS;
		
    	static public function get_token(): string {
			$symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
			$return = "";
			for ($x = 1; $x <= 64; $x++) {
				$return .= mb_substr($symbols, mt_rand(0, mb_strlen($symbols) - 1), 1);
			}
			
			return $return;
    	}
		
		static public function get_crud_token(): string {
			$symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			$return = "";
			for ($x = 1; $x <= 64; $x++) {
				$return .= mb_substr($symbols, mt_rand(0, mb_strlen($symbols) - 1), 1);
			}
			
			return $return;
    	}
		
		static public function escape_query(string $query): string {
			return str_replace([chr(0), chr(8), chr(9), chr(10), chr(13), chr(26), "'", '"', "%", "_"], ["\\0", "\\b", "\\t", "\\n", "\\r", "\Z", "\\'", "\\\"", "\%", "\_"], str_replace("\\", "\\\\", $query));
		}
		
		static public function regex(string $type): string {
			switch ($type) {
				case "punct":
					return self::REGEX_TYPE_PUNCT;
				case "numbers":
					return self::REGEX_TYPE_NUMBERS;
				case "basic_letters":
					return self::REGEX_TYPE_BASIC_LETTERS;
				case "spanish_letters":
					return self::REGEX_TYPE_SPANISH_LETTERS;
				case "other_latin_letters":
					return self::REGEX_TYPE_OTHER_LATIN_LETTERS;
				case "business_name":
					return "[" . self::REGEX_TYPE_ALL_LATIN_SYMBOLS . "]( [" . self::REGEX_TYPE_ALL_LATIN_SYMBOLS . "]+|[" . self::REGEX_TYPE_ALL_LATIN_SYMBOLS . "]+)*";
				case "foreign_zipcode":
					return "[" . self::REGEX_TYPE_ALL_LATIN_LETTERS_WITH_NUMBERS . "]+";
				case "phone":
					return "\+[1-9][0-9]{1,24}";
				case "phone_int":
					return "[0-9#\*\+]+";
			}
		}
		
		static public function mail(string $from, array $to, array $cc, array $bcc, string $subject, string $body, string $mime = "html", array $attach = []): bool {
			try {
				if ($from == "" || count($to) == 0 || $subject == "" || !in_array($mime, ["text", "html"])) return false;
				if (!in_array($mime, ["text", "html"])) return false;

				$fields = [
					"from" => $from,
					"to" => $to,
					"subject" => $subject,
					$mime => $body
				];

				if (count($cc) > 0) $fields["cc"] = $cc;
				if (count($bcc) > 0) $fields["bcc"] = $bcc;

				$client = new Client();
				$result = $client->post(
					"https://api.mailgun.net/v3/info.??????.com.ar/messages",
					[
						"auth" => ["api", "b861ff0fcab955ffd696ebfd37466d16-acb0b40c-8513046a"],
						"form_params" => $fields
					]
				);

				if ($result->getStatusCode() != 200) return false;
				
				return true;
			}
			catch (Exception $ex) {
				return false;
			}
		}
	}
?>