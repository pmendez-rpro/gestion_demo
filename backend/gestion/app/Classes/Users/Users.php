<?php
	namespace App\Classes\Users;
	
	use GuzzleHttp\Exception\GuzzleException;
    use GuzzleHttp\Client;
    use App\Models\UsersModel;
	use Illuminate\Support\Facades\Cache;
	use App\Classes\General\Functions;
	
	class Users {
		private $lang;
		
		public function __construct(string $lang) {
			$this->lang = $lang;
		}
		
    	public function login(string $google_token) {
			$endpoint = str_replace("{token}", urlencode($google_token), env("GOOGLEAPIS_CHECK_LOGIN_TOKEN_URL"));
			
			try {
				$client = new Client();
				$result = $client->request(
					"GET",
					$endpoint
				);
				if ($result->getStatusCode() != 200) return false;
				$json = json_decode((string)$result->getBody());
				
				//Busca en la base de datos, si se trata de un usuario del sistema:
				$user = new UsersModel($this->lang);
				$login_result = $user->login($json->email, $json->picture);
				if ($login_result !== false) {
					return $login_result;
				}
				else {
					return false;
				}
			}
			catch (\Exception $ex) {
				return false;
			}
    	}
		
		public function force_login() {
			try {
				$user = new UsersModel($this->lang);
				return $user->login("???.developer.qa@gmail.com", "https://lh4.googleusercontent.com/-VilGH-udIqg/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdD4Q9Ph1EDfLECnAo9DuZLbN6apQ/s96-c/photo.jpg");
			}
			catch (\Exception $ex) {
				return false;
			}
    	}
		
		public function crud_token(): string {
			return Functions::get_crud_token();
    	}
		
		public function data(string $token) {
			try {
				return Cache::get("user_token_$token");
			}
			catch (\Exception $ex) {
				return false;
			}
    	}
		
		public function login_as(int $person_id, string $ancestor_token, string $ancestor_email) {
			try {
				$user = new UsersModel($this->lang);
				$login_result = $user->login_as($person_id, $ancestor_token, $ancestor_email);
				if ($login_result !== false) {
					$login_result["photo"] = "";
					return $login_result;
				}
				else {
					return false;
				}
			}
			catch (\Exception $ex) {
				return false;
			}
    	}
		
		public function logout(string $token) {
			try {
				//Procura leer primeramente los datos asociados al Token, para verificar si el usuario posee un ancestro:
				$user = Cache::get("user_token_$token");
				if (isset($user["ancestor"])) {
					$ancestor = Cache::get("user_token_{$user["ancestor"]}");
					$return = (isset($ancestor["id"])) ? $ancestor : "";
				}
				else {
					$return = "";
				}
				
				Cache::forget("user_token_$token");
				return $return;
			}
			catch (\Exception $ex) {
				return "";
			}
    	}
		
		public function list(): array {
			$user = new UsersModel($this->lang);
			return $user->list();
    	}
	}
?>