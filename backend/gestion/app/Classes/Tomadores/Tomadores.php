<?php
	namespace App\Classes\Tomadores;
	
    use App\Models\TomadoresModel;
	
	class Tomadores {
		private $lang;
		
		public function __construct(string $lang) {
			$this->lang = $lang;
		}
		
		public function insert(array $input): array {
			$tomador = new TomadoresModel($this->lang);
			return $tomador->insert($input);
    	}
	}
?>