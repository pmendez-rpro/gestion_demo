<?php
	namespace App\Classes\GoogleDrive;
	
	class GoogleDrive {
		static public function create_folder(string $folder, string $parent_code = ""): string {
			return @shell_exec("cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=create-folder --name=\"$folder\" --parent=\"$parent_code\"");
    	}
		
		static public function upload(string $file, string $name, string $parent_code = ""): string {
			return @shell_exec("cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=write --file=\"$file\" --name=\"$name\" --parent=\"$parent_code\"");
    	}
	}
?>