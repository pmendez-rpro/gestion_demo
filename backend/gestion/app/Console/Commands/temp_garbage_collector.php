<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	class temp_garbage_collector extends Command {
		//cd /var/www/html/gestion && php artisan gestion:temp_garbage_collector
		
		/**
		* The name and signature of the console command.
		*
		* @var string
		*/
		protected $signature = "gestion:temp_garbage_collector";
		
		/**
		* The console command description.
		*
		* @var string
		*/
		protected $description = 'Elimina todos los archivos y carpetas temporales.';
		
		private $max_time = 60 * 60 * 24;
		
		/**
		* Create a new command instance.
		*
		* @return void
		*/
		public function __construct() {
			parent::__construct();
		}
		
		/**
		* Execute the console command.
		*
		* @return mixed
		*/
		public function handle() {
			$now = time();
			$folders = glob("/usr/share/nginx/html/dist/temp/*");
			if (count($folders) > 0) {
				foreach ($folders as $folder) {
					if (is_dir($folder)) {
						$files = glob("$folder/*");
						if (count($files) > 0) {
							foreach ($files as $file) {
								$mtime = explode("_", pathinfo($file, PATHINFO_FILENAME));
								$mtime = explode("-", $mtime[1]);
								$mtime = mktime($mtime[3], $mtime[4], $mtime[5], $mtime[1], $mtime[2], $mtime[0]);
								if ($now - $mtime > $this->max_time) {
									@unlink($file);
								}
							}
							$files = glob("$folder/*");
							if (count($files) == 0) @rmdir($folder);
						}
					}
				}
			}
		}
	}
?>