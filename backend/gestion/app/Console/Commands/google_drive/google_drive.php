<?php
	require __DIR__ . "/../../../../vendor/autoload.php";
    
    class google_drive {
		//cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php
		
		//cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=first-time
		
		//cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=read --file="ID"
        //cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=read --file="1fJWYOF6sq-5HiH4rcD9rp9_b9JMsBEmc"
		
        //cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=write --file="xxx" --name="xxx"
        //cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=write --file="/var/www/html/rrhh/public/imgs/icons/bars-graphic-charts-referencing-statistics-icone-8333-32.png" --name="icono.png"
		
		//cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=delete --file="ID"
        //cd /var/www/html/gestion/app/Console/Commands/google_drive && php google_drive.php --operation=delete --file="1fojnO4hMYrnpe1nLT4LAp9du5DRXcc3k"
        
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
		
        /**
         * The console command description.
         *
         * @var string
         */

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct() {
        }
		
		public function getClient() {
			$client = new \Google_Client();
			$client->setApplicationName('??? ??? - ' . env("ENVIRONMENT"));
			$client->setScopes(\Google_Service_Drive::DRIVE);
			$client->setAuthConfig('credentials.json');
			$client->setAccessType('offline');
			
			// Load previously authorized credentials from a file.
			$credentialsPath = 'token.json';
			if (file_exists($credentialsPath)) {
				$accessToken = json_decode(file_get_contents($credentialsPath), true);
			}
			else {
				// Request authorization from the user.
				$authUrl = $client->createAuthUrl();
				printf("Open the following link in your browser:\n%s\n", $authUrl);
				print 'Enter verification code: ';
				$authCode = trim(fgets(STDIN));
				
				// Exchange authorization code for an access token.
				$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
				
				// Check to see if there was an error.
				if (array_key_exists('error', $accessToken)) {
					throw new Exception(join(', ', $accessToken));
				}
				
				// Store the credentials to disk.
				if (!file_exists(dirname($credentialsPath))) {
					mkdir(dirname($credentialsPath), 0700, true);
				}
				file_put_contents($credentialsPath, json_encode($accessToken));
				printf("Credentials saved to %s\n", $credentialsPath);
			}
			$client->setAccessToken($accessToken);
			
			// Refresh the token if it's expired.
			if ($client->isAccessTokenExpired()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
				file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
			}
			
			return $client;
		}
        
        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle() {
			try {
				for ($x = 1; $x <= 4; $x++) {
					if (!isset($_SERVER["argv"][$x])) continue;
					$value = explode("=", $_SERVER["argv"][$x]);
					switch ($value[0]) {
						case "--operation":
							$p_operation = $value[1];
							break;
						case "--file":
							$p_file = $value[1];
							break;
						case "--name":
							$p_name = $value[1];
							break;
						case "--parent":
							$p_parent = $value[1];
							break;
						default:
							die();
					}
				}
				switch ($p_operation ?? "") {
					case "first-time":
						$this->getClient();
						break;
					case "read":
						$client = $this->getClient();
						$service = new Google_Service_Drive($client);
						$response = $service->files->get(
							$p_file,
							[
								"alt" => "media"
							]
						);
						$result = $response->getHeaders()["Content-Type"][0] ?? "";
						if ($result != "") $result .= "@" . ($response->getBody()->getContents() ?? "");
						echo $result;

						break;
					case "delete":
						$client = $this->getClient();
						$service = new Google_Service_Drive($client);
						$service->files->delete($p_file);
						
						break;
					case "create-folder":
						$client = $this->getClient();
						$service = new Google_Service_Drive($client);
						$setup = [
							"name" => $p_name,
							"mimeType" => "application/vnd.google-apps.folder"
						];
						if (isset($p_parent) && $p_parent != "") $setup["parents"] = [$p_parent];
						$folder = new Google_Service_Drive_DriveFile($setup);
						$result = $service->files->create(
							$folder,
							[
								"fields" => "id"
							]
						);

						echo $result->id ?? "";

						break;
					case "write":
						$client = $this->getClient();
						$service = new Google_Service_Drive($client);
						$setup = [
							"name" => $p_name
						];
						if (isset($p_parent) && $p_parent != "") $setup["parents"] = [$p_parent];
						$file = new Google_Service_Drive_DriveFile($setup);
						$result = $service->files->create(
							$file,
							[
								"data" => file_get_contents($p_file),
								"mimeType" => "application/octet-stream",
								"uploadType" => "media"
							]
						);

						echo $result->id ?? "";

						break;
					default:
						die();
				}
			}
			catch(\Exception $e) {
				//echo $e->getMessage();
			}
        }
    }

	$drive = new google_drive();
	$drive->handle();
?>