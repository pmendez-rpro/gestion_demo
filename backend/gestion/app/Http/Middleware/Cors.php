<?php
	namespace App\Http\Middleware;
	
	use Closure;
	
	class Cors {
		/**
			* Handle an incoming request.
			*
			* @param  \Illuminate\Http\Request  $request
			* @param  \Closure  $next
			* @return mixed
		*/
		
		public function handle($request, Closure $next) {
			return $next($request)
				->header("Access-Control-Allow-Origin", "https://" . env("APP_URL_FRONTEND"))
				->header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
				->header("Access-Control-Allow-Headers", "content-type")
				->header("Access-Control-Max-Age", "1728000")
				->header("Access-Control-Allow-Credentials", "true")
			;
		}
	}
?>