<?php
	namespace App\Http\Controllers;
	
	use App\Classes\Entities\Entities;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Cache;
	
	class EntitiesController extends Controller {
		/**
		* @OA\Post(
		*     summary="Lista de entidades",
		*     description="Obtiene una lista de entidades, a base de fragmentos de sus razones sociales y/o documentos.",
		*     path="/entidades/lista",
		*     tags={"Lista de entidades"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: search",
		*         in="query",
		*         description="Texto por buscar dentro de la razón social o documento de la entidad, para que resulte listada. Debe poseer una longitud de entre cuatro y 200 caracteres.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la lista de entidades, cuyas razones sociales y/o documentos coincidan con el texto proporcionado.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="entities",
		*                 type="array",
		*                 @OA\Items(
		*                     @OA\Property(
		*                         property="id",
		*                         type="number",
		*                         example=1
		*                     ),
		*                     @OA\Property(
		*                         property="text",
		*                         type="string",
		*                         example="(CUIT/CUIL: 2024555999) Pérez, Juan (Tomador)"
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación (PERMISO_TOMADOR_ALTA).",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function list(Request $request) {
			$input = $request->all();
			if (!isset($input["token"]) || !isset($input["search"]) || mb_strlen($input["search"]) < 4 || mb_strlen($input["search"]) > 200) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_TOMADOR_ALTA"])) return $this->invalid_permission();
				$entities = new Entities(env("APP_LANG"));
				$list = $entities->list($input["search"]);
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"entities" => $list
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
	}
?>