<?php
	namespace App\Http\Controllers;
	
	use App\Classes\Filesystem\Filesystem;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Cache;
	
	class FilesystemController extends Controller {
		/**
		* @OA\Post(
		*     summary="Envío de un archivo",
		*     description="Envía un archivo al servidor.",
		*     path="/archivo/enviar",
		*     tags={"Envío de un archivo"},
		*     @OA\Parameter(
		*         name="Content-Type: application/x-www-form-urlencoded",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/x-www-form-urlencoded")
		*     ),
		*     @OA\Parameter(
		*         name="token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="crud_token",
		*         in="query",
		*         description="Token obtenido para la realización del CRUD.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="reason",
		*         in="query",
		*         description="Motivo por el cual se envía el archivo.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="file",
		*         in="query",
		*         description="Archivo que se envía.",
		*         required=true,
		*         @OA\Schema(type="file")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la URL pública del archivo subido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="url",
		*                 type="string",
		*                 example="https://dev-gestion.??????.com.ar/temp/5mcmKnjTkoilbdkTwumw0wH5cG6czrm4QVlP3HY0lreY3Kai5J_n3LPRrWgpheKO/nosis_2019-05-31-17-23-24.pdf"
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TYPE"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function send(Request $request) {
			$input = $request->all();
			if (!isset($input["token"]) || !isset($input["crud_token"])  || $input["crud_token"] == "" || !isset($input["reason"]) || !isset($input["file"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$request->hasFile("file") || !$request->file("file")->isValid() || $input["file"]->getError() != 0) return $this->invalid_request();
				switch ($input["reason"]) {
					case "REASON_NOSIS":
						$permissions = ["PERMISO_TOMADOR_ALTA"];
						$mimes = ["application/pdf"];
						$max_filesize = 2097152;
						$time = date("Y-m-d-H-i-s", time());
						$final_filename = "nosis_$time.pdf";
						
						break;
					default:
						return $this->invalid_request();
				}
				if (!$this->has_permission($data["permisos_nombres"], $permissions)) return $this->invalid_permission();
				$another_errors = [];
				if (!in_array($input["file"]->getClientMimeType(), $mimes)) $another_errors[] = "ERR_TYPE";
				if ($input["file"]->getClientSize() > $max_filesize) $another_errors[] = "ERR_SIZE";
				if (count($another_errors) > 0) {
					return response()->json(
						[
							"error" => $another_errors
						],
						400
					);
				}
				
				$destination = "/usr/share/nginx/html/dist/temp/{$input["crud_token"]}";
				@mkdir($destination);
				@unlink("$destination/$final_filename");
				$input["file"]->move($destination, $final_filename);
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"url" => "https://" . env("APP_URL_FRONTEND") . "/temp/{$input["token"]}/$final_filename"
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
	}
?>