<?php
	namespace App\Http\Controllers;
	
	use App\Classes\Geo\Geo;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Cache;
	
	class GeoController extends Controller {
		/**
		* @OA\Post(
		*     summary="Lista de países",
		*     description="Obtiene la lista de países y sus prefijos telefónicos.",
		*     path="/geo/paises",
		*     tags={"Lista de países"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la lista de países y sus prefijos telefónicos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="countries",
		*                 type="array",
		*                 @OA\Items(
		*                     @OA\Property(
		*                         property="id",
		*                         type="number",
		*                         example=110
		*                     ),
		*                     @OA\Property(
		*                         property="name",
		*                         type="string",
		*                         example="Argentina"
		*                     ),
		*                     @OA\Property(
		*                         property="prefix",
		*                         type="number",
		*                         example=54
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación (PERMISO_TOMADOR_ALTA).",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function countries(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_TOMADOR_ALTA"])) return $this->invalid_permission();
				$geo = new Geo(env("APP_LANG"));
				$list = $geo->countries();
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"countries" => $list
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Lista de provincias",
		*     description="Obtiene la lista de provincias de un país.",
		*     path="/geo/provincias",
		*     tags={"Lista de provincias"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: country",
		*         in="query",
		*         description="Código del prefijo telefónico del país.",
		*         required=true,
		*         @OA\Schema(type="number")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la lista de provincias de un país.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="provinces",
		*                 type="array",
		*                 @OA\Items(
		*                     @OA\Property(
		*                         property="id",
		*                         type="number",
		*                         example=1
		*                     ),
		*                     @OA\Property(
		*                         property="name",
		*                         type="string",
		*                         example="Buenos Aires"
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación (PERMISO_TOMADOR_ALTA).",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function provinces(Request $request) {
			$input = $request->all();
			if (!isset($input["token"]) || !isset($input["country"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_TOMADOR_ALTA"])) return $this->invalid_permission();
				$geo = new Geo(env("APP_LANG"));
				$list = $geo->provinces((int)$input["country"]);
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"provinces" => $list
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Lista de localidades",
		*     description="Obtiene la lista de localidades de una provincia.",
		*     path="/geo/localidades",
		*     tags={"Lista de localidades"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: province",
		*         in="query",
		*         description="Código de la provincia.",
		*         required=true,
		*         @OA\Schema(type="number")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la lista de localidades de una provincia.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="cities",
		*                 type="array",
		*                 @OA\Items(
		*                     @OA\Property(
		*                         property="id",
		*                         type="number",
		*                         example=1
		*                     ),
		*                     @OA\Property(
		*                         property="name",
		*                         type="string",
		*                         example="Florencio Varela"
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación (PERMISO_TOMADOR_ALTA).",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function cities(Request $request) {
			$input = $request->all();
			if (!isset($input["token"]) || !isset($input["province"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_TOMADOR_ALTA"])) return $this->invalid_permission();
				$geo = new Geo(env("APP_LANG"));
				$list = $geo->cities((int)$input["province"]);
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"cities" => $list
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
	}
?>