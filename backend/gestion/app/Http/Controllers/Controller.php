<?php
	namespace App\Http\Controllers;
	
	use Laravel\Lumen\Routing\Controller as BaseController;
	//use Illuminate\Support\Facades\Cache;
	
	//Cache::flush(); die();
	
	class Controller extends BaseController {
		/**
		* @OA\Info(
		*   title="Sistema de Gestión Empresarial",
		*   version="1.0",
		*   @OA\Contact(
		*     email="desarrollo-scrum@??????.com.ar",
		*     name="Equipo Scrum"
		*   )
		* )
		*/
		
		protected function invalid_request(): \Illuminate\Http\JsonResponse {
			return response()->json(
				[
					"error" => ["ERR_INVALID_REQUEST"]
				],
				400
			);
		}
		
		protected function invalid_token(): \Illuminate\Http\JsonResponse {
			return response()->json(
				[
					"error" => ["ERR_TOKEN_NOT_FOUND"]
				],
				403
			);
		}
		
		protected function invalid_permission(): \Illuminate\Http\JsonResponse {
			return response()->json(
				[
					"error" => ["ERR_NO_PERMISSION"]
				],
				401
			);
		}
		
		protected function has_permission(string $permissions, array $needed, string $and_or = "or"): bool {
			if (count($needed) == 0) return true;
			$permissions = explode(",", $permissions);
			if (count($permissions) == 0) return false;
			$and_or = mb_strtolower($and_or);
			if (!in_array($and_or, ["and", "or"])) return false;
			$hits = 0;
			foreach ($needed as $permission) {
				if (in_array($permission, $permissions)) {
					$hits++;
					if ($and_or == "or") break;
				}
			}
			
			return (($and_or == "or" && $hits > 0) || ($and_or == "and" && $hits == count($needed))) ? true : false;
		}
	}
?>