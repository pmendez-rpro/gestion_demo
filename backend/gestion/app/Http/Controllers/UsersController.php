<?php
	namespace App\Http\Controllers;
	
	use App\Classes\Users\Users;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Cache;
	
	class UsersController extends Controller {
		/**
		* @OA\Post(
		*     summary="Login",
		*     description="Efectúa el Login con Google.",
		*     path="/usuario/login",
		*     tags={"Login"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: id_token",
		*         in="query",
		*         description="Token brindado por Google, cuando se lleva a cabo el Login mediante este sistema.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve un Token generado por el Backend, mediante el cual se podrán efectuar posteriores peticiones, en nombre del usuario que ha ingresado.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="token",
		*                 type="string",
		*                 example="TOKEN_DEVUELTO_POR_EL_BACKEND"
		*             ),
		*             @OA\Property(
		*                 property="permissions",
		*                 type="array",
		*                 @OA\Items(
		*                     example="PERMISO_INGRESO"", ""PERMISO_LOGIN_COMO"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="email",
		*                 type="string",
		*                 example="EMAIL_DEL_USUARIO"
		*             ),
		*             @OA\Property(
		*                 property="name",
		*                 type="string",
		*                 example="RAZÓN_SOCIAL_DEL_USUARIO"
		*             ),
		*             @OA\Property(
		*                 property="photo",
		*                 type="string",
		*                 example="URL_DE_LA_FOTO_DE_PERFIL"
		*             ),
		*             @OA\Property(
		*                 property="area",
		*                 type="string",
		*                 example="ÁREA_EN_LA_QUE_TRABAJA"
		*             ),
		*             @OA\Property(
		*                 property="areas",
		*                 type="array",
		*                 @OA\Items(
		*                     example="COMERCIAL"", ""RRCLC"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="agency",
		*                 type="string",
		*                 example="NOMBRE_DE_LA_AGENCIA"
		*             ),
		*             @OA\Property(
		*                 property="agencies",
		*                 type="array",
		*                 @OA\Items(
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="notifications",
		*                 type="object",
		*                 @OA\Property(
		*                     property="social",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 ),
		*                 @OA\Property(
		*                     property="enterprise",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Usuario no autorizado a ingresar.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_LOGIN"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function login(Request $request) {
			$input = $request->all();
			if (!isset($input["id_token"])) return $this->invalid_request();
			$users = new Users(env("APP_LANG"));
			$state = $users->login($input["id_token"]);
			if ($state !== false) {
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"token" => $state["token"],
						"permissions" => explode(",", $state["permisos_nombres"]),
						"email" => $state["email"],
						"name" => $state["name"],
						"photo" => $state["photo"],
						"area" => $state["area_nombre"],
						"areas" => [
							$state["area_constante"]
						],
						"agency" => $state["agencia_nombre"],
						"agencies" => [],
						"notifications" => [
							"social" => [],
							"enterprise" => []
						]
					],
					200
				);
			}
			else {
				return response()->json(
					[
						"error" => [
							"ERR_INVALID_LOGIN"
						]
					],
					403
				);
			}
		}
		
		public function force_login(Request $request) {
			$input = $request->all();
			if (!isset($input["secret"]) || sha1(md5($input["secret"])) != "78c79ea4b0b4140f4b7279ae8c0fbfb48d421d31") die();
			$users = new Users(env("APP_LANG"));
			$state = $users->force_login();
			if ($state !== false) {
				echo $state["token"];
			}
			else {
				echo "";
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Obtener Token para CRUD",
		*     description="Obtiene un Token especial, para que cada archivo que se transmita como parte de una ABM, pueda asociarse con la misma.",
		*     path="/crud/obtener-token",
		*     tags={"Obtener Token para CRUD"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve un Token para CRUD.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="crud_token",
		*                 type="string",
		*                 example="TOKEN_DEVUELTO_PARA_LA_REALIZACIÓN_DE_CRUD"
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function crud_token(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				$users = new Users(env("APP_LANG"));
				$crud_token = $users->crud_token();
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"crud_token" => $crud_token
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Datos del usuario logueado",
		*     description="Obtiene todos los datos del usuario logueado.",
		*     path="/usuario/datos",
		*     tags={"Datos del usuario logueado"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve todos los datos del usuario Logueado.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="token",
		*                 type="string",
		*                 example="TOKEN_DEVUELTO_POR_EL_BACKEND"
		*             ),
		*             @OA\Property(
		*                 property="permissions",
		*                 type="array",
		*                 @OA\Items(
		*                     example="PERMISO_INGRESO"", ""PERMISO_LOGIN_COMO"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="email",
		*                 type="string",
		*                 example="EMAIL_DEL_USUARIO"
		*             ),
		*             @OA\Property(
		*                 property="name",
		*                 type="string",
		*                 example="RAZÓN_SOCIAL_DEL_USUARIO"
		*             ),
		*             @OA\Property(
		*                 property="photo",
		*                 type="string",
		*                 example="URL_DE_LA_FOTO_DE_PERFIL"
		*             ),
		*             @OA\Property(
		*                 property="area",
		*                 type="string",
		*                 example="ÁREA_EN_LA_QUE_TRABAJA"
		*             ),
		*             @OA\Property(
		*                 property="areas",
		*                 type="array",
		*                 @OA\Items(
		*                     example="COMERCIAL"", ""RRCLC"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="agency",
		*                 type="string",
		*                 example="NOMBRE_DE_LA_AGENCIA"
		*             ),
		*             @OA\Property(
		*                 property="agencies",
		*                 type="array",
		*                 @OA\Items(
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="notifications",
		*                 type="object",
		*                 @OA\Property(
		*                     property="social",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 ),
		*                 @OA\Property(
		*                     property="enterprise",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Usuario no autorizado a ingresar.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_LOGIN"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function data(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			$users = new Users(env("APP_LANG"));
			$state = $users->data($input["token"]);
			if ($state !== false) {
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"token" => $state["token"],
						"permissions" => explode(",", $state["permisos_nombres"]),
						"email" => $state["email"],
						"name" => $state["name"],
						"photo" => $state["photo"],
						"area" => $state["area_nombre"],
						"areas" => [
							$state["area_constante"]
						],
						"agency" => $state["agencia_nombre"],
						"agencies" => [],
						"notifications" => [
							"social" => [],
							"enterprise" => []
						]
					],
					200
				);
			}
			else {
				return response()->json(
					[
						"error" => [
							"ERR_INVALID_LOGIN"
						]
					],
					403
				);
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Login como",
		*     description="Otorga al desarrollador, el derecho a ingresar como otro usuario.",
		*     path="/usuario/login-como",
		*     tags={"Login como"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: person_id",
		*         in="query",
		*         description="ID de la persona cuyo Login se imitará.",
		*         required=true,
		*         @OA\Schema(type="number")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve un Token generado por el Backend, mediante el cual se podrán efectuar posteriores peticiones, en nombre del usuario cuyo Login se ha imitado.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="token",
		*                 type="string",
		*                 example="TOKEN_DEVUELTO_POR_EL_BACKEND (usuario emulado)"
		*             ),
		*             @OA\Property(
		*                 property="permissions",
		*                 type="array",
		*                 @OA\Items(
		*                     example="PERMISO_INGRESO"", ""PERMISO_LOGIN_COMO (usuario emulado)"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="email",
		*                 type="string",
		*                 example="EMAIL_DEL_DESARROLLADOR (usuario que solicitó 'loguearse como')"
		*             ),
		*             @OA\Property(
		*                 property="name",
		*                 type="string",
		*                 example="RAZÓN_SOCIAL_DEL_USUARIO (usuario emulado)"
		*             ),
		*             @OA\Property(
		*                 property="photo",
		*                 type="string",
		*                 example=""
		*             ),
		*             @OA\Property(
		*                 property="area",
		*                 type="string",
		*                 example="ÁREA_EN_LA_QUE_TRABAJA (usuario emulado)"
		*             ),
		*             @OA\Property(
		*                 property="areas",
		*                 type="array",
		*                 @OA\Items(
		*                     example="COMERCIAL"", ""RRCLC (usuario emulado)"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="agency",
		*                 type="string",
		*                 example="NOMBRE_DE_LA_AGENCIA (usuario emulado)"
		*             ),
		*             @OA\Property(
		*                 property="agencies",
		*                 type="array",
		*                 @OA\Items(
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="notifications",
		*                 type="object",
		*                 @OA\Property(
		*                     property="social",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 ),
		*                 @OA\Property(
		*                     property="enterprise",
		*                     type="array",
		*                     @OA\Items(
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Usuario no autorizado a ingresar.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_LOGIN"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function login_as(Request $request) {
			$input = $request->all();
			if (!isset($input["token"]) || !isset($input["person_id"])) return $this->invalid_request();
			$data = Cache::get("user_token_{$input["token"]}");
			if ($data == false) return $this->invalid_token();
			if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_LOGIN_COMO"])) return $this->invalid_permission();
			$users = new Users(env("APP_LANG"));
			$state = $users->login_as((int)$input["person_id"], $input["token"], $data["email"]);
			if ($state !== false) {
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"token" => $state["token"],
						"permissions" => explode(",", $state["permisos_nombres"]),
						"email" => $state["email"],
						"name" => $state["name"],
						"photo" => $state["photo"],
						"area" => $state["area_nombre"],
						"areas" => [
							$state["area_constante"]
						],
						"agency" => $state["agencia_nombre"],
						"agencies" => [],
						"notifications" => [
							"social" => [],
							"enterprise" => []
						]
					],
					200
				);
			}
			else {
				return response()->json(
					[
						"error" => [
							"ERR_INVALID_LOGIN"
						]
					],
					403
				);
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Logout",
		*     description="Efectúa el Logout.",
		*     path="/usuario/logout",
		*     tags={"Logout"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Logout realizado correctamente.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="token",
		*                 type="string",
		*                 example="TOKEN_DEVUELTO_POR_EL_BACKEND (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="permissions",
		*                 type="array",
		*                 @OA\Items(
		*                     example="PERMISO_INGRESO"", ""PERMISO_LOGIN_COMO (sólo si se sale de un 'Login como')"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="email",
		*                 type="string",
		*                 example="EMAIL_DEL_USUARIO (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="name",
		*                 type="string",
		*                 example="RAZÓN_SOCIAL_DEL_USUARIO (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="photo",
		*                 type="string",
		*                 example="URL_DE_LA_FOTO_DE_PERFIL (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="area",
		*                 type="string",
		*                 example="ÁREA_EN_LA_QUE_TRABAJA (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="areas",
		*                 type="array",
		*                 @OA\Items(
		*                     example="COMERCIAL"", ""RRCLC (sólo si se sale de un 'Login como')"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="agency",
		*                 type="string",
		*                 example="NOMBRE_DE_LA_AGENCIA (sólo si se sale de un 'Login como')"
		*             ),
		*             @OA\Property(
		*                 property="agencies",
		*                 type="array",
		*                 @OA\Items(
		*                     example="(sólo si se sale de un 'Login como')"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="notifications",
		*                 type="object",
		*                 @OA\Property(
		*                     property="social",
		*                     type="array",
		*                     @OA\Items(
		*                         example="(sólo si se sale de un 'Login como')"
		*                     )
		*                 ),
		*                 @OA\Property(
		*                     property="enterprise",
		*                     type="array",
		*                     @OA\Items(
		*                         example="(sólo si se sale de un 'Login como')"
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function logout(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			$users = new Users(env("APP_LANG"));
			$state = $users->logout($input["token"]);
			if (isset($state["id"])) {
				//Es un Logout que se corresponde con la acción "Login como...":
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"token" => $state["token"],
						"permissions" => explode(",", $state["permisos_nombres"]),
						"email" => $state["email"],
						"name" => $state["name"],
						"photo" => $state["photo"],
						"area" => $state["area_nombre"],
						"areas" => [
							$state["area_constante"]
						],
						"agency" => $state["agencia_nombre"],
						"agencies" => [],
						"notifications" => [
							"social" => [],
							"enterprise" => []
						]
					],
					200
				);
			}
			else {
				//Logout normal:
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						]
					],
					200
				);
			}
		}
		
		/**
		* @OA\Post(
		*     summary="Lista de usuarios para la realización de la operación 'login como...'",
		*     description="Retorna una lista con los 'id' y los nombres de los usuarios del sistema, a condición de que la persona solicitante, posea el permiso 'PERMISO_LOGIN_COMO'.",
		*     path="/usuarios/lista",
		*     tags={"Lista de usuarios para 'login como'"},
		*     @OA\Parameter(
		*         name="Content-Type: application/json",
		*         in="header",
		*         description="",
		*         required=true,
		*         @OA\Schema(pattern="application/json")
		*     ),
		*     @OA\Parameter(
		*         name="JSON: token",
		*         in="query",
		*         description="Token brindado oportunamente por el Backend.",
		*         required=true,
		*         @OA\Schema(type="string")
		*     ),
		*     @OA\Response(
		*         response="200",
		*         description="Devuelve la lista de usuarios.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_ERROR"
		*                 )
		*             ),
		*             @OA\Property(
		*                 property="users",
		*                 type="array",
		*                 @OA\Items(
		*                     @OA\Property(
		*                         property="id",
		*                         type="number",
		*                         example=1
		*                     ),
		*                     @OA\Property(
		*                         property="name",
		*                         type="string",
		*                         example="Laura"
		*                     )
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="400",
		*         description="Petición inválida.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INVALID_REQUEST"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="401",
		*         description="No posee permisos para realizar la operación.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NO_PERMISSION"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="403",
		*         description="Token inválido.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_TOKEN_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="404",
		*         description="URL o método inválidos.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_NOT_FOUND"
		*                 )
		*             )
		*         )
		*     ),
		*     @OA\Response(
		*         response="500",
		*         description="Error interno del servidor.",
		*         @OA\JsonContent(
		*             @OA\Property(
		*                 property="error",
		*                 type="array",
		*                 @OA\Items(
		*                     example="ERR_INTERNAL_SERVER_ERROR"
		*                 )
		*             )
		*         )
		*     )
		* )
		*/
		public function list(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_LOGIN_COMO"])) return $this->invalid_permission();
				$users = new Users(env("APP_LANG"));
				$list = $users->list();
				
				return response()->json(
					[
						"error" => [
							"ERR_NO_ERROR"
						],
						"users" => $list
					],
					200
				);
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
	}
?>