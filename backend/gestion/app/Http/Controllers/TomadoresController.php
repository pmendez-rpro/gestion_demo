<?php
	namespace App\Http\Controllers;
	
	use App\Classes\Tomadores\Tomadores;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Cache;
	
	class TomadoresController extends Controller {
		public function insert(Request $request) {
			$input = $request->all();
			if (!isset($input["token"])) return $this->invalid_request();
			try {
				$data = Cache::get("user_token_{$input["token"]}");
				if ($data == false) return $this->invalid_token();
				if (!$this->has_permission($data["permisos_nombres"], ["PERMISO_TOMADOR_ALTA"])) return $this->invalid_permission();
				$tomador = new Tomadores(env("APP_LANG"));
				$result = $tomador->insert($input);
				if (!isset($result["id"])) {
					return response()->json(
						[
							"error" => $result["errors"]
						],
						400
					);
				}
				else {
					return response()->json(
						[
							"error" => [
								"ERR_NO_ERROR"
							],
							"data" => [
								"id" => $result["id"],
								"name" => $result["name"]
							]
						],
						200
					);
				}
			}
			catch (Exception $e) {
				return $this->invalid_token();
			}
		}
	}
?>