<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute',
    'active_url'           => ':attribute',
    'after'                => ':attribute',
    'after_or_equal'       => ':attribute',
    'alpha'                => ':attribute',
    'alpha_dash'           => ':attribute',
    'alpha_num'            => ':attribute',
    'array'                => ':attribute',
    'before'               => ':attribute',
    'before_or_equal'      => ':attribute',
    'between'              => [
        'numeric' => ':attribute',
        'file'    => ':attribute',
        'string'  => ':attribute',
        'array'   => ':attribute',
    ],
    'boolean'              => ':attribute',
    'confirmed'            => ':attribute',
    'date'                 => ':attribute',
    'date_format'          => ':attribute',
    'different'            => ':attribute',
    'digits'               => ':attribute',
    'digits_between'       => ':attribute',
    'dimensions'           => ':attribute',
    'distinct'             => ':attribute',
    'email'                => ':attribute',
    'exists'               => ':attribute',
    'file'                 => ':attribute',
    'filled'               => ':attribute',
    'image'                => ':attribute',
    'in'                   => ':attribute',
    'in_array'             => ':attribute',
    'integer'              => ':attribute',
    'ip'                   => ':attribute',
    'json'                 => ':attribute',
    'max'                  => [
        'numeric' => ':attribute',
        'file'    => ':attribute',
        'string'  => ':attribute',
        'array'   => ':attribute',
    ],
    'mimes'                => ':attribute',
    'mimetypes'            => ':attribute',
    'min'                  => [
        'numeric' => ':attribute',
        'file'    => ':attribute',
        'string'  => ':attribute',
        'array'   => ':attribute',
    ],
    'not_in'               => ':attribute',
    'numeric'              => ':attribute',
    'present'              => ':attribute',
    'regex'                => ':attribute',
    'required'             => ':attribute',
    'required_if'          => ':attribute',
    'required_unless'      => ':attribute',
    'required_with'        => ':attribute',
    'required_with_all'    => ':attribute',
    'required_without'     => ':attribute',
    'required_without_all' => ':attribute',
    'same'                 => ':attribute',
    'size'                 => [
        'numeric' => ':attribute',
        'file'    => ':attribute',
        'string'  => ':attribute',
        'array'   => ':attribute',
    ],
    'string'               => ':attribute',
    'timezone'             => ':attribute',
    'unique'               => ':attribute',
    'uploaded'             => ':attribute',
    'url'                  => ':attribute',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
