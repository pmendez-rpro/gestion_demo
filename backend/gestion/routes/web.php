<?php
use Illuminate\Http\Response;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
 
$router->group(['middleware' => 'cors'], function () use ($router) {
	$router->options('/archivo/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/crud/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/entidades/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/geo/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/tomador/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/usuario/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->options('/usuarios/{all}',
					function($all) {
						return (new Response("", 200));
					}
	);
	$router->post('1zcTwubuURaP0DucQKgxfqYDUlJf2CXZnMa32VOE', "UsersController@force_login");
	$router->post('/archivo/enviar', "FilesystemController@send");
	$router->post('/crud/obtener-token', "UsersController@crud_token");
	$router->post('/entidades/lista', "EntitiesController@list");
	$router->post('/geo/localidades', "GeoController@cities");
	$router->post('/geo/paises', "GeoController@countries");
	$router->post('/geo/provincias', "GeoController@provinces");
	$router->put('/tomador/insertar', "TomadoresController@insert");
	$router->post('/usuario/datos', "UsersController@data");
	$router->post('/usuario/login', "UsersController@login");
	$router->post('/usuario/login-como', "UsersController@login_as");
	$router->post('/usuario/logout', "UsersController@logout");
	$router->post('/usuarios/lista', "UsersController@list");
});